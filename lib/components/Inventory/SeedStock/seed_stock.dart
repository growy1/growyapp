import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:growyapp/components/Strain/strain.dart';
import 'package:growyapp/generated/graphql_api.dart';

class SeedStockComponent extends StatefulWidget {
  const SeedStockComponent({this.inventoryId});
  final String inventoryId;
  @override
  _SeedStockComponentState createState() => _SeedStockComponentState();
}

class _SeedStockComponentState extends State<SeedStockComponent> {
  @override
  Widget build(BuildContext context) {
    return Query(
      options: QueryOptions(
        documentNode: SeedInventoryQuery().document,
        variables:
            SeedInventoryArguments(inventoryId: widget.inventoryId).toJson(),
      ),
      builder: (QueryResult result,
          {Future<QueryResult> Function() refetch, FetchMore fetchMore}) {
        if (result.hasException) {
          return Center(
              child: Text(
            '⚠️ ${result.exception.toString()}',
            textAlign: TextAlign.center,
            style: const TextStyle(color: Colors.red),
          ));
        }
        if (result.loading) {
          return const Center(
            child: CircularProgressIndicator(
              backgroundColor: Colors.white,
            ),
          );
        }

        final List<SeedInventory$Query$Inventory$SeedStock> seedStocklist =
            SeedInventoryQuery()
                .parse(result.data as Map<String, dynamic>)
                .inventory
                .seedStock;
        seedStocklist.sort((SeedInventory$Query$Inventory$SeedStock a,
                SeedInventory$Query$Inventory$SeedStock b) =>
            a.count.compareTo(b.count));

        return RefreshIndicator(
          onRefresh: () async => await refetch(),
          child: GridView.builder(
              itemCount: seedStocklist.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: (MediaQuery.of(context).orientation ==
                          Orientation.portrait)
                      ? 2
                      : 3),
              itemBuilder: (BuildContext ctx, int index) {
                return Card(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Colors.green,
                        radius: 40,
                        child: Text(
                          seedStocklist[index].count.toInt().toString(),
                          textScaleFactor: 1.7,
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                      RaisedButton(
                        onPressed: () => Navigator.push<dynamic>(
                          context,
                          MaterialPageRoute<dynamic>(
                              builder: (BuildContext context) =>
                                  StrainComponent(
                                    id: seedStocklist[index].strain.id,
                                  )),
                        ),
                        child: Text(
                          seedStocklist[index].strain.name,
                          textScaleFactor: 1.2,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                          style: const TextStyle(color: Colors.black),
                        ),
                      )
                    ],
                  ),
                );
              }),
          // child: ListView.builder(
          //   scrollDirection: Axis.vertical,
          //   padding: const EdgeInsets.all(3),
          //   itemBuilder: (_, int index) {
          //     return Padding(
          //         padding: const EdgeInsets.all(8.0),
          //         child: ListTile(
          //           title: Row(
          //             mainAxisAlignment: MainAxisAlignment.start,
          //             children: <Widget>[
          //               Expanded(
          //                 child: Text(
          //                   seedStocklist[index].strain.name,
          //                   textScaleFactor: 1.2,
          //                   overflow: TextOverflow.ellipsis,
          //                   style: const TextStyle(color: Colors.black),
          //                 ),
          //               ),
          //               const Text(
          //                 ' - ',
          //               ),
          //               Text(
          //                 seedStocklist[index]
          //                     .strain
          //                     .photoperiod
          //                     .toString()
          //                     .split('.')
          //                     .last
          //                     .substring(0, 3)
          //                     .toUpperCase(),
          //                 overflow: TextOverflow.ellipsis,
          //                 style: const TextStyle(color: Colors.black),
          //               ),
          //             ],
          //           ),
          //           tileColor: Colors.white,
          //           onTap: () => Navigator.push<dynamic>(
          //             context,
          //             MaterialPageRoute<dynamic>(
          //                 builder: (BuildContext context) => StrainComponent(
          //                       id: seedStocklist[index].strain.id,
          //                     )),
          //           ),
          //           leading: CircleAvatar(
          //             backgroundColor: Colors.green,
          //             child: Text(
          //               seedStocklist[index].count.toInt().toString(),
          //               textScaleFactor: 1.7,
          //               style: const TextStyle(color: Colors.white),
          //             ),
          //           ),
          //           trailing: IconButton(
          //             icon: const Icon(Icons.edit),
          //             onPressed: () => print('ok'),
          //           ),
          //         ));
          //   },
          //   itemCount: seedStocklist.length,
          // ),
        );
      },
    );
  }
}
