import 'package:flutter/material.dart';
import 'package:growyapp/generated/graphql_api.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:growyapp/components/Strain/strain_select_list.dart';

class AddSeedStockDialog extends StatefulWidget {
  const AddSeedStockDialog({
    Key key,
    this.inventoryId,
  }) : super(key: key);
  final String inventoryId;

  @override
  _AddSeedStockDialogState createState() => _AddSeedStockDialogState();
}

class _AddSeedStockDialogState extends State<AddSeedStockDialog> {
  int _selectedAddSeedStrainId = 1;
  int _addSeedCountInput = 0;
  @override
  Widget build(BuildContext context) {
    final AddSeedStockArguments vars = AddSeedStockArguments(
        inventoryId: widget.inventoryId,
        input: SeedStockInput(
            unit: 'seed',
            count: _addSeedCountInput.toDouble(),
            strainId: _selectedAddSeedStrainId));
    final AddSeedStockMutation mut = AddSeedStockMutation(variables: vars);
    return Mutation(
      options: MutationOptions(
          documentNode: mut.document,
          update: (Cache _, QueryResult res) {
            if (res.hasException) {
              final SnackBar snackBar =
                  SnackBar(content: Text('⚠️ ${res.exception.toString()}'));
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
              Navigator.pop(context);
            } else if (!res.loading) {
              const SnackBar snackBar = SnackBar(content: Text('Saved'));
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
              Navigator.pop(context);
            }
          }),
      builder: (RunMutation run, QueryResult result) => AlertDialog(
        title: const Text('+ Add seed(s) in stock'),
        insetPadding: const EdgeInsets.all(20),
        actions: <Widget>[
          if (result.loading)
            const CircularProgressIndicator()
          else
            FlatButton.icon(
              label: const Text(
                '+',
                textAlign: TextAlign.end,
              ),
              color: Colors.green,
              icon: const Icon(Icons.save),
              onPressed: () => run(vars.toJson()),
            ),
          IconButton(
            color: Colors.grey,
            icon: const Icon(
              Icons.cancel,
            ),
            onPressed: () => Navigator.pop(context),
          ),
        ],
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            StrainSelectListComponent(
              value: _selectedAddSeedStrainId,
              callback: (int strainId) {
                _selectedAddSeedStrainId = strainId;
                setState(() {});
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                TextButton(
                  child: const Text('-'),
                  onPressed: () => setState(() {
                    if (_addSeedCountInput > 0) {
                      _addSeedCountInput--;
                    }
                  }),
                ),
                Text(_addSeedCountInput.toString()),
                TextButton(
                  child: const Text('+'),
                  onPressed: () => setState(() {
                    _addSeedCountInput += 1;
                  }),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
