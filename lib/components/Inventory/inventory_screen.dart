import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:growyapp/components/Inventory/DeviceStock/add_device_stock.dart';
import 'package:growyapp/components/Inventory/DeviceStock/device_stock.dart';
import 'package:growyapp/components/Inventory/SeedStock/add_seed_stock.dart';
import 'package:growyapp/components/Inventory/SeedStock/seed_stock.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import 'package:sembast_web/sembast_web.dart';

class InventoryScreenComponent extends StatefulWidget {
  const InventoryScreenComponent();

  @override
  _InventoryScreenComponentState createState() =>
      _InventoryScreenComponentState();
}

typedef IntCallback = void Function(int val);

class _InventoryScreenComponentState extends State<InventoryScreenComponent> {
  String inventoryId;
  Future<void> getInventoryID() async {
    Database db;
    const String dbPath = 'growyapp.db';
    final StoreRef<String, String> store = StoreRef<String, String>.main();
    if (!kIsWeb) {
      final Directory appDocDir = await getApplicationDocumentsDirectory();
      final DatabaseFactory dbFactory = databaseFactoryIo;
      db = await dbFactory.openDatabase(appDocDir.path + '/' + dbPath);
    } else {
      final DatabaseFactory dbFactory = databaseFactoryWeb;
      db = await dbFactory.openDatabase(dbPath);
    }
    inventoryId = await store.record('inventory_id').get(db);
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getInventoryID();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        const SizedBox(
          height: 30,
        ),
        Container(
          padding: const EdgeInsets.all(8),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.green),
          ),
          child: const Text(
            'Inventory',
            textScaleFactor: 3,
            style: TextStyle(color: Colors.white),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Expanded(
          child: DefaultTabController(
            length: 3,
            child: Column(
              children: <Widget>[
                AppBar(
                  leading: Container(),
                  backgroundColor: Colors.black,
                  flexibleSpace: const TabBar(
                    tabs: <Widget>[
                      Tab(
                        icon: Text(
                          '🌰 Seeds',
                          textScaleFactor: 1.3,
                          style: TextStyle(
                              color: Colors.white, fontFamily: 'EmojiOne'),
                        ),
                      ),
                      Tab(
                          icon: Text(
                        '🧰 Devices',
                        textScaleFactor: 1.3,
                        style: TextStyle(
                            color: Colors.white, fontFamily: 'EmojiOne'),
                      )),
                      Tab(
                          icon: Text(
                        '🦄 Harvest',
                        textScaleFactor: 1.3,
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'EmojiOne',
                        ),
                      )),
                    ],
                  ),
                ),
                if (inventoryId == null || inventoryId.isEmpty)
                  const Center(
                    child: Text(
                      'Empty inventory, setup first',
                      style: TextStyle(color: Colors.white),
                    ),
                  )
                else
                  Expanded(
                    child: TabBarView(children: <Widget>[
                      SeedStockComponent(
                        inventoryId: inventoryId,
                      ),
                      DeviceStockComponent(
                        inventoryId: inventoryId,
                      ),
                      const Center(
                          child: Text(
                        'harvest',
                        style: TextStyle(color: Colors.white),
                      )),
                    ]),
                  ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              FlatButton(
                color: Colors.green,
                onPressed: () async => await showDialog<dynamic>(
                    context: context,
                    builder: (BuildContext context) => AddSeedStockDialog(
                          inventoryId: inventoryId,
                        )),
                child: const Text(
                  '+ 🌰',
                  style: TextStyle(color: Colors.white, fontFamily: 'EmojiOne'),
                ),
              ),
              FlatButton(
                color: Colors.red,
                onPressed: () async => await showDialog<dynamic>(
                    context: context,
                    builder: (BuildContext context) => AddDeviceStockDialog(
                          inventoryId: inventoryId,
                        )),
                child: const Text(
                  '+ 🧰',
                  style: TextStyle(color: Colors.white, fontFamily: 'EmojiOne'),
                ),
              ),
              FlatButton(
                color: Colors.pink,
                onPressed: () => print('add harvest'),
                child: const Text(
                  '+ 🦄',
                  style: TextStyle(color: Colors.white, fontFamily: 'EmojiOne'),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
