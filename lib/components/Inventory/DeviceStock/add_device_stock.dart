import 'package:flutter/material.dart';
import 'package:growyapp/components/Device/device_select_list.dart';
import 'package:growyapp/generated/graphql_api.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class AddDeviceStockDialog extends StatefulWidget {
  const AddDeviceStockDialog({
    Key key,
    this.inventoryId,
  }) : super(key: key);
  final String inventoryId;

  @override
  _AddDeviceStockDialogState createState() => _AddDeviceStockDialogState();
}

class _AddDeviceStockDialogState extends State<AddDeviceStockDialog> {
  int _selectedAddDeviceId = 1;
  int _addDeviceCountInput = 0;
  @override
  Widget build(BuildContext context) {
    final AddDeviceStockArguments vars = AddDeviceStockArguments(
        inventoryId: widget.inventoryId,
        input: DeviceStockInput(
            unit: 'unit',
            count: _addDeviceCountInput.toDouble(),
            deviceId: _selectedAddDeviceId));
    final AddDeviceStockMutation mut = AddDeviceStockMutation(variables: vars);
    return Mutation(
      options: MutationOptions(
          documentNode: mut.document,
          update: (Cache _, QueryResult res) {
            if (res.hasException) {
              final SnackBar snackBar =
                  SnackBar(content: Text('⚠️ ${res.exception.toString()}'));
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
              Navigator.pop(context);
            } else if (!res.loading) {
              const SnackBar snackBar = SnackBar(content: Text('Saved'));
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
              Navigator.pop(context);
            }
          }),
      builder: (RunMutation run, QueryResult result) => AlertDialog(
        title: const Text('+ Add device(s) in stock'),
        insetPadding: const EdgeInsets.all(20),
        actions: <Widget>[
          if (result.loading)
            const CircularProgressIndicator()
          else
            FlatButton.icon(
              label: const Text(
                '+',
                textAlign: TextAlign.end,
              ),
              color: Colors.green,
              icon: const Icon(Icons.save),
              onPressed: () => run(vars.toJson()),
            ),
          IconButton(
            color: Colors.grey,
            icon: const Icon(
              Icons.cancel,
            ),
            onPressed: () => Navigator.pop(context),
          ),
        ],
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            DeviceSelectListComponent(
              value: _selectedAddDeviceId,
              callback: (int strainId) {
                _selectedAddDeviceId = strainId;
                setState(() {});
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                TextButton(
                  child: const Text('-'),
                  onPressed: () => setState(() {
                    if (_addDeviceCountInput > 0) {
                      _addDeviceCountInput--;
                    }
                  }),
                ),
                Text(_addDeviceCountInput.toString()),
                TextButton(
                  child: const Text('+'),
                  onPressed: () => setState(() {
                    _addDeviceCountInput += 1;
                  }),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
