import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:growyapp/generated/graphql_api.dart';

class DeviceStockComponent extends StatefulWidget {
  const DeviceStockComponent({this.inventoryId});
  final String inventoryId;
  @override
  _DeviceStockComponentState createState() => _DeviceStockComponentState();
}

class _DeviceStockComponentState extends State<DeviceStockComponent> {
  @override
  Widget build(BuildContext context) {
    return Query(
      options: QueryOptions(
        documentNode: DeviceInventoryQuery().document,
        variables:
            DeviceInventoryArguments(inventoryId: widget.inventoryId).toJson(),
      ),
      builder: (QueryResult result,
          {Future<QueryResult> Function() refetch, FetchMore fetchMore}) {
        if (result.hasException) {
          return Center(
              child: Text(
            '⚠️ ${result.exception.toString()}',
            textAlign: TextAlign.center,
            style: const TextStyle(color: Colors.red),
          ));
        }
        if (result.loading) {
          return const Center(
            child: CircularProgressIndicator(
              backgroundColor: Colors.white,
            ),
          );
        }

        final List<DeviceInventory$Query$Inventory$DeviceStock>
            deviceStocklist = DeviceInventoryQuery()
                .parse(result.data as Map<String, dynamic>)
                .inventory
                .deviceStock;
        deviceStocklist.sort((DeviceInventory$Query$Inventory$DeviceStock a,
                DeviceInventory$Query$Inventory$DeviceStock b) =>
            a.count.compareTo(b.count));

        return RefreshIndicator(
          onRefresh: () async => await refetch(),
          child: ListView.builder(
            scrollDirection: Axis.vertical,
            padding: const EdgeInsets.all(3),
            itemBuilder: (_, int index) {
              return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListTile(
                    title: Text(
                      deviceStocklist[index].device.name,
                      textScaleFactor: 1.2,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(color: Colors.black),
                    ),
                    tileColor: Colors.white,
                    // onTap: () => Navigator.push<dynamic>(
                    //   context,
                    //   MaterialPageRoute<dynamic>(
                    //       builder: (BuildContext context) => StrainComponent(
                    //             id: seedStocklist[index].strain.id,
                    //           )),
                    // ),
                    leading: CircleAvatar(
                      backgroundColor: Colors.green,
                      child: Text(
                        deviceStocklist[index].count.toInt().toString(),
                        textScaleFactor: 1.7,
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                    trailing: IconButton(
                      icon: const Icon(Icons.edit),
                      onPressed: () => print('ok'),
                    ),
                  ));
            },
            itemCount: deviceStocklist.length,
          ),
        );
      },
    );
  }
}
