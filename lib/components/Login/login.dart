import 'dart:io';

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:growyapp/jwt.dart';
import 'package:growyapp/main.dart';
import 'package:growyapp/push_messaging.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import 'package:sembast_web/sembast_web.dart';
import 'package:growyapp/generated/graphql_api.dart';

@immutable
class LoginPage extends StatefulWidget {
  const LoginPage({Key key, this.logedInNotify}) : super(key: key);
  final Function() logedInNotify;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Database db;
  String fcmToken;
  Jwt tokenDetails;
  String growyUsername;
  String growyPassword;
  MyHomePage parent;

  bool _loading = false;
  static const String dbPath = 'growyapp.db';
  final StoreRef<String, String> store = StoreRef<String, String>.main();
  final TextEditingController _userController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  Future<void> getUserSavedCreds() async {
    if (!kIsWeb) {
      final Directory appDocDir = await getApplicationDocumentsDirectory();
      final DatabaseFactory dbFactory = databaseFactoryIo;
      db = await dbFactory.openDatabase(appDocDir.path + '/' + dbPath);
      if (!Platform.isLinux) {
        fcmToken = await PushNotificationsManager().token;
      }
    } else {
      final DatabaseFactory dbFactory = databaseFactoryWeb;
      db = await dbFactory.openDatabase(dbPath);
    }
    growyUsername = await store.record('growy-username').get(db);
    growyPassword = await store.record('growy-password').get(db);
    _userController.text = growyUsername;
    _passwordController.text = growyPassword;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getUserSavedCreds();
  }

  Future<bool> updateFcmToken(GraphQLClient client) async {
    bool success = false;
    if (tokenDetails == null || fcmToken == null || fcmToken.isEmpty) {
      print('null tokendetails ...');
      return true;
    }
    try {
      final QueryResult res = await client.mutate(MutationOptions(
          documentNode: UpdateFcmTokenMutation().document,
          variables: UpdateFcmTokenArguments(
                  userId: tokenDetails.audience, fcmToken: fcmToken)
              .toJson()));
      final UpdateFcmToken$Mutation$User tr =
          UpdateFcmToken$Mutation$User.fromJson(
              res.data as Map<String, dynamic>);
      print(tr.id);
      success = true;
    } catch (e) {
      print(e.toString());
    }
    return success;
  }

  Future<bool> doLogin(
      GraphQLClient client, String user, String password) async {
    bool success = false;
    try {
      final QueryResult res = await client.query(QueryOptions(
          fetchPolicy: FetchPolicy.noCache,
          errorPolicy: ErrorPolicy.none,
          variables: LoginArguments(name: user, password: password).toJson(),
          documentNode: LoginQuery().document));
      if (!res.hasException && !res.loading) {
        final Login$Query$Session tr =
            LoginQuery().parse(res.data as Map<String, dynamic>).login;
        tokenDetails = Jwt.fromToken(tr.token);
        await store.record('jwt').put(db, tr.token);
        await store.record('inventory_id').put(db, tr.user.inventory.id);
        await store.record('growy-userid').put(db, tokenDetails.audience);
        await store.record('growy-useremail').put(db, tr.user.email);
        success = true;

        setState(() {});
      } else if (res.hasException) {
        throw res.exception;
      }
    } catch (e) {
      print(e.toString());
    }
    return success;
  }

  Future<bool> login(GraphQLClient client) async {
    if (!kIsWeb) {
      final Directory appDocDir = await getApplicationDocumentsDirectory();
      final DatabaseFactory dbFactory = databaseFactoryIo;
      db = await dbFactory.openDatabase(appDocDir.path + '/' + dbPath);
    } else {
      final DatabaseFactory dbFactory = databaseFactoryWeb;
      db = await dbFactory.openDatabase('growyapp.db');
    }
    if (_userController.text.isNotEmpty) {
      await store.record('growy-username').put(db, _userController.text);
      growyUsername = _userController.text;
    }
    if (_passwordController.text.isNotEmpty) {
      await store.record('growy-password').put(db, _passwordController.text);
      growyPassword = _passwordController.text;
    }
    return await doLogin(client, growyUsername, growyPassword);
  }

  Future<void> logout() async {
    if (!kIsWeb) {
      final Directory appDocDir = await getApplicationDocumentsDirectory();
      final DatabaseFactory dbFactory = databaseFactoryIo;
      db = await dbFactory.openDatabase(appDocDir.path + '/' + dbPath);
    } else {
      final DatabaseFactory dbFactory = databaseFactoryWeb;
      db = await dbFactory.openDatabase(dbPath);
    }
    await store.record('growy-username').delete(db);
    await store.record('growy-password').delete(db);
    await store.record('growy-useremail').delete(db);
    await store.record('jwt').delete(db);
    growyUsername = '';
    growyPassword = '';
    _userController.clear();
    _passwordController.clear();
    setState(() {});
    return;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        // direction: Axis.vertical,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 10, left: 10),
            child: TextField(
              style: TextStyle(color: Theme.of(context).accentColor),
              controller: _userController,
              decoration: InputDecoration(
                hintStyle: const TextStyle(color: Colors.white),
                hintText: 'Enter username',
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    width: 3,
                    color: Colors.green,
                    style: BorderStyle.solid,
                  ),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                prefixIcon: const Icon(
                  Icons.verified_user_sharp,
                  color: Colors.white,
                ),
              ),
              obscureText: false,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              style: TextStyle(color: Theme.of(context).accentColor),
              controller: _passwordController,
              decoration: InputDecoration(
                hintStyle: const TextStyle(color: Colors.white),
                hintText: 'Enter password',
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    width: 3,
                    color: Colors.green,
                    style: BorderStyle.solid,
                  ),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                prefixIcon: const Icon(
                  Icons.lock,
                  color: Colors.white,
                ),
              ),
              obscureText: true,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                GraphQLConsumer(
                  builder: (GraphQLClient client) {
                    return _loading
                        ? const CircularProgressIndicator()
                        : FlatButton.icon(
                            color: Colors.green,
                            icon: const Icon(Icons.check),
                            onPressed: () async {
                              setState(() {
                                _loading = true;
                              });
                              FocusScope.of(context).unfocus();
                              bool res = await login(client);
                              final SnackBar snackBar = SnackBar(
                                  duration: const Duration(seconds: 2),
                                  content: Text(res
                                      ? 'Welcome $growyUsername 👋'
                                      : 'Oh oh ... invalid credential or server error, try again later'));
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackBar);
                              if (res) {
                                res = await updateFcmToken(client);
                                widget.logedInNotify();
                              }
                              setState(() {
                                _loading = false;
                              });
                            },
                            label: const Text(
                              'Login',
                              style: TextStyle(color: Colors.white),
                            ),
                          );
                  },
                ),
                FlatButton.icon(
                  color: Colors.grey,
                  icon: const Icon(Icons.clear),
                  onPressed: () async {
                    FocusScope.of(context).unfocus();
                    setState(() {
                      _passwordController.text = '';
                      _userController.text = '';
                    });
                    await logout();
                    const SnackBar snackBar =
                        SnackBar(content: Text('See you !'));
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  },
                  label: const Text(
                    'Logout',
                    style: TextStyle(color: Colors.white),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
