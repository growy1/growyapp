import 'package:flutter/material.dart';
import 'package:growyapp/components/Inventory/inventory_screen.dart';
import 'package:growyapp/generated/graphql_api.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class DeviceSelectListComponent extends StatefulWidget {
  const DeviceSelectListComponent({this.callback, this.value});
  final IntCallback callback;
  final int value;

  @override
  _DeviceSelectListComponentState createState() =>
      _DeviceSelectListComponentState();
}

class _DeviceSelectListComponentState extends State<DeviceSelectListComponent> {
  _DeviceSelectListComponentState();
  int _selectedId = 1;
  @override
  void initState() {
    super.initState();
    setState(() {
      _selectedId = widget.value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Query(
      options: QueryOptions(documentNode: DevicesQuery().document),
      builder: (QueryResult result,
          {Future<QueryResult> Function() refetch, FetchMore fetchMore}) {
        if (result.hasException) {
          return Center(
            child: Text(
              '⚠️ ${result.exception.toString()}',
              textAlign: TextAlign.center,
              style: const TextStyle(color: Colors.red),
            ),
          );
        }
        if (result.loading) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        final List<Devices$Query$Device> allDevices =
            DevicesQuery().parse(result.data as Map<String, dynamic>).devices;
        return DropdownButton<int>(
          value: _selectedId,
          isExpanded: true,
          onChanged: (int e) => setState(() {
            _selectedId = e;
            widget.callback(e);
          }),
          items: allDevices
              .map((Devices$Query$Device e) => DropdownMenuItem<int>(
                    value: e.id,
                    child: Text(
                      e.name,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ))
              .toList(),
        );
      },
    );
  }
}
