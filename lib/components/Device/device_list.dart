import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:growyapp/generated/graphql_api.dart';

class DeviceListComponent extends StatefulWidget {
  const DeviceListComponent({this.growingSessionId});
  final String growingSessionId;
  @override
  _DeviceListComponentState createState() => _DeviceListComponentState();
}

class _DeviceListComponentState extends State<DeviceListComponent> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: Query(
        options: QueryOptions(
          documentNode: GrowingSessionDevicesQuery().document,
          variables: GrowingSessionDevicesArguments(
                  growingSessionId: widget.growingSessionId)
              .toJson(),
        ),
        builder: (QueryResult result,
            {Future<QueryResult> Function() refetch, FetchMore fetchMore}) {
          if (result.hasException) {
            return Text(result.exception.toString());
          }
          if (result.loading) {
            return const Center(
              child: CircularProgressIndicator(
                backgroundColor: Colors.white,
              ),
            );
          }
          final List<GrowingSessionDevices$Query$Device> devicelist =
              GrowingSessionDevicesQuery()
                  .parse(result.data as Map<String, dynamic>)
                  .devices;
          return RefreshIndicator(
            onRefresh: () async => await refetch(),
            child: ListView.builder(
              scrollDirection: Axis.vertical,
              padding: const EdgeInsets.all(3),
              itemBuilder: (_, int index) {
                return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListTile(
                      tileColor: Colors.white,
                      trailing: Text(
                          devicelist[index].type.toString().split('.').last),
                      title: Text(devicelist[index].name),
                    ));
              },
              itemCount: devicelist.length,
            ),
          );
        },
      ),
    );
  }
}
