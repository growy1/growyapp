import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import 'package:http/http.dart' as http;
import 'package:sembast_web/sembast_web.dart';

@immutable
class CredentialPage extends StatefulWidget {
  const CredentialPage({Key key}) : super(key: key);

  @override
  _CredentialPageState createState() => _CredentialPageState();
}

class _CredentialPageState extends State<CredentialPage> {
  Database db;
  String adminToken;
  String appUsername;
  String appPassword;
  bool _adminMode = false;
  bool _isloading = false;
  static const String dbPath = 'growyapp.db';
  final StoreRef<String, String> store = StoreRef<String, String>.main();
  final TextEditingController _userController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _adminapikeyController = TextEditingController();

  Future<void> getUserSavedCreds() async {
    const String dbPath = 'growyapp.db';
    if (!kIsWeb) {
      final Directory appDocDir = await getApplicationDocumentsDirectory();
      final DatabaseFactory dbFactory = databaseFactoryIo;
      db = await dbFactory.openDatabase(appDocDir.path + '/' + dbPath);
    } else {
      final DatabaseFactory dbFactory = databaseFactoryWeb;
      db = await dbFactory.openDatabase(dbPath);
    }
    appUsername = await store.record('username').get(db);
    appPassword = await store.record('password').get(db);
    adminToken = await store.record('adminapikey').get(db);
    _userController.text = appUsername;
    _passwordController.text = appPassword;
    _adminapikeyController.text = adminToken;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getUserSavedCreds();
  }

  Future<void> setcredentials() async {
    if (!kIsWeb) {
      final Directory appDocDir = await getApplicationDocumentsDirectory();
      final DatabaseFactory dbFactory = databaseFactoryIo;
      db = await dbFactory.openDatabase(appDocDir.path + '/' + dbPath);
    } else {
      final DatabaseFactory dbFactory = databaseFactoryWeb;
      // Open the database
      db = await dbFactory.openDatabase('growyapp.db');
    }
    if (_userController.text.isNotEmpty) {
      await store.record('username').put(db, _userController.text);
      appUsername = _userController.text;
    }
    if (_passwordController.text.isNotEmpty) {
      await store.record('password').put(db, _passwordController.text);
      appPassword = _passwordController.text;
    }
    if (_adminMode && _adminapikeyController.text.isNotEmpty) {
      await store.record('adminapikey').put(db, _adminapikeyController.text);
      adminToken = _adminapikeyController.text;
    }
    if (_passwordController.text.isNotEmpty &&
        _userController.text.isNotEmpty) {
      final String picAuth =
          'Basic ' + base64Encode(utf8.encode('$appUsername:$appPassword'));
      await store.record('basicauth').put(db, picAuth);
    }
    return;
  }

  Future<void> clearcredentials() async {
    if (!kIsWeb) {
      final Directory appDocDir = await getApplicationDocumentsDirectory();
      final DatabaseFactory dbFactory = databaseFactoryIo;
      db = await dbFactory.openDatabase(appDocDir.path + '/' + dbPath);
    } else {
      final DatabaseFactory dbFactory = databaseFactoryWeb;
      db = await dbFactory.openDatabase(dbPath);
    }
    await store.record('username').delete(db);
    await store.record('password').delete(db);
    await store.record('basicauth').delete(db);
    if (_adminMode) {
      await store.record('adminapikey').delete(db);
      adminToken = '';
      _adminapikeyController.clear();
    }
    appUsername = '';
    appPassword = '';
    _userController.clear();
    _passwordController.clear();
    return;
  }

  Future<bool> restartService(String serviceName) async {
    if (!kIsWeb) {
      final Directory appDocDir = await getApplicationDocumentsDirectory();
      final DatabaseFactory dbFactory = databaseFactoryIo;
      db = await dbFactory.openDatabase(appDocDir.path + '/' + dbPath);
    } else {
      final DatabaseFactory dbFactory = databaseFactoryWeb;
      db = await dbFactory.openDatabase(dbPath);
    }
    final String apiKey = await store.record('adminapikey').get(db);
    final http.Response resp = await http.put(
      'https://system.aerogrow.org/api/v1/restart',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': apiKey,
      },
      body: jsonEncode(<String, String>{
        'serviceName': serviceName,
      }),
    );
    return resp.statusCode == 200;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      // scrollDirection: Axis.horizontal,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            style: TextStyle(color: Theme.of(context).accentColor),
            controller: _userController,
            decoration: InputDecoration(
              hintStyle: const TextStyle(color: Colors.white),
              hintText: 'Enter username',
              enabledBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  width: 3,
                  color: Colors.green,
                  style: BorderStyle.solid,
                ),
                borderRadius: BorderRadius.circular(10.0),
              ),
              prefixIcon: const Icon(
                Icons.verified_user_sharp,
                color: Colors.white,
              ),
            ),
            obscureText: false,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            style: TextStyle(color: Theme.of(context).accentColor),
            controller: _passwordController,
            decoration: InputDecoration(
              hintStyle: const TextStyle(color: Colors.white),
              hintText: 'Enter password',
              enabledBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  width: 3,
                  color: Colors.green,
                  style: BorderStyle.solid,
                ),
                borderRadius: BorderRadius.circular(10.0),
              ),
              prefixIcon: const Icon(
                Icons.lock,
                color: Colors.white,
              ),
            ),
            obscureText: true,
          ),
        ),
        if (_adminMode)
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              style: TextStyle(color: Theme.of(context).accentColor),
              controller: _adminapikeyController,
              decoration: InputDecoration(
                hintStyle: const TextStyle(color: Colors.white),
                hintText: 'Enter admin api key',
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    width: 3,
                    color: Colors.white,
                    style: BorderStyle.solid,
                  ),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                prefixIcon: const Icon(
                  Icons.admin_panel_settings,
                  color: Colors.white,
                ),
              ),
              obscureText: false,
            ),
          ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            FlatButton.icon(
              color: Colors.green,
              icon: const Icon(Icons.check),
              onPressed: () async {
                FocusScope.of(context).unfocus();
                await setcredentials();
                const SnackBar snackBar =
                    SnackBar(content: Text('Credential saved'));
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              },
              label: const Text(
                'Save',
                style: TextStyle(color: Colors.white),
              ),
            ),
            Switch(
              value: _adminMode,
              onChanged: (bool v) => setState(() {
                _adminMode = v;
              }),
            ),
            FlatButton.icon(
              color: Colors.grey,
              icon: const Icon(Icons.clear),
              onPressed: () async {
                FocusScope.of(context).unfocus();
                setState(() {
                  _passwordController.text = '';
                  _userController.text = '';
                });
                await clearcredentials();
                const SnackBar snackBar =
                    SnackBar(content: Text('Credential cleared'));
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              },
              label: const Text(
                'Clear',
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ),
        if (_adminMode)
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              if (_isloading)
                const CircularProgressIndicator(
                  backgroundColor: Colors.white,
                  strokeWidth: 3,
                )
              else
                FlatButton.icon(
                  color: Colors.lightGreen,
                  icon: const Icon(Icons.refresh),
                  onPressed: () async {
                    setState(() {
                      _isloading = true;
                    });
                    final bool res = await restartService('inlets');
                    final SnackBar snackBar =
                        SnackBar(content: Text(res ? 'Done !' : 'Error...'));
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    setState(() {
                      _isloading = false;
                    });
                  },
                  label: const Text(
                    'Restart tunnel service (inlets)',
                    style: TextStyle(color: Colors.white),
                  ),
                )
            ],
          )
      ],
    );
  }
}
