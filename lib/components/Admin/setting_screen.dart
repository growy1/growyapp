import 'package:flutter/material.dart';
import 'package:growyapp/components/Admin/set_credential.dart';
import 'package:growyapp/components/Admin/user_profile.dart';

class SettingScreenComponent extends StatefulWidget {
  const SettingScreenComponent();

  @override
  _SettingScreenComponentState createState() => _SettingScreenComponentState();
}

class _SettingScreenComponentState extends State<SettingScreenComponent> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListView(
        shrinkWrap: true,
        children: const <Widget>[
          UserProfilePage(),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              'Admin',
              textScaleFactor: 1.6,
              style: TextStyle(color: Colors.white),
            ),
          ),
          CredentialPage()
        ],
      ),
    );
  }
}
