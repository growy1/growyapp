import 'dart:io';

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import 'package:sembast_web/sembast_web.dart';
import 'package:growyapp/generated/graphql_api.dart';

@immutable
class UserProfilePage extends StatefulWidget {
  const UserProfilePage({Key key}) : super(key: key);

  @override
  _UserProfilePageState createState() => _UserProfilePageState();
}

class _UserProfilePageState extends State<UserProfilePage> {
  Database db;
  // bool _adminMode = false;
  // bool _isloading = false;
  String growyEmail;
  String growyUserID;
  String growyUsername;
  String growyPassword;
  static const String dbPath = 'growyapp.db';
  final StoreRef<String, String> store = StoreRef<String, String>.main();
  final TextEditingController _userPseudoController = TextEditingController();
  final TextEditingController _userPasswordController = TextEditingController();
  final TextEditingController _userEmailController = TextEditingController();

  Future<void> getUserSavedCreds() async {
    if (!kIsWeb) {
      final Directory appDocDir = await getApplicationDocumentsDirectory();
      final DatabaseFactory dbFactory = databaseFactoryIo;
      db = await dbFactory.openDatabase(appDocDir.path + '/' + dbPath);
    } else {
      final DatabaseFactory dbFactory = databaseFactoryWeb;
      db = await dbFactory.openDatabase(dbPath);
    }
    growyUsername = await store.record('growy-username').get(db);
    growyPassword = await store.record('growy-password').get(db);
    growyUserID = await store.record('growy-userid').get(db);
    growyEmail = await store.record('growy-useremail').get(db);
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getUserSavedCreds();
  }

  @override
  Widget build(BuildContext context) {
    return Mutation(
      options: MutationOptions(
          documentNode: UpdateUserMutation().document,
          update: (Cache _, QueryResult res) {
            if (!res.loading && !res.hasException) {
              print(res.data);
              const SnackBar snackBar = SnackBar(content: Text('Updated'));
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            } else if (res.hasException) {
              print(res.exception.toString());
            }
          }),
      builder: (RunMutation run, QueryResult result) {
        return Column(
          // scrollDirection: Axis.horizontal,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    growyUsername != null ? growyUsername.toUpperCase() : '',
                    textScaleFactor: 1.3,
                    style: const TextStyle(color: Colors.white),
                  ),
                  Text(growyEmail ?? '',
                      style: const TextStyle(color: Colors.white))
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                style: TextStyle(color: Theme.of(context).accentColor),
                controller: _userPseudoController,
                decoration: InputDecoration(
                  hintStyle: const TextStyle(color: Colors.white),
                  hintText: 'Update pseudo',
                  enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                      width: 3,
                      color: Colors.green,
                      style: BorderStyle.solid,
                    ),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  prefixIcon: const Icon(
                    Icons.verified_user_sharp,
                    color: Colors.white,
                  ),
                ),
                obscureText: false,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                style: TextStyle(color: Theme.of(context).accentColor),
                controller: _userEmailController,
                decoration: InputDecoration(
                  hintStyle: const TextStyle(color: Colors.white),
                  hintText: 'Update email',
                  enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                      width: 3,
                      color: Colors.green,
                      style: BorderStyle.solid,
                    ),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  prefixIcon: const Icon(
                    Icons.email,
                    color: Colors.white,
                  ),
                ),
                obscureText: false,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                style: TextStyle(color: Theme.of(context).accentColor),
                controller: _userPasswordController,
                decoration: InputDecoration(
                  hintStyle: const TextStyle(color: Colors.white),
                  hintText: 'Update password',
                  enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                      width: 3,
                      color: Colors.green,
                      style: BorderStyle.solid,
                    ),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  prefixIcon: const Icon(
                    Icons.lock,
                    color: Colors.white,
                  ),
                ),
                obscureText: true,
              ),
            ),
            if (!result.loading)
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  FlatButton.icon(
                    color: Colors.green,
                    icon: const Icon(Icons.check),
                    onPressed: () async {
                      final Map<String, dynamic> args = UpdateUserArguments(
                          input: UpdateUserInput(
                        id: growyUserID,
                        name: _userPseudoController.text,
                        email: _userEmailController.text,
                        password: _userPasswordController.text,
                      )).toJson();
                      FocusScope.of(context).unfocus();
                      run(args);
                      // await setcredentials();
                    },
                    label: const Text(
                      'Save',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              )
            else
              const Center(child: CircularProgressIndicator())
          ],
        );
      },
    );
  }
}
