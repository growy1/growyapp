import 'package:flutter/material.dart';
import 'package:growyapp/components/Strain/strain.dart';
import 'package:growyapp/generated/graphql_api.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class StrainListComponent extends StatelessWidget {
  const StrainListComponent();

  @override
  Widget build(BuildContext context) {
    return Query(
      options: QueryOptions(documentNode: StrainsQuery().document),
      builder: (QueryResult result,
          {Future<QueryResult> Function() refetch, FetchMore fetchMore}) {
        if (result.hasException) {
          return Center(
            child: Text(
              '⚠️ ${result.exception.toString()}',
              textAlign: TextAlign.center,
              style: const TextStyle(color: Colors.red),
            ),
          );
        }
        if (result.loading) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        final List<Strains$Query$Strain> allStrains =
            StrainsQuery().parse(result.data as Map<String, dynamic>).strains;
        return Center(
          child: Column(
            children: <Widget>[
              const SizedBox(
                height: 30,
              ),
              const Card(
                  child: Text(
                'Strains Inventory',
                textScaleFactor: 3,
              )),
              Expanded(
                child: ListView.builder(
                  itemCount: allStrains.length,
                  itemBuilder: (_, int index) {
                    return ListTile(
                      leading: const Icon(
                        Icons.weekend,
                        color: Colors.white,
                      ),
                      onTap: () => Navigator.push<dynamic>(
                        context,
                        MaterialPageRoute<dynamic>(
                            builder: (BuildContext context) =>
                                StrainComponent(id: allStrains[index].id)),
                      ),
                      title: Text(
                        '${allStrains[index].name} - ${allStrains[index].photoperiod.toString().split('.').last}',
                        style: const TextStyle(color: Colors.white),
                      ),
                    );
                  },
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
