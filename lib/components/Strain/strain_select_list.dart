import 'package:flutter/material.dart';
import 'package:growyapp/components/Inventory/inventory_screen.dart';
import 'package:growyapp/generated/graphql_api.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class StrainSelectListComponent extends StatefulWidget {
  const StrainSelectListComponent({this.callback, this.value});
  final IntCallback callback;
  final int value;

  @override
  _StrainSelectListComponentState createState() =>
      _StrainSelectListComponentState();
}

class _StrainSelectListComponentState extends State<StrainSelectListComponent> {
  _StrainSelectListComponentState();
  int _selectedId = 1;
  @override
  void initState() {
    super.initState();
    setState(() {
      _selectedId = widget.value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Query(
      options: QueryOptions(documentNode: StrainsQuery().document),
      builder: (QueryResult result,
          {Future<QueryResult> Function() refetch, FetchMore fetchMore}) {
        if (result.hasException) {
          return Center(
            child: Text(
              '⚠️ ${result.exception.toString()}',
              textAlign: TextAlign.center,
              style: const TextStyle(color: Colors.red),
            ),
          );
        }
        if (result.loading) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        final List<Strains$Query$Strain> allStrains =
            StrainsQuery().parse(result.data as Map<String, dynamic>).strains;
        return DropdownButton<int>(
          value: _selectedId,
          onChanged: (int e) => setState(() {
            _selectedId = e;
            widget.callback(e);
          }),
          items: allStrains
              .map((Strains$Query$Strain e) => DropdownMenuItem<int>(
                    value: e.id,
                    child: Text(e.name),
                  ))
              .toList(),
        );
      },
    );
  }
}
