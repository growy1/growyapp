import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:growyapp/generated/graphql_api.dart';
import 'package:url_launcher/url_launcher.dart';

class StrainComponent extends StatefulWidget {
  const StrainComponent({@required this.id});
  final int id;

  @override
  _StrainComponentState createState() => _StrainComponentState();
}

class _StrainComponentState extends State<StrainComponent> {
  String convertFloweringTimes(double ftime) =>
      '$ftime days - ${(ftime.toInt() ~/ 7).toInt()} weeks - ${(ftime.toInt() / 30).toStringAsPrecision(2)} month';
  List<Widget> _tabs(
          List<Strain$Query$Strain$StrainTypeInfos> types,
          List<Strain$Query$Strain$Cannabinoid> cannabinoids,
          List<Strain$Query$Strain$Measure> ftimes,
          Future<QueryResult> Function() refetch) =>
      <Widget>[
        ListView.builder(
          itemCount: types.length,
          itemBuilder: (_, int index) {
            return ListTile(
              tileColor: Colors.black,
              leading: CircleAvatar(
                child: types[index].type == StrainType.sativa
                    ? const Text(
                        '🎉',
                        style: TextStyle(
                          fontFamily: 'EmojiOne',
                        ),
                      )
                    : const Text('😴',
                        style: TextStyle(
                          fontFamily: 'EmojiOne',
                        )),
              ),
              trailing: Wrap(
                children: <Widget>[
                  Mutation(
                      options: MutationOptions(
                        documentNode: DeleteStrainTypeInfosMutation().document,
                        update: (Cache _, QueryResult res) {
                          if (!res.loading &&
                              res.data is Map<String, dynamic>) {
                            final DeleteStrainTypeInfos$Mutation$StrainTypeInfos
                                response = DeleteStrainTypeInfosMutation()
                                    .parse(res.data as Map<String, dynamic>)
                                    .deletestraintypeinfos;
                            final SnackBar snackBar = SnackBar(
                                content: Text(
                                    'Successfuly deleted id ${response.id}'));
                            ScaffoldMessenger.of(context)
                                .showSnackBar(snackBar);
                            refetch();
                          } else if (res.hasException) {
                            final SnackBar snackBar = SnackBar(
                                backgroundColor: Colors.red,
                                content:
                                    Text('url : ${res.exception.toString()}'));
                            ScaffoldMessenger.of(context)
                                .showSnackBar(snackBar);
                          }
                        },
                      ),
                      builder: (RunMutation run, QueryResult result) {
                        final DeleteStrainTypeInfosArguments vars =
                            DeleteStrainTypeInfosArguments(
                          straintypeinfosId: types[index].id,
                          strainId: widget.id,
                        );
                        return IconButton(
                          color: Colors.grey,
                          icon: const Icon(Icons.delete_forever),
                          onPressed: () => run(vars.toJson()),
                        );
                      }),
                  IconButton(
                    color: Colors.grey,
                    icon: const Icon(Icons.edit),
                    onPressed: () {
                      print('ok');
                    },
                  ),
                ],
              ),
              title: Text(
                  types[index].type.toString().split('.').last.toUpperCase(),
                  style: const TextStyle(color: Colors.white)),
              subtitle: Text('${types[index].percentage ?? 'x'} %',
                  style: const TextStyle(color: Colors.white)),
            );
          },
        ),
        ListView.builder(
          itemCount: cannabinoids.length,
          itemBuilder: (_, int index) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListTile(
                tileColor: Colors.black,
                leading: const CircleAvatar(
                  child: Text('🔬',
                      style: TextStyle(
                        fontFamily: 'EmojiOne',
                      )),
                ),
                trailing: Wrap(
                  children: <Widget>[
                    IconButton(
                      color: Colors.grey,
                      icon: const Icon(Icons.delete_forever),
                      onPressed: () {
                        print('ok');
                      },
                    ),
                    IconButton(
                      color: Colors.grey,
                      icon: const Icon(Icons.edit),
                      onPressed: () {
                        print('ok');
                      },
                    ),
                  ],
                ),
                title: Text(cannabinoids[index].name,
                    style: const TextStyle(color: Colors.white)),
                subtitle: Text('${cannabinoids[index].percentage} %',
                    style: const TextStyle(color: Colors.white)),
              ),
            );
          },
        ),
        ListView.builder(
          itemCount: ftimes.length,
          scrollDirection: Axis.vertical,
          itemBuilder: (_, int index) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListTile(
                tileColor: Colors.black,
                leading: const CircleAvatar(
                  child: Text('🌻',
                      style: TextStyle(
                        fontFamily: 'EmojiOne',
                      )),
                ),
                trailing: Wrap(
                  children: <Widget>[
                    IconButton(
                      color: Colors.grey,
                      icon: const Icon(Icons.delete_forever),
                      onPressed: () {
                        print('ok');
                      },
                    ),
                    IconButton(
                      color: Colors.grey,
                      icon: const Icon(Icons.edit),
                      onPressed: () {
                        print('ok');
                      },
                    ),
                  ],
                ),
                title: Text(convertFloweringTimes(ftimes[index].value),
                    style: const TextStyle(color: Colors.white)),
              ),
            );
          },
        )
      ];
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Query(
        options: QueryOptions(
            fetchPolicy: FetchPolicy.cacheAndNetwork,
            variables: <String, int>{'id': widget.id},
            documentNode: StrainQuery().document),
        builder: (QueryResult result,
            {Future<QueryResult> Function() refetch, FetchMore fetchMore}) {
          if (result.hasException) {
            return Text(result.exception.toString());
          }
          if (result.loading) {
            return const Scaffold(
              backgroundColor: Colors.black,
              body: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.white,
                ),
              ),
            );
          }
          final Strain$Query$Strain strain =
              StrainQuery().parse(result.data as Map<String, dynamic>).strain;
          final List<Strain$Query$Strain$StrainTypeInfos> types = strain.type;
          final List<Strain$Query$Strain$Cannabinoid> cannabinoids =
              strain.cannabinoids;
          final List<Strain$Query$Strain$Measure> ftimes =
              strain.floweringTimes;
          return Scaffold(
            backgroundColor: Colors.black,
            bottomNavigationBar: kIsWeb || (!kIsWeb && Platform.isLinux)
                ? BottomAppBar(
                    color: Colors.black,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        IconButton(
                          color: Colors.green,
                          icon: const Icon(Icons.arrow_back),
                          onPressed: () => Navigator.of(context).pop(),
                        ),
                      ],
                    ),
                  )
                : null,
            body: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                const SizedBox(
                  height: 30,
                ),
                Container(
                  padding: const EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.green, width: 3),
                  ),
                  child: Column(
                    children: <Widget>[
                      Text(
                        strain.name,
                        textScaleFactor: 3,
                        textAlign: TextAlign.center,
                        style: const TextStyle(color: Colors.white),
                      ),
                      Text(
                        strain.genetic,
                        style: const TextStyle(color: Colors.white),
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                ListTile(
                    tileColor: Colors.black,
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          strain.seedBank.name,
                          textScaleFactor: 1.2,
                          style: const TextStyle(
                            color: Colors.white,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                        RichText(
                            text: TextSpan(
                          children: <InlineSpan>[
                            TextSpan(
                              text: 'From ${strain.supplier} - ',
                              style: const TextStyle(color: Colors.white),
                            ),
                            TextSpan(
                              text: '🌎',
                              style: const TextStyle(fontFamily: 'EmojiOne'),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () => launch(strain.link),
                            )
                          ],
                        )),
                      ],
                    ),
                    leading: Image.network(
                      strain.seedBank.logoUrl,
                    )),
                if (strain.seedFinderStrainInfo != null)
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                            color: Colors.white,
                            height: MediaQuery.of(context).size.height / 2.5,
                            padding: const EdgeInsets.all(10),
                            child: SingleChildScrollView(
                              child: Html(
                                  shrinkWrap: true,
                                  data: strain.seedFinderStrainInfo
                                      .sfStrainHtmlDescription),
                            )),
                        const Padding(
                          padding: EdgeInsets.all(10),
                          child: Text(
                            '* From SeedFinder',
                            style: TextStyle(color: Colors.white),
                          ),
                        )
                      ],
                    ),
                  ),
                Expanded(
                  child: DefaultTabController(
                    length: 3,
                    child: Scaffold(
                      backgroundColor: Colors.black,
                      appBar: AppBar(
                        leading: Container(),
                        backgroundColor: Colors.black,
                        flexibleSpace: const TabBar(
                          tabs: <Widget>[
                            Tab(
                                icon: Text('🌴',
                                    style: TextStyle(
                                      fontFamily: 'EmojiOne',
                                    ))),
                            Tab(
                                icon: Text('🔬',
                                    style: TextStyle(
                                      fontFamily: 'EmojiOne',
                                    ))),
                            Tab(
                                icon: Text('🌻',
                                    style: TextStyle(
                                      fontFamily: 'EmojiOne',
                                    ))),
                          ],
                        ),
                      ),
                      body: TabBarView(
                          children:
                              _tabs(types, cannabinoids, ftimes, refetch)),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
