import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:growyapp/components/GrowingSession/Plants/plant.dart';
import 'package:growyapp/components/Strain/strain.dart';
import 'package:growyapp/generated/graphql_api.dart';

class PlantListComponent extends StatefulWidget {
  const PlantListComponent({this.plantlist, this.refetch});
  final List<GrowingSession$Query$GrowingSession$Plant> plantlist;
  final Future<QueryResult> Function() refetch;

  @override
  _PlantListComponentState createState() => _PlantListComponentState();
}

class _PlantListComponentState extends State<PlantListComponent> {
  @override
  void initState() {
    super.initState();
    widget.plantlist.sort((GrowingSession$Query$GrowingSession$Plant a,
            GrowingSession$Query$GrowingSession$Plant b) =>
        b.name.compareTo(a.name));
  }

  Widget getplantleadingtile(String plantName, String strainName) {
    return CircleAvatar(
      radius: 60,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            plantName,
            textScaleFactor: 0.9,
            textAlign: TextAlign.center,
            style: const TextStyle(color: Colors.white),
          ),
          Text(
            strainName,
            textScaleFactor: 0.6,
            textAlign: TextAlign.center,
            style: const TextStyle(color: Colors.white),
          ),
        ],
      ),
    );
  }

  Widget getplanttrailingtile(int strainID) {
    return FloatingActionButton(
      heroTag: null,
      onPressed: () => Navigator.push<dynamic>(
        context,
        MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => StrainComponent(
                  id: strainID,
                )),
      ),
      child: const Text(
        '🌿',
        style: TextStyle(fontFamily: 'EmojiOne', fontSize: 20),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: widget.plantlist.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount:
              (MediaQuery.of(context).orientation == Orientation.portrait)
                  ? 2
                  : 3),
      itemBuilder: (BuildContext ctx, int index) {
        return Card(
          borderOnForeground: true,
          color: Colors.green,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: getplantleadingtile(widget.plantlist[index].name,
                    widget.plantlist[index].strain.name),
              ),
              Flexible(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    getplanttrailingtile(widget.plantlist[index].strain.id),
                    FloatingActionButton(
                      heroTag: null,
                      child: (widget.plantlist[index].endedAt is! DateTime)
                          ? const Text(
                              '📜',
                              style: TextStyle(
                                  fontFamily: 'EmojiOne', fontSize: 20),
                            )
                          : const Text(
                              '🔴',
                              style: TextStyle(
                                  fontFamily: 'EmojiOne', fontSize: 20),
                            ),
                      onPressed: () =>
                          widget.plantlist[index].endedAt is DateTime
                              ? ScaffoldMessenger.of(ctx).showSnackBar(SnackBar(
                                  duration: const Duration(seconds: 2),
                                  content: Text(
                                      'Sorry plant ended ${DateTime.now().difference(widget.plantlist[index].endedAt).inDays} days ago, no status available'),
                                ))
                              : Navigator.push<dynamic>(
                                  ctx,
                                  MaterialPageRoute<dynamic>(
                                      builder: (BuildContext context) =>
                                          PlantComponent(
                                            id: widget.plantlist[index].id,
                                          )),
                                ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 5,
              )
            ],
          ),
        );
      },
    );
  }
}
