import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:growyapp/generated/graphql_api.dart';

class PlantStateInputComponent extends StatefulWidget {
  const PlantStateInputComponent({@required this.id});
  final String id;

  @override
  _PlantStateInputComponent createState() => _PlantStateInputComponent();
}

class _PlantStateInputComponent extends State<PlantStateInputComponent> {
  AddGrowStateMutation foo;
  final TextEditingController controller = TextEditingController();
  DateTime startDate = DateTime.now();
  DevelopmentStateType dropdownValue = DevelopmentStateType.seeding;
  // DateTime endDate = DateTime.now();
  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: startDate,
        firstDate: DateTime(2018),
        lastDate: DateTime.now());
    print(picked.toString());
    if (picked != null && picked != startDate)
      setState(() {
        startDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          const Text('New state'),
          RaisedButton(
            onPressed: () => _selectDate(context),
            child: const Text('Select start date'),
          ),
          RaisedButton(
            onPressed: () => _selectDate(context),
            child: const Text('Select end date'),
          ),
          Row(
            children: <Widget>[
              const Padding(
                padding: EdgeInsets.only(top: 3),
                child: Text(
                  'State',
                  textScaleFactor: 1.1,
                ),
              ),
              Expanded(child: Container()),
              DropdownButton<DevelopmentStateType>(
                  value: dropdownValue,
                  items: DevelopmentStateType.values
                      .map<DropdownMenuItem<DevelopmentStateType>>(
                          (DevelopmentStateType e) {
                    return DropdownMenuItem<DevelopmentStateType>(
                        value: e, child: Text(e.toString().split('.').last));
                  }).toList(),
                  onChanged: (DevelopmentStateType e) {
                    setState(() {});
                  }),
            ],
          ),
          Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const Text(
                'Comments :',
                textScaleFactor: 1.1,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8, right: 8, bottom: 8),
                child: TextField(
                  controller: controller,
                ),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () {
                  foo = AddGrowStateMutation(
                      variables: AddGrowStateArguments(
                          plantId: widget.id,
                          input: GrowStateInput(
                              developmentState: dropdownValue,
                              startedAt: startDate)));
                  Mutation(
                      options: MutationOptions(
                          documentNode: foo.document,
                          variables: foo.variables.toJson()),
                      builder: (MultiSourceResult Function(Map<String, dynamic>,
                                  {Object optimisticResult})
                              result,
                          QueryResult foo) {
                        if (foo.hasException) {
                          return Text(foo.exception.toString());
                        }
                        if (foo.loading) {
                          return const Scaffold(
                            backgroundColor: Colors.green,
                            body: Center(
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.white,
                              ),
                            ),
                          );
                        }
                        return Text(foo.data.toString());
                      });
                },
                child: const Text('Add'),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, 'ko');
                },
                child: const Text('Cancel'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
