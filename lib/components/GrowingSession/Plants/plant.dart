import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:growyapp/generated/graphql_api.dart';

class PlantComponent extends StatefulWidget {
  const PlantComponent({@required this.id});
  final String id;

  @override
  _PlantComponentState createState() => _PlantComponentState();
}

class _PlantComponentState extends State<PlantComponent> {
  String plannedEndDate(Plant$Query$Plant plant, double floweringTime) {
    final DateTime endDate = DateTime.now().add(Duration(
        days:
            (floweringTime - DateTime.now().difference(plant.startedAt).inDays)
                .toInt()));
    return DateFormat('E dd MMM yyyy').format(endDate.toLocal());
  }

  int plantRunningDays(Plant$Query$Plant plant) =>
      DateTime.now().difference(plant.startedAt).inDays;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      bottomNavigationBar: kIsWeb || (!kIsWeb && Platform.isLinux)
          ? BottomAppBar(
              color: Colors.black,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  IconButton(
                    color: Colors.green,
                    icon: const Icon(Icons.arrow_back),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                ],
              ),
            )
          : null,
      body: Query(
        options: QueryOptions(
            pollInterval: 3 * 10000000,
            variables: <String, String>{'id': widget.id},
            documentNode: PlantQuery().document),
        builder: (QueryResult result,
            {Future<QueryResult> Function() refetch, FetchMore fetchMore}) {
          if (result.hasException) {
            return Center(child: Text(result.exception.toString()));
          }
          if (result.loading) {
            return const Center(
              child: CircularProgressIndicator(
                backgroundColor: Colors.white,
              ),
            );
          }
          final Plant$Query$Plant plant =
              PlantQuery().parse(result.data as Map<String, dynamic>).plant;
          final List<Plant$Query$Plant$GrowState> plantStates = plant.states;

          plant.strain.floweringTimes.sort((Plant$Query$Plant$Strain$Measure a,
                  Plant$Query$Plant$Strain$Measure b) =>
              a.value.compareTo(b.value));
          return Column(
            children: <Widget>[
              const SizedBox(
                height: 30,
              ),
              Container(
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.green),
                ),
                child: Column(
                  children: <Widget>[
                    Text(
                      '${plant.name} - ${plant.strain.name}',
                      textScaleFactor: 1.6,
                      style: const TextStyle(color: Colors.white),
                    ),
                    Text(
                      'from ${plant.strain.seedBank.name}',
                      textScaleFactor: 1.3,
                      style: const TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 22,
              ),
              Expanded(
                child: DefaultTabController(
                    length: 2,
                    child: Column(
                      children: <Widget>[
                        AppBar(
                          leading: Container(),
                          backgroundColor: Colors.black,
                          flexibleSpace: const TabBar(
                            tabs: <Widget>[
                              Tab(
                                icon: Text(
                                  '⌚ Progress',
                                  textScaleFactor: 1.3,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'EmojiOne',
                                  ),
                                ),
                              ),
                              Tab(
                                  icon: Text(
                                '📖 States',
                                textScaleFactor: 1.3,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'EmojiOne',
                                ),
                              )),
                            ],
                          ),
                        ),
                        Expanded(
                          child: TabBarView(children: <Widget>[
                            ListView.builder(
                              itemCount: plant.strain.floweringTimes.length,
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemBuilder: (BuildContext context, int index) {
                                return Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Stack(
                                        alignment: Alignment.center,
                                        children: <Widget>[
                                          Column(
                                            children: <Widget>[
                                              Text(
                                                '${plantRunningDays(plant)} / ${plant.strain.floweringTimes[index].value} ${plant.strain.floweringTimes[index].unit}',
                                                style: const TextStyle(
                                                    color: Colors.white),
                                              ),
                                              Text(
                                                '${plant.strain.floweringTimes[index].value - DateTime.now().difference(plant.startedAt).inDays} days left',
                                                textScaleFactor: 1.4,
                                                style: const TextStyle(
                                                    color: Colors.white),
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            height: 150,
                                            width: 150,
                                            child: CircularProgressIndicator(
                                              backgroundColor: Colors.white,
                                              strokeWidth: 6,
                                              value: DateTime.now()
                                                      .difference(
                                                          plant.startedAt)
                                                      .inDays
                                                      .toDouble() /
                                                  plant
                                                      .strain
                                                      .floweringTimes[index]
                                                      .value,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          'Estimated end date : ${plannedEndDate(plant, plant.strain.floweringTimes[index].value)}',
                                          style: const TextStyle(
                                              color: Colors.white),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            ),
                            ListView.builder(
                              itemBuilder: (_, int index) {
                                return ListTile(
                                  leading: Column(
                                    children: <Widget>[
                                      const Icon(
                                        Icons.sentiment_very_satisfied,
                                        color: Colors.green,
                                      ),
                                      Text(
                                        plantStates[index]
                                            .developmentState
                                            .toString()
                                            .split('.')
                                            .last
                                            .toUpperCase(),
                                        style: const TextStyle(
                                            color: Colors.white),
                                      ),
                                    ],
                                  ),
                                  title: Column(
                                    children: <Widget>[
                                      Text(
                                        plantStates[index]
                                            .startedAt
                                            .toLocal()
                                            .toString(),
                                        style: const TextStyle(
                                            color: Colors.white),
                                      ),
                                      const Text(
                                        'to',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      Text(
                                        plantStates[index]
                                            .endedAt
                                            .toLocal()
                                            .toString(),
                                        style: const TextStyle(
                                            color: Colors.white),
                                      ),
                                    ],
                                  ),
                                );
                              },
                              itemCount: plantStates.length,
                            )
                          ]),
                        )
                      ],
                    )),
              ),
            ],
          );
        },
      ),
    );
  }
}
