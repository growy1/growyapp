import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:growyapp/components/GrowingSession/add_growing_session.dart';
import 'package:growyapp/components/GrowingSession/growing_session.dart';
import 'package:growyapp/generated/graphql_api.dart';
import 'package:growyapp/jwt.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import 'package:sembast_web/sembast_web.dart';

class GrowingSessionsComponent extends StatefulWidget {
  const GrowingSessionsComponent();

  @override
  _GrowingSessionsComponentState createState() =>
      _GrowingSessionsComponentState();
}

class _GrowingSessionsComponentState extends State<GrowingSessionsComponent> {
  Database db;
  bool _addOne = false;
  String userID;
  final StoreRef<String, String> store = StoreRef<String, String>.main();

  Future<void> getUserID() async {
    String jwt;
    const String dbPath = 'growyapp.db';
    if (!kIsWeb) {
      final Directory appDocDir = await getApplicationDocumentsDirectory();
      final DatabaseFactory dbFactory = databaseFactoryIo;
      db = await dbFactory.openDatabase(appDocDir.path + '/' + dbPath);
    } else {
      final DatabaseFactory dbFactory = databaseFactoryWeb;
      db = await dbFactory.openDatabase(dbPath);
    }
    jwt = await store.record('jwt').get(db);
    if (jwt != null && jwt.isNotEmpty) {
      final Jwt decod = Jwt.fromToken(jwt);
      if (!decod.isexpired) {
        setState(() {
          userID = decod.audience;
        });
      }
    }
  }

  @override
  void initState() {
    super.initState();
    getUserID();
  }

  Widget getgrowingsessiontypeIcon(GrowingSessionType t) {
    switch (t) {
      case GrowingSessionType.indoor:
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            Icon(
              Icons.lightbulb,
              color: Colors.yellow,
            ),
            Text('indoor')
          ],
        );
      case GrowingSessionType.outdoor:
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            Icon(
              Icons.wb_sunny,
              color: Colors.yellow,
            ),
            Text('outdoor')
          ],
        );
      case GrowingSessionType.artemisUnknown:
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            Icon(
              Icons.device_unknown,
              color: Colors.grey,
            ),
            Text('unknow')
          ],
        );
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const <Widget>[
        Icon(
          Icons.device_unknown,
          color: Colors.grey,
        ),
        Text('unknow')
      ],
    );
  }

  Widget getgrowingsessionstatusIcon(GrowingSessionState t) {
    switch (t) {
      case GrowingSessionState.project:
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            Icon(
              Icons.next_plan,
              color: Colors.yellow,
            ),
            Text('project')
          ],
        );
      case GrowingSessionState.running:
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            Icon(
              Icons.wb_sunny,
              color: Colors.green,
            ),
            Text('running')
          ],
        );
      case GrowingSessionState.terminated:
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            Icon(
              Icons.kitchen,
              color: Colors.red,
            ),
            Text('terminated')
          ],
        );
      case GrowingSessionState.artemisUnknown:
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            Icon(
              Icons.not_interested_rounded,
              color: Colors.grey,
            ),
            Text('unknow')
          ],
        );
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const <Widget>[
        Icon(
          Icons.not_interested_rounded,
          color: Colors.grey,
        ),
        Text('unknow')
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Query(
      options: QueryOptions(
          fetchPolicy: FetchPolicy.cacheAndNetwork,
          documentNode: GrowingSessionsQuery().document),
      builder: (QueryResult result,
          {Future<QueryResult> Function() refetch, FetchMore fetchMore}) {
        if (result.hasException) {
          return Center(
            child: Text(
              '⚠️ ${result.exception.toString()}',
              textAlign: TextAlign.center,
              style: const TextStyle(color: Colors.red),
            ),
          );
        }
        if (result.loading) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        final List<GrowingSessions$Query$GrowingSession> allGrowingSessions =
            GrowingSessionsQuery()
                .parse(result.data as Map<String, dynamic>)
                .growingsessions;
        return Column(
          children: <Widget>[
            const SizedBox(
              height: 30,
            ),
            Container(
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.green, width: 3),
              ),
              child: const Text(
                'Growing sessions',
                style: TextStyle(color: Colors.white),
                textScaleFactor: 3,
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListTile(
                      tileColor: Colors.white,
                      leading: getgrowingsessiontypeIcon(
                          allGrowingSessions[index].type),
                      trailing: getgrowingsessionstatusIcon(
                          allGrowingSessions[index].state),
                      onTap: () => Navigator.push<dynamic>(
                        context,
                        MaterialPageRoute<dynamic>(
                            builder: (BuildContext context) =>
                                GrowingSessionComponent(
                                  growingsessionID:
                                      allGrowingSessions[index].id,
                                  gSession: allGrowingSessions[index],
                                  userID: userID,
                                )),
                      ),
                      title: Text(
                        allGrowingSessions[index].name,
                        textAlign: TextAlign.center,
                        textScaleFactor: 1.1,
                      ),
                      subtitle: Center(
                        child: Text(allGrowingSessions[index]
                            .startedAt
                            .toLocal()
                            .toString()),
                      ),
                    ),
                  );
                },
                itemCount: allGrowingSessions.length,
              ),
            ),
            FlatButton(
                color: Colors.white,
                textColor: Colors.black,
                child: Text(
                  _addOne ? '-' : '+',
                  style: const TextStyle(color: Colors.black),
                ),
                onPressed: () => setState(() => _addOne = !_addOne),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0))),
            if (_addOne)
              Expanded(
                child: AddGrowingSessionComponent(
                  userId: userID,
                ),
              )
          ],
        );
      },
    );
  }
}
