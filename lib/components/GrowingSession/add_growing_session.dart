import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:growyapp/generated/graphql_api.dart';

class AddGrowingSessionComponent extends StatefulWidget {
  const AddGrowingSessionComponent({this.userId});
  final String userId;

  @override
  _AddGrowingSessionComponentState createState() =>
      _AddGrowingSessionComponentState();
}

/// type GrowingSessionInput {
///  name: String!
///  type: GrowingSessionType!
///  state: GrowingSessionState!
///  startedAt: Time!
///  endedAt: Time
///  boxVolume: [MeasureInput!]
///  plants: [PlantInput!]
///  userId: String!
/// }
class _AddGrowingSessionComponentState
    extends State<AddGrowingSessionComponent> {
  final TextEditingController _sessionNameController = TextEditingController();
  GrowingSessionType _sessionType = GrowingSessionType.indoor;
  GrowingSessionState _sessionState = GrowingSessionState.project;
  final DateTime _sessionStartedAt = DateTime.now();
  final DateTime _sessionEndedAt = DateTime.now();
  ValueChanged<DateTime> _selectedStartedAt;
  ValueChanged<DateTime> _selectedEndedAt;

  Future<void> _selectStartDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _sessionStartedAt,
        firstDate: DateTime(2018),
        lastDate: DateTime(2022));
    if (picked != null && picked != _sessionStartedAt) {
      _selectedStartedAt(picked);
    }
  }

  Future<void> _selectEndDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _sessionEndedAt,
        firstDate: DateTime(2018),
        lastDate: DateTime(2022));
    if (picked != null && picked != _sessionEndedAt) {
      _selectedEndedAt(picked);
    }
  }

  @override
  Widget build(BuildContext context) {
    final AddGrowingSessionArguments yolo = AddGrowingSessionArguments(
        input: GrowingSessionInput(
      type: _sessionType,
      name: _sessionNameController.text,
      state: _sessionState,
      startedAt: _sessionStartedAt,
      endedAt: _sessionStartedAt,
      userId: widget.userId,
    ));
    return Mutation(
        options: MutationOptions(
            documentNode: AddGrowingSessionMutation().document,
            update: (Cache _, QueryResult res) {
              if (!res.loading && !res.hasException) {
                final AddGrowingSession$Mutation$GrowingSession added =
                    AddGrowingSessionMutation()
                        .parse(res.data as Map<String, dynamic>)
                        .addgrowingsession;
                final SnackBar snackBar =
                    SnackBar(content: Text('Successfuly saved ${added.id}'));
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else if (res.hasException) {
                final SnackBar snackBar = SnackBar(
                    backgroundColor: Colors.red,
                    content: Text('Error : ${res.exception.toString()}'));
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            }),
        builder: (RunMutation run, QueryResult result) {
          return Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.white),
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: ListView(
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(6),
                  child: TextField(
                    maxLines: 1,
                    style: TextStyle(color: Theme.of(context).accentColor),
                    controller: _sessionNameController,
                    decoration: InputDecoration(
                      hintStyle: const TextStyle(color: Colors.white),
                      hintText: 'Session name',
                      enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(
                          width: 1.3,
                          color: Colors.white,
                          style: BorderStyle.solid,
                        ),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      prefixIcon: const Icon(
                        Icons.edit_outlined,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Row(
                      children: <Widget>[
                        const Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text(
                            'Session type :',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        DropdownButton<GrowingSessionType>(
                          value: _sessionType,
                          onChanged: (GrowingSessionType e) => setState(() {
                            _sessionType = e;
                          }),
                          style: const TextStyle(color: Colors.white),
                          dropdownColor: Colors.black,
                          items: GrowingSessionType.values
                              .map<DropdownMenuItem<GrowingSessionType>>(
                                  (GrowingSessionType e) =>
                                      DropdownMenuItem<GrowingSessionType>(
                                        value: e,
                                        child: Text(
                                          e
                                              .toString()
                                              .split('.')
                                              .last
                                              .toUpperCase(),
                                          style: const TextStyle(
                                              color: Colors.white),
                                        ),
                                      ))
                              .toList(),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    padding: const EdgeInsets.all(6),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Row(
                      children: <Widget>[
                        const Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text(
                            'Session state :',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        DropdownButton<GrowingSessionState>(
                          value: _sessionState,
                          onChanged: (GrowingSessionState e) => setState(() {
                            _sessionState = e;
                          }),
                          style: const TextStyle(color: Colors.white),
                          dropdownColor: Colors.black,
                          items: GrowingSessionState.values
                              .map<DropdownMenuItem<GrowingSessionState>>(
                                  (GrowingSessionState e) =>
                                      DropdownMenuItem<GrowingSessionState>(
                                        value: e,
                                        child: Text(
                                          e
                                              .toString()
                                              .split('.')
                                              .last
                                              .toUpperCase(),
                                          style: const TextStyle(
                                              color: Colors.white),
                                        ),
                                      ))
                              .toList(),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(6),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          'Session start :',
                          textScaleFactor: 1.1,
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      _InputDropdown(
                        labelText: 'Started at',
                        valueStyle: const TextStyle(color: Colors.white),
                        valueText: DateFormat.yMMMd().format(_sessionStartedAt),
                        onPressed: () => _selectStartDate(context),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(6),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          'Session end :',
                          textScaleFactor: 1.1,
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      _InputDropdown(
                        labelText: 'Ended at',
                        valueStyle: const TextStyle(color: Colors.white),
                        valueText: DateFormat.yMMMd().format(_sessionEndedAt),
                        // valueStyle: valueStyle,
                        onPressed: () => _selectEndDate(context),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FlatButton.icon(
                    color: Colors.white,
                    icon: const Icon(Icons.save),
                    label: const Text('save'),
                    onPressed: () => run(yolo.toJson()),
                  ),
                )
              ],
            ),
          );
        });
  }
}

class _InputDropdown extends StatelessWidget {
  const _InputDropdown(
      {Key key,
      this.child,
      this.labelText,
      this.valueText,
      this.valueStyle,
      this.onPressed})
      : super(key: key);

  final String labelText;
  final String valueText;
  final TextStyle valueStyle;
  final void Function() onPressed;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: InputDecorator(
        decoration: InputDecoration(
          labelText: labelText,
        ),
        baseStyle: valueStyle,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(valueText, style: valueStyle),
            Icon(Icons.arrow_drop_down,
                color: Theme.of(context).brightness == Brightness.light
                    ? Colors.grey.shade700
                    : Colors.white70),
          ],
        ),
      ),
    );
  }
}
