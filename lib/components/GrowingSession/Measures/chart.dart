import 'package:intl/intl.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:growyapp/generated/graphql_api.dart';
import 'package:flutter/material.dart';

class MeasureChartComponent extends StatefulWidget {
  const MeasureChartComponent(
      {this.growingSessionID, this.measureType, this.offset});
  final String growingSessionID;
  final DataType measureType;
  final int offset;
  @override
  _MeasureChartComponentState createState() => _MeasureChartComponentState();
}

class _MeasureChartComponentState extends State<MeasureChartComponent> {
  @override
  Widget build(BuildContext context) {
    return Query(
        options: QueryOptions(
            documentNode: AllMeasuresQuery().document,
            variables: AllMeasuresArguments(
              growingSessionId: widget.growingSessionID,
              type: widget.measureType,
              count: (6 * 12) + 2,
              offset: ((6 * 12) + 2) * widget.offset,
            ).toJson()),
        builder: (QueryResult result,
            {Future<QueryResult> Function() refetch, FetchMore fetchMore}) {
          if (result.hasException) {
            return Center(
              child: Text(
                result.exception.toString(),
                style: const TextStyle(color: Colors.red),
              ),
            );
          }
          if (result.loading) {
            return const Center(
              child: CircularProgressIndicator(
                backgroundColor: Colors.green,
              ),
            );
          }
          final List<AllMeasures$Query$Measure> measures = AllMeasuresQuery()
              .parse(result.data as Map<String, dynamic>)
              .measures;
          final List<FlSpot> linebarchartdatas = measures
              .map((AllMeasures$Query$Measure e) => FlSpot(
                  e.date.toLocal().millisecondsSinceEpoch.toDouble(), e.value))
              .toList();
          final String dataTypeFmt =
              widget.measureType.toString().split('.').last.toUpperCase();
          final double maxYVal = measures
              .reduce((AllMeasures$Query$Measure cur,
                      AllMeasures$Query$Measure next) =>
                  cur.value > next.value ? cur : next)
              .value;
          final double minYVal = measures
              .reduce((AllMeasures$Query$Measure cur,
                      AllMeasures$Query$Measure next) =>
                  cur.value < next.value ? cur : next)
              .value;
          return Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.white),
                ),
                padding: const EdgeInsets.all(10),
                child: Text(
                  '$dataTypeFmt in ${measures.first.unit.toUpperCase()} for 12H\nMax : $maxYVal - Min : $minYVal',
                  textAlign: TextAlign.center,
                  style: const TextStyle(color: Colors.white),
                ),
              ),
              const SizedBox(
                height: 22,
              ),
              LineChart(LineChartData(
                  gridData: FlGridData(show: true),
                  maxY: maxYVal + 2,
                  minY: minYVal - 2,
                  borderData: FlBorderData(
                    show: true,
                    border: const Border(
                      bottom: BorderSide(
                        color: Colors.white,
                        width: 4,
                      ),
                      left: BorderSide(
                        color: Colors.white,
                        width: 4,
                      ),
                      right: BorderSide(
                        color: Colors.white,
                        width: 4,
                      ),
                      top: BorderSide(
                        color: Colors.white,
                        width: 4,
                      ),
                    ),
                  ),
                  titlesData: FlTitlesData(
                      leftTitles: SideTitles(
                          showTitles: true,
                          getTextStyles: (double v) => const TextStyle(
                                color: Colors.white,
                              ),
                          getTitles: (double v) =>
                              v.round().isEven ? v.round().toString() : ''),
                      bottomTitles: SideTitles(
                          showTitles: true,
                          margin: 20,
                          getTextStyles: (double v) =>
                              const TextStyle(color: Colors.white),
                          getTitles: (double v) {
                            final DateTime date =
                                DateTime.fromMillisecondsSinceEpoch(v.toInt())
                                    .toLocal();
                            return date.minute == 30 || date.minute == 0
                                ? DateFormat('dd-MMM\nkk:mm').format(date)
                                : '';
                          })),
                  lineBarsData: <LineChartBarData>[
                    LineChartBarData(
                      spots: linebarchartdatas,
                    )
                  ])),
            ],
          );
        });
  }
}
