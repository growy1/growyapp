import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:growyapp/generated/graphql_api.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class MeasureComponent extends StatefulWidget {
  const MeasureComponent({this.growingSessionID, this.measureType});
  final String growingSessionID;
  final DataType measureType;
  @override
  _MeasureComponentState createState() => _MeasureComponentState();
}

class _MeasureComponentState extends State<MeasureComponent> {
  @override
  Widget build(BuildContext context) {
    return Query(
        options: QueryOptions(
            documentNode: LastMeasureQuery().document,
            variables: LastMeasureArguments(
                    growingSessionId: widget.growingSessionID,
                    type: widget.measureType)
                .toJson()),
        builder: (QueryResult result,
            {Future<QueryResult> Function() refetch, FetchMore fetchMore}) {
          if (result.hasException) {
            return Center(
              child: Text(
                result.exception.toString(),
                style: const TextStyle(color: Colors.red),
              ),
            );
          }
          if (result.loading) {
            return const Center(
              child: CircularProgressIndicator(
                backgroundColor: Colors.green,
              ),
            );
          }
          final LastMeasure$Query$Measure measure = LastMeasureQuery()
              .parse(result.data as Map<String, dynamic>)
              .measure;
          return Container(
            color: Colors.green,
            height: MediaQuery.of(context).size.height / 3,
            width: MediaQuery.of(context).size.width - 3,
            child: Stack(
              // mainAxisAlignment: MainAxisAlignment.center,
              // mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Positioned(
                  top: 3,
                  left: 3,
                  child: Text(
                    widget.measureType.toString().split('.').last.toUpperCase(),
                    style: const TextStyle(color: Colors.white),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            measure.value.toString(),
                            textScaleFactor: 2.3,
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            measure.unit,
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                    Text(
                      DateFormat("E dd MMM kk'H'mm")
                          .format(measure.date.toLocal()),
                      style: const TextStyle(color: Colors.white),
                    )
                  ],
                ),
              ],
            ),
          );
        });
  }
}
