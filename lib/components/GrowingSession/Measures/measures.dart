import 'package:flutter/material.dart';
import 'package:growyapp/components/GrowingSession/Measures/chart.dart';
import 'package:growyapp/components/GrowingSession/Measures/measure.dart';
import 'package:growyapp/generated/graphql_api.dart';

class MeasuresComponent extends StatefulWidget {
  const MeasuresComponent({this.growingSessionID});
  final String growingSessionID;
  @override
  _MeasuresComponentState createState() => _MeasuresComponentState();
}

class _MeasuresComponentState extends State<MeasuresComponent> {
  bool _switchtype = true;
  int _offset = 0;
  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      child: ListView(
        scrollDirection: Axis.horizontal,
        semanticChildCount: 2,
        shrinkWrap: true,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(5),
                  child: MeasureComponent(
                    growingSessionID: widget.growingSessionID,
                    measureType: DataType.temperature,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(5),
                  child: MeasureComponent(
                    growingSessionID: widget.growingSessionID,
                    measureType: DataType.humidity,
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Row(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    IconButton(
                      color: Colors.green,
                      iconSize: 47,
                      onPressed: () => setState(() {
                        _offset++;
                      }),
                      icon: const Icon(Icons.arrow_left),
                    ),
                    Expanded(
                      child: MeasureChartComponent(
                        offset: _offset,
                        growingSessionID: widget.growingSessionID,
                        measureType: _switchtype
                            ? DataType.temperature
                            : DataType.humidity,
                      ),
                    ),
                    IconButton(
                      color: Colors.green,
                      iconSize: 47,
                      onPressed: () => setState(() {
                        if (_offset - 1 >= 0) {
                          _offset--;
                        }
                      }),
                      icon: const Icon(Icons.arrow_right),
                    ),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const Text(
                      'Humidity',
                      style: TextStyle(color: Colors.white),
                    ),
                    Switch(
                      value: _switchtype,
                      onChanged: (bool v) => setState(() {
                        _switchtype = v;
                      }),
                    ),
                    const Text(
                      'Temperature',
                      style: TextStyle(color: Colors.white),
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
