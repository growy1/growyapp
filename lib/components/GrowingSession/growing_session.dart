import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:growyapp/components/Device/device_list.dart';
import 'package:growyapp/components/GrowingSession/Pictures/picture_list.dart';
import 'package:growyapp/components/GrowingSession/Plants/plant_list.dart';
import 'package:growyapp/generated/graphql_api.dart';
import 'package:flutter/material.dart';

import 'Measures/measures.dart';

class GrowingSessionComponent extends StatefulWidget {
  const GrowingSessionComponent(
      {@required this.growingsessionID,
      @required this.userID,
      @required this.gSession});
  final String userID;
  final String growingsessionID;
  final GrowingSessions$Query$GrowingSession gSession;

  @override
  _GrowingSessionComponentState createState() =>
      _GrowingSessionComponentState();
}

class _GrowingSessionComponentState extends State<GrowingSessionComponent> {
  int _currentIndex = 1;

  List<Widget> _children(
    List<GrowingSession$Query$GrowingSession$Plant> plantlist,
    Future<QueryResult> Function() refetch,
    bool loading,
  ) =>
      <Widget>[
        DeviceListComponent(
          growingSessionId: widget.growingsessionID,
        ),
        PlantListComponent(
          plantlist: plantlist,
          refetch: refetch,
        ),
        MeasuresComponent(
          growingSessionID: widget.growingsessionID,
        ),
        PictureListComponent(
          growingSessionId: widget.growingsessionID,
        )
      ];
  @override
  Widget build(BuildContext context) {
    final AddPictureMutation mut = AddPictureMutation(
        variables:
            AddPictureArguments(growingSessionId: widget.growingsessionID));
    return Scaffold(
      backgroundColor: Colors.black,
      floatingActionButton: (_currentIndex == 3)
          ? Mutation(
              options: MutationOptions(
                documentNode: mut.document,
                update: (Cache _, QueryResult res) {
                  if (res.data is Map<String, dynamic>) {
                    final AddPicture$Mutation$Picture response =
                        AddPictureMutation()
                            .parse(res.data as Map<String, dynamic>)
                            .addpicture;
                    final SnackBar snackBar =
                        SnackBar(content: Text('url : ${response.url}'));
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  } else if (res.hasException) {
                    final SnackBar snackBar = SnackBar(
                        backgroundColor: Colors.red,
                        content: Text('url : ${res.exception.toString()}'));
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }
                },
              ),
              builder: (RunMutation run, QueryResult result) {
                return Padding(
                  padding: const EdgeInsets.only(bottom: 30),
                  child: FloatingActionButton(
                    heroTag: null,
                    child: const Icon(Icons.camera_alt),
                    mini: true,
                    onPressed: () => run(mut.getVariablesMap()),
                  ),
                );
              })
          : Container(),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        unselectedItemColor: Colors.white,
        backgroundColor: Colors.black,
        type: BottomNavigationBarType.fixed,
        onTap: (int index) {
          setState(() {
            _currentIndex = index;
          });
        },
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.handyman),
            label: 'Devices',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Plants',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.thermostat_rounded),
            label: 'Measures',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.camera),
            label: 'Pictures',
          ),
          // BottomNavigationBarItem(
          //     icon: Icon(Icons.person), label: 'Profile')
        ],
      ),
      appBar: AppBar(
        backgroundColor: Colors.black,
        centerTitle: true,
        title: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              widget.gSession.name,
              style: const TextStyle(color: Colors.white),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  '${DateTime.now().difference(widget.gSession.startedAt).inDays} days - ',
                  softWrap: true,
                  textAlign: TextAlign.center,
                  // textScaleFactor: 1.6,
                  style: const TextStyle(color: Colors.white),
                ),
                Text(
                  '${DateTime.now().difference(widget.gSession.startedAt).inDays ~/ 7} weeks',
                  softWrap: true,
                  textAlign: TextAlign.center,
                  // textScaleFactor: 1.6,
                  style: const TextStyle(color: Colors.white),
                ),
              ],
            ),
          ],
        ),
      ),
      body: Query(
        options: QueryOptions(
            fetchPolicy: FetchPolicy.cacheAndNetwork,
            variables: <String, String>{'id': widget.growingsessionID},
            documentNode: GrowingSessionQuery().document),
        builder: (QueryResult result,
            {Future<QueryResult> Function() refetch, FetchMore fetchMore}) {
          if (result.hasException) {
            return Text(result.exception.toString());
          }
          if (result.loading) {
            return const Center(
              child: CircularProgressIndicator(
                backgroundColor: Colors.green,
              ),
            );
          }
          final GrowingSession$Query$GrowingSession growingSession =
              GrowingSessionQuery()
                  .parse(result.data as Map<String, dynamic>)
                  .growingsession;
          final List<GrowingSession$Query$GrowingSession$Plant> plantlist =
              growingSession.plants;
          return _children(plantlist, refetch, result.loading)[_currentIndex];
        },
      ),
    );
  }
}
