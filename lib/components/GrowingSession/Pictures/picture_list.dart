import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import 'package:sembast_web/sembast_web.dart';
import 'package:growyapp/generated/graphql_api.dart';
import 'package:growyapp/components/GrowingSession/Pictures/picture.dart';

class PictureListComponent extends StatefulWidget {
  const PictureListComponent({this.growingSessionId});
  final String growingSessionId;
  @override
  _PictureListComponentState createState() => _PictureListComponentState();
}

class _PictureListComponentState extends State<PictureListComponent> {
  String picAuth;
  Future<void> getUserSavedCreds() async {
    Database db;
    const String dbPath = 'growyapp.db';
    final StoreRef<String, String> store = StoreRef<String, String>.main();
    if (!kIsWeb) {
      final Directory appDocDir = await getApplicationDocumentsDirectory();
      final DatabaseFactory dbFactory = databaseFactoryIo;
      db = await dbFactory.openDatabase(appDocDir.path + '/' + dbPath);
    } else {
      final DatabaseFactory dbFactory = databaseFactoryWeb;
      db = await dbFactory.openDatabase(dbPath);
    }
    picAuth = await store.record('basicauth').get(db);
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getUserSavedCreds();
  }

  @override
  Widget build(BuildContext context) {
    return Query(
      options: QueryOptions(
        documentNode: GrowingSessionPicturesQuery().document,
        variables: GrowingSessionPicturesArguments(
                growingSessionId: widget.growingSessionId)
            .toJson(),
      ),
      builder: (QueryResult result,
          {Future<QueryResult> Function() refetch, FetchMore fetchMore}) {
        if (result.hasException) {
          return Text(result.exception.toString());
        }
        if (result.loading) {
          return const Center(
            child: CircularProgressIndicator(
              backgroundColor: Colors.white,
            ),
          );
        }
        final List<GrowingSessionPictures$Query$Picture> pictureslist =
            GrowingSessionPicturesQuery()
                .parse(result.data as Map<String, dynamic>)
                .pictures;
        final DeletePictureMutation mut = DeletePictureMutation();
        return RefreshIndicator(
          onRefresh: () async => await refetch(),
          child: ListView.builder(
            scrollDirection: Axis.vertical,
            padding: const EdgeInsets.all(3),
            itemCount: pictureslist.length,
            itemBuilder: (_, int index) {
              return Card(
                color: Colors.black,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Stack(
                      alignment: Alignment.topRight,
                      children: <Widget>[
                        PictureComponent(
                          auth: picAuth,
                          pic: pictureslist[index],
                        ),
                        Mutation(
                            options: MutationOptions(
                              documentNode: mut.document,
                              update: (Cache _, QueryResult res) {
                                if (!res.loading &&
                                    res.data is Map<String, dynamic>) {
                                  final DeletePicture$Mutation$Picture
                                      response = DeletePictureMutation()
                                          .parse(
                                              res.data as Map<String, dynamic>)
                                          .deletepicture;
                                  final SnackBar snackBar = SnackBar(
                                      content: Text(
                                          'Successfuly deleted id ${response.id}'));
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(snackBar);
                                  refetch();
                                } else if (res.hasException) {
                                  final SnackBar snackBar = SnackBar(
                                      backgroundColor: Colors.red,
                                      content: Text(
                                          'Error : ${res.exception.toString()}'));
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(snackBar);
                                }
                              },
                            ),
                            builder: (RunMutation run, QueryResult result) {
                              final DeletePictureArguments vars =
                                  DeletePictureArguments(
                                      id: pictureslist[index].id);
                              return FloatingActionButton(
                                mini: true,
                                heroTag: null,
                                child: const Icon(Icons.delete),
                                onPressed: () => run(vars.toJson()),
                              );
                            }),
                      ],
                    ),
                  ],
                ),
              );
            },
          ),
        );
      },
    );
  }
}
