import 'package:intl/intl.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:growyapp/generated/graphql_api.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class PictureComponent extends StatefulWidget {
  const PictureComponent({this.auth, this.pic});
  final String auth;
  final GrowingSessionPictures$Query$Picture pic;

  @override
  _PictureComponentState createState() => _PictureComponentState();
}

class _PictureComponentState extends State<PictureComponent> {
  @override
  Widget build(BuildContext context) {
    final CachedNetworkImage _img = CachedNetworkImage(
        imageUrl: widget.pic.url,
        imageRenderMethodForWeb: ImageRenderMethodForWeb.HttpGet,
        progressIndicatorBuilder: (BuildContext context, String url, _) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
        httpHeaders: <String, String>{
          'Authorization': widget.auth,
        });
    return Column(
      children: <Widget>[
        ClipRRect(
          borderRadius: BorderRadius.circular(16.0),
          child: _img,
        ),
        Container(
          height: 40,
          padding: const EdgeInsets.all(6),
          decoration: BoxDecoration(
            color: Colors.black,
            border: Border.all(color: Colors.green),
            borderRadius: BorderRadius.circular(16.0),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                DateFormat.yMMMEd().format(widget.pic.takenAt.toLocal()),
                textScaleFactor: 1.2,
                style: const TextStyle(color: Colors.white),
              ),
              Text(
                DateFormat.Hm().format(widget.pic.takenAt.toLocal()),
                textScaleFactor: 1.2,
                style: const TextStyle(color: Colors.white),
              ),
              FloatingActionButton(
                  mini: true,
                  // heroTag: 'picshare${widget.pic.id}',
                  heroTag: null,
                  onPressed: () => print('ok'),
                  child: const Text(
                    '🔗',
                    style: TextStyle(fontFamily: 'EmojiOne'),
                  )),
              FloatingActionButton(
                mini: true,
                // heroTag: 'picsave${widget.pic.id}',
                heroTag: null,
                onPressed: () => print('ok'),
                child: const Text(
                  '💾',
                  style: TextStyle(fontFamily: 'EmojiOne'),
                ),
              ),
              FloatingActionButton(
                  mini: true,
                  // heroTag: 'picview${widget.pic.id}',
                  heroTag: null,
                  onPressed: () => Navigator.push<dynamic>(
                        context,
                        MaterialPageRoute<dynamic>(
                            builder: (BuildContext context) => PhotoView(
                                  imageProvider: NetworkImage(widget.pic.url,
                                      headers: <String, String>{
                                        'Authorization': widget.auth
                                      }),
                                )),
                      ),
                  child: const Text(
                    '👁️',
                    style: TextStyle(fontFamily: 'EmojiOne'),
                  ))
            ],
          ),
        )
      ],
    );
  }
}
