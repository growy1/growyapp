import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';

class PushNotificationsManager {
  factory PushNotificationsManager() => _instance;
  PushNotificationsManager._();

  static final PushNotificationsManager _instance =
      PushNotificationsManager._();

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  bool _initialized = false;
  Future<String> get token async => await _firebaseMessaging.getToken();

  Future<void> init() async {
    if (!kIsWeb && !Platform.isLinux) {
      if (!_initialized) {
        _firebaseMessaging.requestNotificationPermissions(
            const IosNotificationSettings(
                sound: true, badge: true, alert: true, provisional: true));
        _firebaseMessaging.onIosSettingsRegistered
            .listen((IosNotificationSettings settings) {
          print('Settings registered: $settings');
        });
        _firebaseMessaging.configure(
          onMessage: (Map<String, dynamic> message) async {
            print('onMessage: $message');
          },
          onLaunch: (Map<String, dynamic> message) async {
            print('onLaunch: $message');
          },
          onResume: (Map<String, dynamic> message) async {
            print('onResume: $message');
          },
        );

        // For testing purposes print the Firebase Messaging token
        final String token = await _firebaseMessaging.getToken();
        print('FirebaseMessaging token: [$token]');

        _initialized = true;
      }
    }
  }
}
