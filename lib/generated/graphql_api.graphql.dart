// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:meta/meta.dart';
import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'graphql_api.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class LastMeasure$Query$Measure with EquatableMixin {
  LastMeasure$Query$Measure();

  factory LastMeasure$Query$Measure.fromJson(Map<String, dynamic> json) =>
      _$LastMeasure$Query$MeasureFromJson(json);

  String id;

  @JsonKey(unknownEnumValue: DataType.artemisUnknown)
  DataType type;

  double value;

  String unit;

  DateTime date;

  @override
  List<Object> get props => [id, type, value, unit, date];
  Map<String, dynamic> toJson() => _$LastMeasure$Query$MeasureToJson(this);
}

@JsonSerializable(explicitToJson: true)
class LastMeasure$Query with EquatableMixin {
  LastMeasure$Query();

  factory LastMeasure$Query.fromJson(Map<String, dynamic> json) =>
      _$LastMeasure$QueryFromJson(json);

  LastMeasure$Query$Measure measure;

  @override
  List<Object> get props => [measure];
  Map<String, dynamic> toJson() => _$LastMeasure$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Strains$Query$Strain$SeedBank with EquatableMixin {
  Strains$Query$Strain$SeedBank();

  factory Strains$Query$Strain$SeedBank.fromJson(Map<String, dynamic> json) =>
      _$Strains$Query$Strain$SeedBankFromJson(json);

  String name;

  @override
  List<Object> get props => [name];
  Map<String, dynamic> toJson() => _$Strains$Query$Strain$SeedBankToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Strains$Query$Strain with EquatableMixin {
  Strains$Query$Strain();

  factory Strains$Query$Strain.fromJson(Map<String, dynamic> json) =>
      _$Strains$Query$StrainFromJson(json);

  int id;

  String name;

  @JsonKey(unknownEnumValue: StrainPhotoperiod.artemisUnknown)
  StrainPhotoperiod photoperiod;

  Strains$Query$Strain$SeedBank seedBank;

  @override
  List<Object> get props => [id, name, photoperiod, seedBank];
  Map<String, dynamic> toJson() => _$Strains$Query$StrainToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Strains$Query with EquatableMixin {
  Strains$Query();

  factory Strains$Query.fromJson(Map<String, dynamic> json) =>
      _$Strains$QueryFromJson(json);

  List<Strains$Query$Strain> strains;

  @override
  List<Object> get props => [strains];
  Map<String, dynamic> toJson() => _$Strains$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeviceInventory$Query$Inventory$DeviceStock$Device with EquatableMixin {
  DeviceInventory$Query$Inventory$DeviceStock$Device();

  factory DeviceInventory$Query$Inventory$DeviceStock$Device.fromJson(
          Map<String, dynamic> json) =>
      _$DeviceInventory$Query$Inventory$DeviceStock$DeviceFromJson(json);

  int id;

  String name;

  @override
  List<Object> get props => [id, name];
  Map<String, dynamic> toJson() =>
      _$DeviceInventory$Query$Inventory$DeviceStock$DeviceToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeviceInventory$Query$Inventory$DeviceStock with EquatableMixin {
  DeviceInventory$Query$Inventory$DeviceStock();

  factory DeviceInventory$Query$Inventory$DeviceStock.fromJson(
          Map<String, dynamic> json) =>
      _$DeviceInventory$Query$Inventory$DeviceStockFromJson(json);

  String id;

  double count;

  DeviceInventory$Query$Inventory$DeviceStock$Device device;

  @override
  List<Object> get props => [id, count, device];
  Map<String, dynamic> toJson() =>
      _$DeviceInventory$Query$Inventory$DeviceStockToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeviceInventory$Query$Inventory with EquatableMixin {
  DeviceInventory$Query$Inventory();

  factory DeviceInventory$Query$Inventory.fromJson(Map<String, dynamic> json) =>
      _$DeviceInventory$Query$InventoryFromJson(json);

  List<DeviceInventory$Query$Inventory$DeviceStock> deviceStock;

  @override
  List<Object> get props => [deviceStock];
  Map<String, dynamic> toJson() =>
      _$DeviceInventory$Query$InventoryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeviceInventory$Query with EquatableMixin {
  DeviceInventory$Query();

  factory DeviceInventory$Query.fromJson(Map<String, dynamic> json) =>
      _$DeviceInventory$QueryFromJson(json);

  DeviceInventory$Query$Inventory inventory;

  @override
  List<Object> get props => [inventory];
  Map<String, dynamic> toJson() => _$DeviceInventory$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AllMeasures$Query$Measure with EquatableMixin {
  AllMeasures$Query$Measure();

  factory AllMeasures$Query$Measure.fromJson(Map<String, dynamic> json) =>
      _$AllMeasures$Query$MeasureFromJson(json);

  DateTime date;

  double value;

  String unit;

  @override
  List<Object> get props => [date, value, unit];
  Map<String, dynamic> toJson() => _$AllMeasures$Query$MeasureToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AllMeasures$Query with EquatableMixin {
  AllMeasures$Query();

  factory AllMeasures$Query.fromJson(Map<String, dynamic> json) =>
      _$AllMeasures$QueryFromJson(json);

  List<AllMeasures$Query$Measure> measures;

  @override
  List<Object> get props => [measures];
  Map<String, dynamic> toJson() => _$AllMeasures$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Devices$Query$Device with EquatableMixin {
  Devices$Query$Device();

  factory Devices$Query$Device.fromJson(Map<String, dynamic> json) =>
      _$Devices$Query$DeviceFromJson(json);

  int id;

  String name;

  @JsonKey(unknownEnumValue: DeviceType.artemisUnknown)
  DeviceType type;

  @override
  List<Object> get props => [id, name, type];
  Map<String, dynamic> toJson() => _$Devices$Query$DeviceToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Devices$Query with EquatableMixin {
  Devices$Query();

  factory Devices$Query.fromJson(Map<String, dynamic> json) =>
      _$Devices$QueryFromJson(json);

  List<Devices$Query$Device> devices;

  @override
  List<Object> get props => [devices];
  Map<String, dynamic> toJson() => _$Devices$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class SeedInventory$Query$Inventory$SeedStock$Strain with EquatableMixin {
  SeedInventory$Query$Inventory$SeedStock$Strain();

  factory SeedInventory$Query$Inventory$SeedStock$Strain.fromJson(
          Map<String, dynamic> json) =>
      _$SeedInventory$Query$Inventory$SeedStock$StrainFromJson(json);

  int id;

  String name;

  @JsonKey(unknownEnumValue: StrainPhotoperiod.artemisUnknown)
  StrainPhotoperiod photoperiod;

  @override
  List<Object> get props => [id, name, photoperiod];
  Map<String, dynamic> toJson() =>
      _$SeedInventory$Query$Inventory$SeedStock$StrainToJson(this);
}

@JsonSerializable(explicitToJson: true)
class SeedInventory$Query$Inventory$SeedStock with EquatableMixin {
  SeedInventory$Query$Inventory$SeedStock();

  factory SeedInventory$Query$Inventory$SeedStock.fromJson(
          Map<String, dynamic> json) =>
      _$SeedInventory$Query$Inventory$SeedStockFromJson(json);

  String id;

  double count;

  String unit;

  SeedInventory$Query$Inventory$SeedStock$Strain strain;

  @override
  List<Object> get props => [id, count, unit, strain];
  Map<String, dynamic> toJson() =>
      _$SeedInventory$Query$Inventory$SeedStockToJson(this);
}

@JsonSerializable(explicitToJson: true)
class SeedInventory$Query$Inventory with EquatableMixin {
  SeedInventory$Query$Inventory();

  factory SeedInventory$Query$Inventory.fromJson(Map<String, dynamic> json) =>
      _$SeedInventory$Query$InventoryFromJson(json);

  List<SeedInventory$Query$Inventory$SeedStock> seedStock;

  @override
  List<Object> get props => [seedStock];
  Map<String, dynamic> toJson() => _$SeedInventory$Query$InventoryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class SeedInventory$Query with EquatableMixin {
  SeedInventory$Query();

  factory SeedInventory$Query.fromJson(Map<String, dynamic> json) =>
      _$SeedInventory$QueryFromJson(json);

  SeedInventory$Query$Inventory inventory;

  @override
  List<Object> get props => [inventory];
  Map<String, dynamic> toJson() => _$SeedInventory$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AddPicture$Mutation$Picture with EquatableMixin {
  AddPicture$Mutation$Picture();

  factory AddPicture$Mutation$Picture.fromJson(Map<String, dynamic> json) =>
      _$AddPicture$Mutation$PictureFromJson(json);

  String url;

  @override
  List<Object> get props => [url];
  Map<String, dynamic> toJson() => _$AddPicture$Mutation$PictureToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AddPicture$Mutation with EquatableMixin {
  AddPicture$Mutation();

  factory AddPicture$Mutation.fromJson(Map<String, dynamic> json) =>
      _$AddPicture$MutationFromJson(json);

  AddPicture$Mutation$Picture addpicture;

  @override
  List<Object> get props => [addpicture];
  Map<String, dynamic> toJson() => _$AddPicture$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeleteStrainCannabinoid$Mutation$Cannabinoid with EquatableMixin {
  DeleteStrainCannabinoid$Mutation$Cannabinoid();

  factory DeleteStrainCannabinoid$Mutation$Cannabinoid.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteStrainCannabinoid$Mutation$CannabinoidFromJson(json);

  int id;

  @override
  List<Object> get props => [id];
  Map<String, dynamic> toJson() =>
      _$DeleteStrainCannabinoid$Mutation$CannabinoidToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeleteStrainCannabinoid$Mutation with EquatableMixin {
  DeleteStrainCannabinoid$Mutation();

  factory DeleteStrainCannabinoid$Mutation.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteStrainCannabinoid$MutationFromJson(json);

  DeleteStrainCannabinoid$Mutation$Cannabinoid deletestraincannabinoid;

  @override
  List<Object> get props => [deletestraincannabinoid];
  Map<String, dynamic> toJson() =>
      _$DeleteStrainCannabinoid$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AddDeviceStock$Mutation$DeviceStock with EquatableMixin {
  AddDeviceStock$Mutation$DeviceStock();

  factory AddDeviceStock$Mutation$DeviceStock.fromJson(
          Map<String, dynamic> json) =>
      _$AddDeviceStock$Mutation$DeviceStockFromJson(json);

  String id;

  @override
  List<Object> get props => [id];
  Map<String, dynamic> toJson() =>
      _$AddDeviceStock$Mutation$DeviceStockToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AddDeviceStock$Mutation with EquatableMixin {
  AddDeviceStock$Mutation();

  factory AddDeviceStock$Mutation.fromJson(Map<String, dynamic> json) =>
      _$AddDeviceStock$MutationFromJson(json);

  AddDeviceStock$Mutation$DeviceStock adddevicestock;

  @override
  List<Object> get props => [adddevicestock];
  Map<String, dynamic> toJson() => _$AddDeviceStock$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeviceStockInput with EquatableMixin {
  DeviceStockInput(
      {@required this.deviceId, @required this.count, @required this.unit});

  factory DeviceStockInput.fromJson(Map<String, dynamic> json) =>
      _$DeviceStockInputFromJson(json);

  int deviceId;

  double count;

  String unit;

  @override
  List<Object> get props => [deviceId, count, unit];
  Map<String, dynamic> toJson() => _$DeviceStockInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GrowingSessionDevices$Query$Device with EquatableMixin {
  GrowingSessionDevices$Query$Device();

  factory GrowingSessionDevices$Query$Device.fromJson(
          Map<String, dynamic> json) =>
      _$GrowingSessionDevices$Query$DeviceFromJson(json);

  int id;

  @JsonKey(unknownEnumValue: DeviceType.artemisUnknown)
  DeviceType type;

  String name;

  @override
  List<Object> get props => [id, type, name];
  Map<String, dynamic> toJson() =>
      _$GrowingSessionDevices$Query$DeviceToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GrowingSessionDevices$Query with EquatableMixin {
  GrowingSessionDevices$Query();

  factory GrowingSessionDevices$Query.fromJson(Map<String, dynamic> json) =>
      _$GrowingSessionDevices$QueryFromJson(json);

  List<GrowingSessionDevices$Query$Device> devices;

  @override
  List<Object> get props => [devices];
  Map<String, dynamic> toJson() => _$GrowingSessionDevices$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Plant$Query$Plant$GrowState with EquatableMixin {
  Plant$Query$Plant$GrowState();

  factory Plant$Query$Plant$GrowState.fromJson(Map<String, dynamic> json) =>
      _$Plant$Query$Plant$GrowStateFromJson(json);

  String id;

  @JsonKey(unknownEnumValue: DevelopmentStateType.artemisUnknown)
  DevelopmentStateType developmentState;

  DateTime startedAt;

  DateTime endedAt;

  String comments;

  @override
  List<Object> get props =>
      [id, developmentState, startedAt, endedAt, comments];
  Map<String, dynamic> toJson() => _$Plant$Query$Plant$GrowStateToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Plant$Query$Plant$Strain$SeedBank with EquatableMixin {
  Plant$Query$Plant$Strain$SeedBank();

  factory Plant$Query$Plant$Strain$SeedBank.fromJson(
          Map<String, dynamic> json) =>
      _$Plant$Query$Plant$Strain$SeedBankFromJson(json);

  String name;

  @override
  List<Object> get props => [name];
  Map<String, dynamic> toJson() =>
      _$Plant$Query$Plant$Strain$SeedBankToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Plant$Query$Plant$Strain$Measure with EquatableMixin {
  Plant$Query$Plant$Strain$Measure();

  factory Plant$Query$Plant$Strain$Measure.fromJson(
          Map<String, dynamic> json) =>
      _$Plant$Query$Plant$Strain$MeasureFromJson(json);

  String unit;

  double value;

  @override
  List<Object> get props => [unit, value];
  Map<String, dynamic> toJson() =>
      _$Plant$Query$Plant$Strain$MeasureToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Plant$Query$Plant$Strain with EquatableMixin {
  Plant$Query$Plant$Strain();

  factory Plant$Query$Plant$Strain.fromJson(Map<String, dynamic> json) =>
      _$Plant$Query$Plant$StrainFromJson(json);

  String name;

  Plant$Query$Plant$Strain$SeedBank seedBank;

  List<Plant$Query$Plant$Strain$Measure> floweringTimes;

  @override
  List<Object> get props => [name, seedBank, floweringTimes];
  Map<String, dynamic> toJson() => _$Plant$Query$Plant$StrainToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Plant$Query$Plant with EquatableMixin {
  Plant$Query$Plant();

  factory Plant$Query$Plant.fromJson(Map<String, dynamic> json) =>
      _$Plant$Query$PlantFromJson(json);

  String name;

  DateTime startedAt;

  DateTime endedAt;

  List<Plant$Query$Plant$GrowState> states;

  Plant$Query$Plant$Strain strain;

  @override
  List<Object> get props => [name, startedAt, endedAt, states, strain];
  Map<String, dynamic> toJson() => _$Plant$Query$PlantToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Plant$Query with EquatableMixin {
  Plant$Query();

  factory Plant$Query.fromJson(Map<String, dynamic> json) =>
      _$Plant$QueryFromJson(json);

  Plant$Query$Plant plant;

  @override
  List<Object> get props => [plant];
  Map<String, dynamic> toJson() => _$Plant$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateUser$Mutation$User with EquatableMixin {
  UpdateUser$Mutation$User();

  factory UpdateUser$Mutation$User.fromJson(Map<String, dynamic> json) =>
      _$UpdateUser$Mutation$UserFromJson(json);

  String id;

  @override
  List<Object> get props => [id];
  Map<String, dynamic> toJson() => _$UpdateUser$Mutation$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateUser$Mutation with EquatableMixin {
  UpdateUser$Mutation();

  factory UpdateUser$Mutation.fromJson(Map<String, dynamic> json) =>
      _$UpdateUser$MutationFromJson(json);

  UpdateUser$Mutation$User updateuser;

  @override
  List<Object> get props => [updateuser];
  Map<String, dynamic> toJson() => _$UpdateUser$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateUserInput with EquatableMixin {
  UpdateUserInput(
      {@required this.id,
      this.name,
      this.email,
      this.password,
      this.inventory});

  factory UpdateUserInput.fromJson(Map<String, dynamic> json) =>
      _$UpdateUserInputFromJson(json);

  String id;

  String name;

  String email;

  String password;

  bool inventory;

  @override
  List<Object> get props => [id, name, email, password, inventory];
  Map<String, dynamic> toJson() => _$UpdateUserInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AddSeedStock$Mutation$SeedStock with EquatableMixin {
  AddSeedStock$Mutation$SeedStock();

  factory AddSeedStock$Mutation$SeedStock.fromJson(Map<String, dynamic> json) =>
      _$AddSeedStock$Mutation$SeedStockFromJson(json);

  String id;

  @override
  List<Object> get props => [id];
  Map<String, dynamic> toJson() =>
      _$AddSeedStock$Mutation$SeedStockToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AddSeedStock$Mutation with EquatableMixin {
  AddSeedStock$Mutation();

  factory AddSeedStock$Mutation.fromJson(Map<String, dynamic> json) =>
      _$AddSeedStock$MutationFromJson(json);

  AddSeedStock$Mutation$SeedStock addseedstock;

  @override
  List<Object> get props => [addseedstock];
  Map<String, dynamic> toJson() => _$AddSeedStock$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class SeedStockInput with EquatableMixin {
  SeedStockInput(
      {@required this.strainId, @required this.count, @required this.unit});

  factory SeedStockInput.fromJson(Map<String, dynamic> json) =>
      _$SeedStockInputFromJson(json);

  int strainId;

  double count;

  String unit;

  @override
  List<Object> get props => [strainId, count, unit];
  Map<String, dynamic> toJson() => _$SeedStockInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Login$Query$Session$User$Inventory with EquatableMixin {
  Login$Query$Session$User$Inventory();

  factory Login$Query$Session$User$Inventory.fromJson(
          Map<String, dynamic> json) =>
      _$Login$Query$Session$User$InventoryFromJson(json);

  String id;

  @override
  List<Object> get props => [id];
  Map<String, dynamic> toJson() =>
      _$Login$Query$Session$User$InventoryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Login$Query$Session$User with EquatableMixin {
  Login$Query$Session$User();

  factory Login$Query$Session$User.fromJson(Map<String, dynamic> json) =>
      _$Login$Query$Session$UserFromJson(json);

  String email;

  Login$Query$Session$User$Inventory inventory;

  @override
  List<Object> get props => [email, inventory];
  Map<String, dynamic> toJson() => _$Login$Query$Session$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Login$Query$Session with EquatableMixin {
  Login$Query$Session();

  factory Login$Query$Session.fromJson(Map<String, dynamic> json) =>
      _$Login$Query$SessionFromJson(json);

  bool otp;

  String token;

  Login$Query$Session$User user;

  @override
  List<Object> get props => [otp, token, user];
  Map<String, dynamic> toJson() => _$Login$Query$SessionToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Login$Query with EquatableMixin {
  Login$Query();

  factory Login$Query.fromJson(Map<String, dynamic> json) =>
      _$Login$QueryFromJson(json);

  Login$Query$Session login;

  @override
  List<Object> get props => [login];
  Map<String, dynamic> toJson() => _$Login$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AddStrainFloweringTimeInfos$Mutation$Measure with EquatableMixin {
  AddStrainFloweringTimeInfos$Mutation$Measure();

  factory AddStrainFloweringTimeInfos$Mutation$Measure.fromJson(
          Map<String, dynamic> json) =>
      _$AddStrainFloweringTimeInfos$Mutation$MeasureFromJson(json);

  String id;

  @override
  List<Object> get props => [id];
  Map<String, dynamic> toJson() =>
      _$AddStrainFloweringTimeInfos$Mutation$MeasureToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AddStrainFloweringTimeInfos$Mutation with EquatableMixin {
  AddStrainFloweringTimeInfos$Mutation();

  factory AddStrainFloweringTimeInfos$Mutation.fromJson(
          Map<String, dynamic> json) =>
      _$AddStrainFloweringTimeInfos$MutationFromJson(json);

  List<AddStrainFloweringTimeInfos$Mutation$Measure> addstrainfloweringTimes;

  @override
  List<Object> get props => [addstrainfloweringTimes];
  Map<String, dynamic> toJson() =>
      _$AddStrainFloweringTimeInfos$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class MeasureInput with EquatableMixin {
  MeasureInput(
      {this.date,
      @required this.type,
      @required this.value,
      @required this.unit});

  factory MeasureInput.fromJson(Map<String, dynamic> json) =>
      _$MeasureInputFromJson(json);

  DateTime date;

  @JsonKey(unknownEnumValue: DataType.artemisUnknown)
  DataType type;

  double value;

  String unit;

  @override
  List<Object> get props => [date, type, value, unit];
  Map<String, dynamic> toJson() => _$MeasureInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AddGrowingSession$Mutation$GrowingSession with EquatableMixin {
  AddGrowingSession$Mutation$GrowingSession();

  factory AddGrowingSession$Mutation$GrowingSession.fromJson(
          Map<String, dynamic> json) =>
      _$AddGrowingSession$Mutation$GrowingSessionFromJson(json);

  String id;

  String name;

  @override
  List<Object> get props => [id, name];
  Map<String, dynamic> toJson() =>
      _$AddGrowingSession$Mutation$GrowingSessionToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AddGrowingSession$Mutation with EquatableMixin {
  AddGrowingSession$Mutation();

  factory AddGrowingSession$Mutation.fromJson(Map<String, dynamic> json) =>
      _$AddGrowingSession$MutationFromJson(json);

  AddGrowingSession$Mutation$GrowingSession addgrowingsession;

  @override
  List<Object> get props => [addgrowingsession];
  Map<String, dynamic> toJson() => _$AddGrowingSession$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GrowingSessionInput with EquatableMixin {
  GrowingSessionInput(
      {@required this.name,
      @required this.type,
      @required this.state,
      @required this.startedAt,
      this.endedAt,
      this.boxVolume,
      this.plants,
      @required this.userId});

  factory GrowingSessionInput.fromJson(Map<String, dynamic> json) =>
      _$GrowingSessionInputFromJson(json);

  String name;

  @JsonKey(unknownEnumValue: GrowingSessionType.artemisUnknown)
  GrowingSessionType type;

  @JsonKey(unknownEnumValue: GrowingSessionState.artemisUnknown)
  GrowingSessionState state;

  DateTime startedAt;

  DateTime endedAt;

  List<MeasureInput> boxVolume;

  List<PlantInput> plants;

  String userId;

  @override
  List<Object> get props =>
      [name, type, state, startedAt, endedAt, boxVolume, plants, userId];
  Map<String, dynamic> toJson() => _$GrowingSessionInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class PlantInput with EquatableMixin {
  PlantInput(
      {@required this.name,
      @required this.startedAt,
      this.endedAt,
      this.strainId});

  factory PlantInput.fromJson(Map<String, dynamic> json) =>
      _$PlantInputFromJson(json);

  String name;

  DateTime startedAt;

  DateTime endedAt;

  int strainId;

  @override
  List<Object> get props => [name, startedAt, endedAt, strainId];
  Map<String, dynamic> toJson() => _$PlantInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeleteStrainTypeInfos$Mutation$StrainTypeInfos with EquatableMixin {
  DeleteStrainTypeInfos$Mutation$StrainTypeInfos();

  factory DeleteStrainTypeInfos$Mutation$StrainTypeInfos.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteStrainTypeInfos$Mutation$StrainTypeInfosFromJson(json);

  int id;

  @override
  List<Object> get props => [id];
  Map<String, dynamic> toJson() =>
      _$DeleteStrainTypeInfos$Mutation$StrainTypeInfosToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeleteStrainTypeInfos$Mutation with EquatableMixin {
  DeleteStrainTypeInfos$Mutation();

  factory DeleteStrainTypeInfos$Mutation.fromJson(Map<String, dynamic> json) =>
      _$DeleteStrainTypeInfos$MutationFromJson(json);

  DeleteStrainTypeInfos$Mutation$StrainTypeInfos deletestraintypeinfos;

  @override
  List<Object> get props => [deletestraintypeinfos];
  Map<String, dynamic> toJson() => _$DeleteStrainTypeInfos$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GrowingSession$Query$GrowingSession$Plant$Strain with EquatableMixin {
  GrowingSession$Query$GrowingSession$Plant$Strain();

  factory GrowingSession$Query$GrowingSession$Plant$Strain.fromJson(
          Map<String, dynamic> json) =>
      _$GrowingSession$Query$GrowingSession$Plant$StrainFromJson(json);

  int id;

  String name;

  @override
  List<Object> get props => [id, name];
  Map<String, dynamic> toJson() =>
      _$GrowingSession$Query$GrowingSession$Plant$StrainToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GrowingSession$Query$GrowingSession$Plant with EquatableMixin {
  GrowingSession$Query$GrowingSession$Plant();

  factory GrowingSession$Query$GrowingSession$Plant.fromJson(
          Map<String, dynamic> json) =>
      _$GrowingSession$Query$GrowingSession$PlantFromJson(json);

  String id;

  String name;

  GrowingSession$Query$GrowingSession$Plant$Strain strain;

  DateTime startedAt;

  DateTime endedAt;

  @override
  List<Object> get props => [id, name, strain, startedAt, endedAt];
  Map<String, dynamic> toJson() =>
      _$GrowingSession$Query$GrowingSession$PlantToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GrowingSession$Query$GrowingSession with EquatableMixin {
  GrowingSession$Query$GrowingSession();

  factory GrowingSession$Query$GrowingSession.fromJson(
          Map<String, dynamic> json) =>
      _$GrowingSession$Query$GrowingSessionFromJson(json);

  String id;

  String name;

  @JsonKey(unknownEnumValue: GrowingSessionType.artemisUnknown)
  GrowingSessionType type;

  @JsonKey(unknownEnumValue: GrowingSessionState.artemisUnknown)
  GrowingSessionState state;

  DateTime startedAt;

  DateTime endedAt;

  List<GrowingSession$Query$GrowingSession$Plant> plants;

  @override
  List<Object> get props => [id, name, type, state, startedAt, endedAt, plants];
  Map<String, dynamic> toJson() =>
      _$GrowingSession$Query$GrowingSessionToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GrowingSession$Query with EquatableMixin {
  GrowingSession$Query();

  factory GrowingSession$Query.fromJson(Map<String, dynamic> json) =>
      _$GrowingSession$QueryFromJson(json);

  GrowingSession$Query$GrowingSession growingsession;

  @override
  List<Object> get props => [growingsession];
  Map<String, dynamic> toJson() => _$GrowingSession$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateFcmToken$Mutation$User with EquatableMixin {
  UpdateFcmToken$Mutation$User();

  factory UpdateFcmToken$Mutation$User.fromJson(Map<String, dynamic> json) =>
      _$UpdateFcmToken$Mutation$UserFromJson(json);

  String id;

  @override
  List<Object> get props => [id];
  Map<String, dynamic> toJson() => _$UpdateFcmToken$Mutation$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateFcmToken$Mutation with EquatableMixin {
  UpdateFcmToken$Mutation();

  factory UpdateFcmToken$Mutation.fromJson(Map<String, dynamic> json) =>
      _$UpdateFcmToken$MutationFromJson(json);

  UpdateFcmToken$Mutation$User updateuserfcmtoken;

  @override
  List<Object> get props => [updateuserfcmtoken];
  Map<String, dynamic> toJson() => _$UpdateFcmToken$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Strain$Query$Strain$SeedFinderStrainInfo with EquatableMixin {
  Strain$Query$Strain$SeedFinderStrainInfo();

  factory Strain$Query$Strain$SeedFinderStrainInfo.fromJson(
          Map<String, dynamic> json) =>
      _$Strain$Query$Strain$SeedFinderStrainInfoFromJson(json);

  int id;

  String sfStrainHtmlDescription;

  @override
  List<Object> get props => [id, sfStrainHtmlDescription];
  Map<String, dynamic> toJson() =>
      _$Strain$Query$Strain$SeedFinderStrainInfoToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Strain$Query$Strain$SeedBank with EquatableMixin {
  Strain$Query$Strain$SeedBank();

  factory Strain$Query$Strain$SeedBank.fromJson(Map<String, dynamic> json) =>
      _$Strain$Query$Strain$SeedBankFromJson(json);

  int id;

  String name;

  String logoUrl;

  @override
  List<Object> get props => [id, name, logoUrl];
  Map<String, dynamic> toJson() => _$Strain$Query$Strain$SeedBankToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Strain$Query$Strain$StrainTypeInfos with EquatableMixin {
  Strain$Query$Strain$StrainTypeInfos();

  factory Strain$Query$Strain$StrainTypeInfos.fromJson(
          Map<String, dynamic> json) =>
      _$Strain$Query$Strain$StrainTypeInfosFromJson(json);

  int id;

  @JsonKey(unknownEnumValue: StrainType.artemisUnknown)
  StrainType type;

  double percentage;

  @override
  List<Object> get props => [id, type, percentage];
  Map<String, dynamic> toJson() =>
      _$Strain$Query$Strain$StrainTypeInfosToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Strain$Query$Strain$Cannabinoid with EquatableMixin {
  Strain$Query$Strain$Cannabinoid();

  factory Strain$Query$Strain$Cannabinoid.fromJson(Map<String, dynamic> json) =>
      _$Strain$Query$Strain$CannabinoidFromJson(json);

  int id;

  String name;

  double percentage;

  @override
  List<Object> get props => [id, name, percentage];
  Map<String, dynamic> toJson() =>
      _$Strain$Query$Strain$CannabinoidToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Strain$Query$Strain$Measure with EquatableMixin {
  Strain$Query$Strain$Measure();

  factory Strain$Query$Strain$Measure.fromJson(Map<String, dynamic> json) =>
      _$Strain$Query$Strain$MeasureFromJson(json);

  String id;

  @JsonKey(unknownEnumValue: DataType.artemisUnknown)
  DataType type;

  String unit;

  double value;

  @override
  List<Object> get props => [id, type, unit, value];
  Map<String, dynamic> toJson() => _$Strain$Query$Strain$MeasureToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Strain$Query$Strain with EquatableMixin {
  Strain$Query$Strain();

  factory Strain$Query$Strain.fromJson(Map<String, dynamic> json) =>
      _$Strain$Query$StrainFromJson(json);

  String supplier;

  String link;

  Strain$Query$Strain$SeedFinderStrainInfo seedFinderStrainInfo;

  Strain$Query$Strain$SeedBank seedBank;

  String name;

  String genetic;

  @JsonKey(unknownEnumValue: StrainPhotoperiod.artemisUnknown)
  StrainPhotoperiod photoperiod;

  List<Strain$Query$Strain$StrainTypeInfos> type;

  List<Strain$Query$Strain$Cannabinoid> cannabinoids;

  List<Strain$Query$Strain$Measure> floweringTimes;

  @override
  List<Object> get props => [
        supplier,
        link,
        seedFinderStrainInfo,
        seedBank,
        name,
        genetic,
        photoperiod,
        type,
        cannabinoids,
        floweringTimes
      ];
  Map<String, dynamic> toJson() => _$Strain$Query$StrainToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Strain$Query with EquatableMixin {
  Strain$Query();

  factory Strain$Query.fromJson(Map<String, dynamic> json) =>
      _$Strain$QueryFromJson(json);

  Strain$Query$Strain strain;

  @override
  List<Object> get props => [strain];
  Map<String, dynamic> toJson() => _$Strain$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AddGrowState$Mutation$GrowState with EquatableMixin {
  AddGrowState$Mutation$GrowState();

  factory AddGrowState$Mutation$GrowState.fromJson(Map<String, dynamic> json) =>
      _$AddGrowState$Mutation$GrowStateFromJson(json);

  @JsonKey(unknownEnumValue: DevelopmentStateType.artemisUnknown)
  DevelopmentStateType developmentState;

  @override
  List<Object> get props => [developmentState];
  Map<String, dynamic> toJson() =>
      _$AddGrowState$Mutation$GrowStateToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AddGrowState$Mutation with EquatableMixin {
  AddGrowState$Mutation();

  factory AddGrowState$Mutation.fromJson(Map<String, dynamic> json) =>
      _$AddGrowState$MutationFromJson(json);

  AddGrowState$Mutation$GrowState addgrowstate;

  @override
  List<Object> get props => [addgrowstate];
  Map<String, dynamic> toJson() => _$AddGrowState$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GrowStateInput with EquatableMixin {
  GrowStateInput(
      {@required this.developmentState,
      @required this.startedAt,
      this.endedAt,
      this.comments});

  factory GrowStateInput.fromJson(Map<String, dynamic> json) =>
      _$GrowStateInputFromJson(json);

  @JsonKey(unknownEnumValue: DevelopmentStateType.artemisUnknown)
  DevelopmentStateType developmentState;

  DateTime startedAt;

  DateTime endedAt;

  String comments;

  @override
  List<Object> get props => [developmentState, startedAt, endedAt, comments];
  Map<String, dynamic> toJson() => _$GrowStateInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GrowingSessions$Query$GrowingSession with EquatableMixin {
  GrowingSessions$Query$GrowingSession();

  factory GrowingSessions$Query$GrowingSession.fromJson(
          Map<String, dynamic> json) =>
      _$GrowingSessions$Query$GrowingSessionFromJson(json);

  String id;

  String name;

  @JsonKey(unknownEnumValue: GrowingSessionType.artemisUnknown)
  GrowingSessionType type;

  @JsonKey(unknownEnumValue: GrowingSessionState.artemisUnknown)
  GrowingSessionState state;

  DateTime startedAt;

  DateTime endedAt;

  @override
  List<Object> get props => [id, name, type, state, startedAt, endedAt];
  Map<String, dynamic> toJson() =>
      _$GrowingSessions$Query$GrowingSessionToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GrowingSessions$Query with EquatableMixin {
  GrowingSessions$Query();

  factory GrowingSessions$Query.fromJson(Map<String, dynamic> json) =>
      _$GrowingSessions$QueryFromJson(json);

  List<GrowingSessions$Query$GrowingSession> growingsessions;

  @override
  List<Object> get props => [growingsessions];
  Map<String, dynamic> toJson() => _$GrowingSessions$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GrowingSessionPictures$Query$Picture with EquatableMixin {
  GrowingSessionPictures$Query$Picture();

  factory GrowingSessionPictures$Query$Picture.fromJson(
          Map<String, dynamic> json) =>
      _$GrowingSessionPictures$Query$PictureFromJson(json);

  String id;

  DateTime takenAt;

  String url;

  @override
  List<Object> get props => [id, takenAt, url];
  Map<String, dynamic> toJson() =>
      _$GrowingSessionPictures$Query$PictureToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GrowingSessionPictures$Query with EquatableMixin {
  GrowingSessionPictures$Query();

  factory GrowingSessionPictures$Query.fromJson(Map<String, dynamic> json) =>
      _$GrowingSessionPictures$QueryFromJson(json);

  List<GrowingSessionPictures$Query$Picture> pictures;

  @override
  List<Object> get props => [pictures];
  Map<String, dynamic> toJson() => _$GrowingSessionPictures$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeletePicture$Mutation$Picture with EquatableMixin {
  DeletePicture$Mutation$Picture();

  factory DeletePicture$Mutation$Picture.fromJson(Map<String, dynamic> json) =>
      _$DeletePicture$Mutation$PictureFromJson(json);

  String id;

  DateTime takenAt;

  @override
  List<Object> get props => [id, takenAt];
  Map<String, dynamic> toJson() => _$DeletePicture$Mutation$PictureToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeletePicture$Mutation with EquatableMixin {
  DeletePicture$Mutation();

  factory DeletePicture$Mutation.fromJson(Map<String, dynamic> json) =>
      _$DeletePicture$MutationFromJson(json);

  DeletePicture$Mutation$Picture deletepicture;

  @override
  List<Object> get props => [deletepicture];
  Map<String, dynamic> toJson() => _$DeletePicture$MutationToJson(this);
}

enum DataType {
  @JsonValue('VOLUME')
  volume,
  @JsonValue('POWER')
  power,
  @JsonValue('TEMPERATURE')
  temperature,
  @JsonValue('HUMIDITY')
  humidity,
  @JsonValue('HEIGHT')
  height,
  @JsonValue('WIDTH')
  width,
  @JsonValue('LENGTH')
  length,
  @JsonValue('DURATION')
  duration,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}
enum StrainPhotoperiod {
  @JsonValue('REGULAR')
  regular,
  @JsonValue('AUTOFLOWERING')
  autoflowering,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}
enum DeviceType {
  @JsonValue('MONITORING')
  monitoring,
  @JsonValue('MEASUREMENT')
  measurement,
  @JsonValue('VENTILATION')
  ventilation,
  @JsonValue('LIGHT')
  light,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}
enum DevelopmentStateType {
  @JsonValue('SEEDING')
  seeding,
  @JsonValue('GERMINATING')
  germinating,
  @JsonValue('GROWING')
  growing,
  @JsonValue('FLOWERING')
  flowering,
  @JsonValue('DRYING')
  drying,
  @JsonValue('CURING')
  curing,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}
enum GrowingSessionState {
  @JsonValue('PROJECT')
  project,
  @JsonValue('RUNNING')
  running,
  @JsonValue('TERMINATED')
  terminated,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}
enum GrowingSessionType {
  @JsonValue('INDOOR')
  indoor,
  @JsonValue('OUTDOOR')
  outdoor,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}
enum StrainType {
  @JsonValue('INDICA')
  indica,
  @JsonValue('SATIVA')
  sativa,
  @JsonValue('RUDERALIS')
  ruderalis,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}

@JsonSerializable(explicitToJson: true)
class LastMeasureArguments extends JsonSerializable with EquatableMixin {
  LastMeasureArguments({@required this.growingSessionId, @required this.type});

  @override
  factory LastMeasureArguments.fromJson(Map<String, dynamic> json) =>
      _$LastMeasureArgumentsFromJson(json);

  final String growingSessionId;

  @JsonKey(unknownEnumValue: DataType.artemisUnknown)
  final DataType type;

  @override
  List<Object> get props => [growingSessionId, type];
  @override
  Map<String, dynamic> toJson() => _$LastMeasureArgumentsToJson(this);
}

class LastMeasureQuery
    extends GraphQLQuery<LastMeasure$Query, LastMeasureArguments> {
  LastMeasureQuery({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'LastMeasure'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'growingSessionId')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'type')),
              type: NamedTypeNode(
                  name: NameNode(value: 'DataType'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'measure'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'growingSessionId'),
                    value: VariableNode(
                        name: NameNode(value: 'growingSessionId'))),
                ArgumentNode(
                    name: NameNode(value: 'type'),
                    value: VariableNode(name: NameNode(value: 'type')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'type'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'value'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'unit'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'date'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'LastMeasure';

  @override
  final LastMeasureArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  LastMeasure$Query parse(Map<String, dynamic> json) =>
      LastMeasure$Query.fromJson(json);
}

class StrainsQuery extends GraphQLQuery<Strains$Query, JsonSerializable> {
  StrainsQuery();

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'Strains'),
        variableDefinitions: [],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'strains'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'name'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'photoperiod'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'seedBank'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'name'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'Strains';

  @override
  List<Object> get props => [document, operationName];
  @override
  Strains$Query parse(Map<String, dynamic> json) =>
      Strains$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class DeviceInventoryArguments extends JsonSerializable with EquatableMixin {
  DeviceInventoryArguments({@required this.inventoryId});

  @override
  factory DeviceInventoryArguments.fromJson(Map<String, dynamic> json) =>
      _$DeviceInventoryArgumentsFromJson(json);

  final String inventoryId;

  @override
  List<Object> get props => [inventoryId];
  @override
  Map<String, dynamic> toJson() => _$DeviceInventoryArgumentsToJson(this);
}

class DeviceInventoryQuery
    extends GraphQLQuery<DeviceInventory$Query, DeviceInventoryArguments> {
  DeviceInventoryQuery({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'DeviceInventory'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'inventoryId')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'inventory'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'id'),
                    value: VariableNode(name: NameNode(value: 'inventoryId')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'deviceStock'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'id'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'count'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'device'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: SelectionSetNode(selections: [
                            FieldNode(
                                name: NameNode(value: 'id'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'name'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null)
                          ]))
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'DeviceInventory';

  @override
  final DeviceInventoryArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  DeviceInventory$Query parse(Map<String, dynamic> json) =>
      DeviceInventory$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class AllMeasuresArguments extends JsonSerializable with EquatableMixin {
  AllMeasuresArguments(
      {@required this.growingSessionId,
      @required this.type,
      @required this.count,
      @required this.offset});

  @override
  factory AllMeasuresArguments.fromJson(Map<String, dynamic> json) =>
      _$AllMeasuresArgumentsFromJson(json);

  final String growingSessionId;

  @JsonKey(unknownEnumValue: DataType.artemisUnknown)
  final DataType type;

  final int count;

  final int offset;

  @override
  List<Object> get props => [growingSessionId, type, count, offset];
  @override
  Map<String, dynamic> toJson() => _$AllMeasuresArgumentsToJson(this);
}

class AllMeasuresQuery
    extends GraphQLQuery<AllMeasures$Query, AllMeasuresArguments> {
  AllMeasuresQuery({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'AllMeasures'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'growingSessionId')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'type')),
              type: NamedTypeNode(
                  name: NameNode(value: 'DataType'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'count')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'offset')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'measures'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'growingSessionId'),
                    value: VariableNode(
                        name: NameNode(value: 'growingSessionId'))),
                ArgumentNode(
                    name: NameNode(value: 'type'),
                    value: VariableNode(name: NameNode(value: 'type'))),
                ArgumentNode(
                    name: NameNode(value: 'pagination'),
                    value: ObjectValueNode(fields: [
                      ObjectFieldNode(
                          name: NameNode(value: 'Count'),
                          value: VariableNode(name: NameNode(value: 'count'))),
                      ObjectFieldNode(
                          name: NameNode(value: 'Offset'),
                          value: VariableNode(name: NameNode(value: 'offset')))
                    ]))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'date'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'value'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'unit'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'AllMeasures';

  @override
  final AllMeasuresArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  AllMeasures$Query parse(Map<String, dynamic> json) =>
      AllMeasures$Query.fromJson(json);
}

class DevicesQuery extends GraphQLQuery<Devices$Query, JsonSerializable> {
  DevicesQuery();

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'Devices'),
        variableDefinitions: [],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'devices'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'name'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'type'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'Devices';

  @override
  List<Object> get props => [document, operationName];
  @override
  Devices$Query parse(Map<String, dynamic> json) =>
      Devices$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class SeedInventoryArguments extends JsonSerializable with EquatableMixin {
  SeedInventoryArguments({@required this.inventoryId});

  @override
  factory SeedInventoryArguments.fromJson(Map<String, dynamic> json) =>
      _$SeedInventoryArgumentsFromJson(json);

  final String inventoryId;

  @override
  List<Object> get props => [inventoryId];
  @override
  Map<String, dynamic> toJson() => _$SeedInventoryArgumentsToJson(this);
}

class SeedInventoryQuery
    extends GraphQLQuery<SeedInventory$Query, SeedInventoryArguments> {
  SeedInventoryQuery({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'SeedInventory'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'inventoryId')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'inventory'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'id'),
                    value: VariableNode(name: NameNode(value: 'inventoryId')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'seedStock'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'id'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'count'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'unit'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'strain'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: SelectionSetNode(selections: [
                            FieldNode(
                                name: NameNode(value: 'id'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'name'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'photoperiod'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null)
                          ]))
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'SeedInventory';

  @override
  final SeedInventoryArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  SeedInventory$Query parse(Map<String, dynamic> json) =>
      SeedInventory$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class AddPictureArguments extends JsonSerializable with EquatableMixin {
  AddPictureArguments({@required this.growingSessionId});

  @override
  factory AddPictureArguments.fromJson(Map<String, dynamic> json) =>
      _$AddPictureArgumentsFromJson(json);

  final String growingSessionId;

  @override
  List<Object> get props => [growingSessionId];
  @override
  Map<String, dynamic> toJson() => _$AddPictureArgumentsToJson(this);
}

class AddPictureMutation
    extends GraphQLQuery<AddPicture$Mutation, AddPictureArguments> {
  AddPictureMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'AddPicture'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'growingSessionId')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'addpicture'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'growingSessionId'),
                    value:
                        VariableNode(name: NameNode(value: 'growingSessionId')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'url'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'AddPicture';

  @override
  final AddPictureArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  AddPicture$Mutation parse(Map<String, dynamic> json) =>
      AddPicture$Mutation.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class DeleteStrainCannabinoidArguments extends JsonSerializable
    with EquatableMixin {
  DeleteStrainCannabinoidArguments(
      {@required this.strainId, @required this.cannabinoidId});

  @override
  factory DeleteStrainCannabinoidArguments.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteStrainCannabinoidArgumentsFromJson(json);

  final int strainId;

  final int cannabinoidId;

  @override
  List<Object> get props => [strainId, cannabinoidId];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteStrainCannabinoidArgumentsToJson(this);
}

class DeleteStrainCannabinoidMutation extends GraphQLQuery<
    DeleteStrainCannabinoid$Mutation, DeleteStrainCannabinoidArguments> {
  DeleteStrainCannabinoidMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'DeleteStrainCannabinoid'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'strainId')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'cannabinoidId')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'deletestraincannabinoid'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'strainId'),
                    value: VariableNode(name: NameNode(value: 'strainId'))),
                ArgumentNode(
                    name: NameNode(value: 'cannabinoidId'),
                    value: VariableNode(name: NameNode(value: 'cannabinoidId')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'DeleteStrainCannabinoid';

  @override
  final DeleteStrainCannabinoidArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  DeleteStrainCannabinoid$Mutation parse(Map<String, dynamic> json) =>
      DeleteStrainCannabinoid$Mutation.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class AddDeviceStockArguments extends JsonSerializable with EquatableMixin {
  AddDeviceStockArguments({@required this.inventoryId, @required this.input});

  @override
  factory AddDeviceStockArguments.fromJson(Map<String, dynamic> json) =>
      _$AddDeviceStockArgumentsFromJson(json);

  final String inventoryId;

  final DeviceStockInput input;

  @override
  List<Object> get props => [inventoryId, input];
  @override
  Map<String, dynamic> toJson() => _$AddDeviceStockArgumentsToJson(this);
}

class AddDeviceStockMutation
    extends GraphQLQuery<AddDeviceStock$Mutation, AddDeviceStockArguments> {
  AddDeviceStockMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'AddDeviceStock'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'inventoryId')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'input')),
              type: NamedTypeNode(
                  name: NameNode(value: 'DeviceStockInput'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'adddevicestock'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'inventoryId'),
                    value: VariableNode(name: NameNode(value: 'inventoryId'))),
                ArgumentNode(
                    name: NameNode(value: 'input'),
                    value: VariableNode(name: NameNode(value: 'input')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'AddDeviceStock';

  @override
  final AddDeviceStockArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  AddDeviceStock$Mutation parse(Map<String, dynamic> json) =>
      AddDeviceStock$Mutation.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class GrowingSessionDevicesArguments extends JsonSerializable
    with EquatableMixin {
  GrowingSessionDevicesArguments({@required this.growingSessionId});

  @override
  factory GrowingSessionDevicesArguments.fromJson(Map<String, dynamic> json) =>
      _$GrowingSessionDevicesArgumentsFromJson(json);

  final String growingSessionId;

  @override
  List<Object> get props => [growingSessionId];
  @override
  Map<String, dynamic> toJson() => _$GrowingSessionDevicesArgumentsToJson(this);
}

class GrowingSessionDevicesQuery extends GraphQLQuery<
    GrowingSessionDevices$Query, GrowingSessionDevicesArguments> {
  GrowingSessionDevicesQuery({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'GrowingSessionDevices'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'growingSessionId')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'devices'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'growingSessionId'),
                    value:
                        VariableNode(name: NameNode(value: 'growingSessionId')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'type'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'name'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'GrowingSessionDevices';

  @override
  final GrowingSessionDevicesArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  GrowingSessionDevices$Query parse(Map<String, dynamic> json) =>
      GrowingSessionDevices$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class PlantArguments extends JsonSerializable with EquatableMixin {
  PlantArguments({@required this.id});

  @override
  factory PlantArguments.fromJson(Map<String, dynamic> json) =>
      _$PlantArgumentsFromJson(json);

  final String id;

  @override
  List<Object> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$PlantArgumentsToJson(this);
}

class PlantQuery extends GraphQLQuery<Plant$Query, PlantArguments> {
  PlantQuery({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'Plant'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'id')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'plant'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'id'),
                    value: VariableNode(name: NameNode(value: 'id')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'name'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'startedAt'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'endedAt'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'states'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'id'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'developmentState'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'startedAt'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'endedAt'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'comments'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ])),
                FieldNode(
                    name: NameNode(value: 'strain'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'name'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'seedBank'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: SelectionSetNode(selections: [
                            FieldNode(
                                name: NameNode(value: 'name'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null)
                          ])),
                      FieldNode(
                          name: NameNode(value: 'floweringTimes'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: SelectionSetNode(selections: [
                            FieldNode(
                                name: NameNode(value: 'unit'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'value'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null)
                          ]))
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'Plant';

  @override
  final PlantArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  Plant$Query parse(Map<String, dynamic> json) => Plant$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class UpdateUserArguments extends JsonSerializable with EquatableMixin {
  UpdateUserArguments({@required this.input});

  @override
  factory UpdateUserArguments.fromJson(Map<String, dynamic> json) =>
      _$UpdateUserArgumentsFromJson(json);

  final UpdateUserInput input;

  @override
  List<Object> get props => [input];
  @override
  Map<String, dynamic> toJson() => _$UpdateUserArgumentsToJson(this);
}

class UpdateUserMutation
    extends GraphQLQuery<UpdateUser$Mutation, UpdateUserArguments> {
  UpdateUserMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'UpdateUser'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'input')),
              type: NamedTypeNode(
                  name: NameNode(value: 'UpdateUserInput'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'updateuser'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'input'),
                    value: VariableNode(name: NameNode(value: 'input')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'UpdateUser';

  @override
  final UpdateUserArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  UpdateUser$Mutation parse(Map<String, dynamic> json) =>
      UpdateUser$Mutation.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class AddSeedStockArguments extends JsonSerializable with EquatableMixin {
  AddSeedStockArguments({@required this.inventoryId, @required this.input});

  @override
  factory AddSeedStockArguments.fromJson(Map<String, dynamic> json) =>
      _$AddSeedStockArgumentsFromJson(json);

  final String inventoryId;

  final SeedStockInput input;

  @override
  List<Object> get props => [inventoryId, input];
  @override
  Map<String, dynamic> toJson() => _$AddSeedStockArgumentsToJson(this);
}

class AddSeedStockMutation
    extends GraphQLQuery<AddSeedStock$Mutation, AddSeedStockArguments> {
  AddSeedStockMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'AddSeedStock'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'inventoryId')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'input')),
              type: NamedTypeNode(
                  name: NameNode(value: 'SeedStockInput'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'addseedstock'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'inventoryId'),
                    value: VariableNode(name: NameNode(value: 'inventoryId'))),
                ArgumentNode(
                    name: NameNode(value: 'input'),
                    value: VariableNode(name: NameNode(value: 'input')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'AddSeedStock';

  @override
  final AddSeedStockArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  AddSeedStock$Mutation parse(Map<String, dynamic> json) =>
      AddSeedStock$Mutation.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class LoginArguments extends JsonSerializable with EquatableMixin {
  LoginArguments({@required this.name, this.password, this.otp});

  @override
  factory LoginArguments.fromJson(Map<String, dynamic> json) =>
      _$LoginArgumentsFromJson(json);

  final String name;

  final String password;

  final int otp;

  @override
  List<Object> get props => [name, password, otp];
  @override
  Map<String, dynamic> toJson() => _$LoginArgumentsToJson(this);
}

class LoginQuery extends GraphQLQuery<Login$Query, LoginArguments> {
  LoginQuery({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'Login'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'name')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'password')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'otp')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'login'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'name'),
                    value: VariableNode(name: NameNode(value: 'name'))),
                ArgumentNode(
                    name: NameNode(value: 'password'),
                    value: VariableNode(name: NameNode(value: 'password'))),
                ArgumentNode(
                    name: NameNode(value: 'otp'),
                    value: VariableNode(name: NameNode(value: 'otp')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'otp'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'token'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'user'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'email'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'inventory'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: SelectionSetNode(selections: [
                            FieldNode(
                                name: NameNode(value: 'id'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null)
                          ]))
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'Login';

  @override
  final LoginArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  Login$Query parse(Map<String, dynamic> json) => Login$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class AddStrainFloweringTimeInfosArguments extends JsonSerializable
    with EquatableMixin {
  AddStrainFloweringTimeInfosArguments(
      {@required this.strainId, @required this.input});

  @override
  factory AddStrainFloweringTimeInfosArguments.fromJson(
          Map<String, dynamic> json) =>
      _$AddStrainFloweringTimeInfosArgumentsFromJson(json);

  final int strainId;

  final List<MeasureInput> input;

  @override
  List<Object> get props => [strainId, input];
  @override
  Map<String, dynamic> toJson() =>
      _$AddStrainFloweringTimeInfosArgumentsToJson(this);
}

class AddStrainFloweringTimeInfosMutation extends GraphQLQuery<
    AddStrainFloweringTimeInfos$Mutation,
    AddStrainFloweringTimeInfosArguments> {
  AddStrainFloweringTimeInfosMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'AddStrainFloweringTimeInfos'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'strainId')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'input')),
              type: ListTypeNode(
                  type: NamedTypeNode(
                      name: NameNode(value: 'MeasureInput'), isNonNull: true),
                  isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'addstrainfloweringTimes'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'strainId'),
                    value: VariableNode(name: NameNode(value: 'strainId'))),
                ArgumentNode(
                    name: NameNode(value: 'input'),
                    value: VariableNode(name: NameNode(value: 'input')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'AddStrainFloweringTimeInfos';

  @override
  final AddStrainFloweringTimeInfosArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  AddStrainFloweringTimeInfos$Mutation parse(Map<String, dynamic> json) =>
      AddStrainFloweringTimeInfos$Mutation.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class AddGrowingSessionArguments extends JsonSerializable with EquatableMixin {
  AddGrowingSessionArguments({@required this.input});

  @override
  factory AddGrowingSessionArguments.fromJson(Map<String, dynamic> json) =>
      _$AddGrowingSessionArgumentsFromJson(json);

  final GrowingSessionInput input;

  @override
  List<Object> get props => [input];
  @override
  Map<String, dynamic> toJson() => _$AddGrowingSessionArgumentsToJson(this);
}

class AddGrowingSessionMutation extends GraphQLQuery<AddGrowingSession$Mutation,
    AddGrowingSessionArguments> {
  AddGrowingSessionMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'AddGrowingSession'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'input')),
              type: NamedTypeNode(
                  name: NameNode(value: 'GrowingSessionInput'),
                  isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'addgrowingsession'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'input'),
                    value: VariableNode(name: NameNode(value: 'input')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'name'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'AddGrowingSession';

  @override
  final AddGrowingSessionArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  AddGrowingSession$Mutation parse(Map<String, dynamic> json) =>
      AddGrowingSession$Mutation.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class DeleteStrainTypeInfosArguments extends JsonSerializable
    with EquatableMixin {
  DeleteStrainTypeInfosArguments(
      {@required this.strainId, @required this.straintypeinfosId});

  @override
  factory DeleteStrainTypeInfosArguments.fromJson(Map<String, dynamic> json) =>
      _$DeleteStrainTypeInfosArgumentsFromJson(json);

  final int strainId;

  final int straintypeinfosId;

  @override
  List<Object> get props => [strainId, straintypeinfosId];
  @override
  Map<String, dynamic> toJson() => _$DeleteStrainTypeInfosArgumentsToJson(this);
}

class DeleteStrainTypeInfosMutation extends GraphQLQuery<
    DeleteStrainTypeInfos$Mutation, DeleteStrainTypeInfosArguments> {
  DeleteStrainTypeInfosMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'DeleteStrainTypeInfos'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'strainId')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable:
                  VariableNode(name: NameNode(value: 'straintypeinfosId')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'deletestraintypeinfos'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'strainId'),
                    value: VariableNode(name: NameNode(value: 'strainId'))),
                ArgumentNode(
                    name: NameNode(value: 'straintypeinfosId'),
                    value: VariableNode(
                        name: NameNode(value: 'straintypeinfosId')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'DeleteStrainTypeInfos';

  @override
  final DeleteStrainTypeInfosArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  DeleteStrainTypeInfos$Mutation parse(Map<String, dynamic> json) =>
      DeleteStrainTypeInfos$Mutation.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class GrowingSessionArguments extends JsonSerializable with EquatableMixin {
  GrowingSessionArguments({@required this.id});

  @override
  factory GrowingSessionArguments.fromJson(Map<String, dynamic> json) =>
      _$GrowingSessionArgumentsFromJson(json);

  final String id;

  @override
  List<Object> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$GrowingSessionArgumentsToJson(this);
}

class GrowingSessionQuery
    extends GraphQLQuery<GrowingSession$Query, GrowingSessionArguments> {
  GrowingSessionQuery({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'GrowingSession'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'id')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'growingsession'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'id'),
                    value: VariableNode(name: NameNode(value: 'id')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'name'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'type'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'state'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'startedAt'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'endedAt'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'plants'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'id'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'name'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'strain'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: SelectionSetNode(selections: [
                            FieldNode(
                                name: NameNode(value: 'id'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'name'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null)
                          ])),
                      FieldNode(
                          name: NameNode(value: 'startedAt'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'endedAt'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'GrowingSession';

  @override
  final GrowingSessionArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  GrowingSession$Query parse(Map<String, dynamic> json) =>
      GrowingSession$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class UpdateFcmTokenArguments extends JsonSerializable with EquatableMixin {
  UpdateFcmTokenArguments({@required this.userId, @required this.fcmToken});

  @override
  factory UpdateFcmTokenArguments.fromJson(Map<String, dynamic> json) =>
      _$UpdateFcmTokenArgumentsFromJson(json);

  final String userId;

  final String fcmToken;

  @override
  List<Object> get props => [userId, fcmToken];
  @override
  Map<String, dynamic> toJson() => _$UpdateFcmTokenArgumentsToJson(this);
}

class UpdateFcmTokenMutation
    extends GraphQLQuery<UpdateFcmToken$Mutation, UpdateFcmTokenArguments> {
  UpdateFcmTokenMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'UpdateFcmToken'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'userId')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'fcmToken')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'updateuserfcmtoken'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'userId'),
                    value: VariableNode(name: NameNode(value: 'userId'))),
                ArgumentNode(
                    name: NameNode(value: 'fcmToken'),
                    value: VariableNode(name: NameNode(value: 'fcmToken')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'UpdateFcmToken';

  @override
  final UpdateFcmTokenArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  UpdateFcmToken$Mutation parse(Map<String, dynamic> json) =>
      UpdateFcmToken$Mutation.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class StrainArguments extends JsonSerializable with EquatableMixin {
  StrainArguments({@required this.id});

  @override
  factory StrainArguments.fromJson(Map<String, dynamic> json) =>
      _$StrainArgumentsFromJson(json);

  final int id;

  @override
  List<Object> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$StrainArgumentsToJson(this);
}

class StrainQuery extends GraphQLQuery<Strain$Query, StrainArguments> {
  StrainQuery({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'Strain'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'id')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'strain'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'id'),
                    value: VariableNode(name: NameNode(value: 'id')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'supplier'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'link'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'seedFinderStrainInfo'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'id'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'sfStrainHtmlDescription'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ])),
                FieldNode(
                    name: NameNode(value: 'seedBank'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'id'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'name'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'logoUrl'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ])),
                FieldNode(
                    name: NameNode(value: 'name'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'genetic'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'photoperiod'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'type'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'id'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'type'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'percentage'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ])),
                FieldNode(
                    name: NameNode(value: 'cannabinoids'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'id'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'name'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'percentage'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ])),
                FieldNode(
                    name: NameNode(value: 'floweringTimes'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'id'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'type'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'unit'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'value'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'Strain';

  @override
  final StrainArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  Strain$Query parse(Map<String, dynamic> json) => Strain$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class AddGrowStateArguments extends JsonSerializable with EquatableMixin {
  AddGrowStateArguments({@required this.plantId, @required this.input});

  @override
  factory AddGrowStateArguments.fromJson(Map<String, dynamic> json) =>
      _$AddGrowStateArgumentsFromJson(json);

  final String plantId;

  final GrowStateInput input;

  @override
  List<Object> get props => [plantId, input];
  @override
  Map<String, dynamic> toJson() => _$AddGrowStateArgumentsToJson(this);
}

class AddGrowStateMutation
    extends GraphQLQuery<AddGrowState$Mutation, AddGrowStateArguments> {
  AddGrowStateMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'AddGrowState'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'plantId')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'input')),
              type: NamedTypeNode(
                  name: NameNode(value: 'GrowStateInput'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'addgrowstate'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'plantId'),
                    value: VariableNode(name: NameNode(value: 'plantId'))),
                ArgumentNode(
                    name: NameNode(value: 'input'),
                    value: VariableNode(name: NameNode(value: 'input')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'developmentState'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'AddGrowState';

  @override
  final AddGrowStateArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  AddGrowState$Mutation parse(Map<String, dynamic> json) =>
      AddGrowState$Mutation.fromJson(json);
}

class GrowingSessionsQuery
    extends GraphQLQuery<GrowingSessions$Query, JsonSerializable> {
  GrowingSessionsQuery();

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'GrowingSessions'),
        variableDefinitions: [],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'growingsessions'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'name'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'type'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'state'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'startedAt'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'endedAt'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'GrowingSessions';

  @override
  List<Object> get props => [document, operationName];
  @override
  GrowingSessions$Query parse(Map<String, dynamic> json) =>
      GrowingSessions$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class GrowingSessionPicturesArguments extends JsonSerializable
    with EquatableMixin {
  GrowingSessionPicturesArguments({@required this.growingSessionId});

  @override
  factory GrowingSessionPicturesArguments.fromJson(Map<String, dynamic> json) =>
      _$GrowingSessionPicturesArgumentsFromJson(json);

  final String growingSessionId;

  @override
  List<Object> get props => [growingSessionId];
  @override
  Map<String, dynamic> toJson() =>
      _$GrowingSessionPicturesArgumentsToJson(this);
}

class GrowingSessionPicturesQuery extends GraphQLQuery<
    GrowingSessionPictures$Query, GrowingSessionPicturesArguments> {
  GrowingSessionPicturesQuery({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'GrowingSessionPictures'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'growingSessionId')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'pictures'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'growingSessionId'),
                    value:
                        VariableNode(name: NameNode(value: 'growingSessionId')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'takenAt'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'url'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'GrowingSessionPictures';

  @override
  final GrowingSessionPicturesArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  GrowingSessionPictures$Query parse(Map<String, dynamic> json) =>
      GrowingSessionPictures$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class DeletePictureArguments extends JsonSerializable with EquatableMixin {
  DeletePictureArguments({@required this.id});

  @override
  factory DeletePictureArguments.fromJson(Map<String, dynamic> json) =>
      _$DeletePictureArgumentsFromJson(json);

  final String id;

  @override
  List<Object> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$DeletePictureArgumentsToJson(this);
}

class DeletePictureMutation
    extends GraphQLQuery<DeletePicture$Mutation, DeletePictureArguments> {
  DeletePictureMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'DeletePicture'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'id')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'deletepicture'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'id'),
                    value: VariableNode(name: NameNode(value: 'id')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'takenAt'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'DeletePicture';

  @override
  final DeletePictureArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  DeletePicture$Mutation parse(Map<String, dynamic> json) =>
      DeletePicture$Mutation.fromJson(json);
}
