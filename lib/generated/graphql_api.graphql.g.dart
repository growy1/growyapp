// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'graphql_api.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LastMeasure$Query$Measure _$LastMeasure$Query$MeasureFromJson(
    Map<String, dynamic> json) {
  return LastMeasure$Query$Measure()
    ..id = json['id'] as String
    ..type = _$enumDecodeNullable(_$DataTypeEnumMap, json['type'],
        unknownValue: DataType.artemisUnknown)
    ..value = (json['value'] as num)?.toDouble()
    ..unit = json['unit'] as String
    ..date =
        json['date'] == null ? null : DateTime.parse(json['date'] as String);
}

Map<String, dynamic> _$LastMeasure$Query$MeasureToJson(
        LastMeasure$Query$Measure instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': _$DataTypeEnumMap[instance.type],
      'value': instance.value,
      'unit': instance.unit,
      'date': instance.date?.toIso8601String(),
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$DataTypeEnumMap = {
  DataType.volume: 'VOLUME',
  DataType.power: 'POWER',
  DataType.temperature: 'TEMPERATURE',
  DataType.humidity: 'HUMIDITY',
  DataType.height: 'HEIGHT',
  DataType.width: 'WIDTH',
  DataType.length: 'LENGTH',
  DataType.duration: 'DURATION',
  DataType.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

LastMeasure$Query _$LastMeasure$QueryFromJson(Map<String, dynamic> json) {
  return LastMeasure$Query()
    ..measure = json['measure'] == null
        ? null
        : LastMeasure$Query$Measure.fromJson(
            json['measure'] as Map<String, dynamic>);
}

Map<String, dynamic> _$LastMeasure$QueryToJson(LastMeasure$Query instance) =>
    <String, dynamic>{
      'measure': instance.measure?.toJson(),
    };

Strains$Query$Strain$SeedBank _$Strains$Query$Strain$SeedBankFromJson(
    Map<String, dynamic> json) {
  return Strains$Query$Strain$SeedBank()..name = json['name'] as String;
}

Map<String, dynamic> _$Strains$Query$Strain$SeedBankToJson(
        Strains$Query$Strain$SeedBank instance) =>
    <String, dynamic>{
      'name': instance.name,
    };

Strains$Query$Strain _$Strains$Query$StrainFromJson(Map<String, dynamic> json) {
  return Strains$Query$Strain()
    ..id = json['id'] as int
    ..name = json['name'] as String
    ..photoperiod = _$enumDecodeNullable(
        _$StrainPhotoperiodEnumMap, json['photoperiod'],
        unknownValue: StrainPhotoperiod.artemisUnknown)
    ..seedBank = json['seedBank'] == null
        ? null
        : Strains$Query$Strain$SeedBank.fromJson(
            json['seedBank'] as Map<String, dynamic>);
}

Map<String, dynamic> _$Strains$Query$StrainToJson(
        Strains$Query$Strain instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'photoperiod': _$StrainPhotoperiodEnumMap[instance.photoperiod],
      'seedBank': instance.seedBank?.toJson(),
    };

const _$StrainPhotoperiodEnumMap = {
  StrainPhotoperiod.regular: 'REGULAR',
  StrainPhotoperiod.autoflowering: 'AUTOFLOWERING',
  StrainPhotoperiod.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

Strains$Query _$Strains$QueryFromJson(Map<String, dynamic> json) {
  return Strains$Query()
    ..strains = (json['strains'] as List)
        ?.map((e) => e == null
            ? null
            : Strains$Query$Strain.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$Strains$QueryToJson(Strains$Query instance) =>
    <String, dynamic>{
      'strains': instance.strains?.map((e) => e?.toJson())?.toList(),
    };

DeviceInventory$Query$Inventory$DeviceStock$Device
    _$DeviceInventory$Query$Inventory$DeviceStock$DeviceFromJson(
        Map<String, dynamic> json) {
  return DeviceInventory$Query$Inventory$DeviceStock$Device()
    ..id = json['id'] as int
    ..name = json['name'] as String;
}

Map<String, dynamic> _$DeviceInventory$Query$Inventory$DeviceStock$DeviceToJson(
        DeviceInventory$Query$Inventory$DeviceStock$Device instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };

DeviceInventory$Query$Inventory$DeviceStock
    _$DeviceInventory$Query$Inventory$DeviceStockFromJson(
        Map<String, dynamic> json) {
  return DeviceInventory$Query$Inventory$DeviceStock()
    ..id = json['id'] as String
    ..count = (json['count'] as num)?.toDouble()
    ..device = json['device'] == null
        ? null
        : DeviceInventory$Query$Inventory$DeviceStock$Device.fromJson(
            json['device'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DeviceInventory$Query$Inventory$DeviceStockToJson(
        DeviceInventory$Query$Inventory$DeviceStock instance) =>
    <String, dynamic>{
      'id': instance.id,
      'count': instance.count,
      'device': instance.device?.toJson(),
    };

DeviceInventory$Query$Inventory _$DeviceInventory$Query$InventoryFromJson(
    Map<String, dynamic> json) {
  return DeviceInventory$Query$Inventory()
    ..deviceStock = (json['deviceStock'] as List)
        ?.map((e) => e == null
            ? null
            : DeviceInventory$Query$Inventory$DeviceStock.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$DeviceInventory$Query$InventoryToJson(
        DeviceInventory$Query$Inventory instance) =>
    <String, dynamic>{
      'deviceStock': instance.deviceStock?.map((e) => e?.toJson())?.toList(),
    };

DeviceInventory$Query _$DeviceInventory$QueryFromJson(
    Map<String, dynamic> json) {
  return DeviceInventory$Query()
    ..inventory = json['inventory'] == null
        ? null
        : DeviceInventory$Query$Inventory.fromJson(
            json['inventory'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DeviceInventory$QueryToJson(
        DeviceInventory$Query instance) =>
    <String, dynamic>{
      'inventory': instance.inventory?.toJson(),
    };

AllMeasures$Query$Measure _$AllMeasures$Query$MeasureFromJson(
    Map<String, dynamic> json) {
  return AllMeasures$Query$Measure()
    ..date =
        json['date'] == null ? null : DateTime.parse(json['date'] as String)
    ..value = (json['value'] as num)?.toDouble()
    ..unit = json['unit'] as String;
}

Map<String, dynamic> _$AllMeasures$Query$MeasureToJson(
        AllMeasures$Query$Measure instance) =>
    <String, dynamic>{
      'date': instance.date?.toIso8601String(),
      'value': instance.value,
      'unit': instance.unit,
    };

AllMeasures$Query _$AllMeasures$QueryFromJson(Map<String, dynamic> json) {
  return AllMeasures$Query()
    ..measures = (json['measures'] as List)
        ?.map((e) => e == null
            ? null
            : AllMeasures$Query$Measure.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$AllMeasures$QueryToJson(AllMeasures$Query instance) =>
    <String, dynamic>{
      'measures': instance.measures?.map((e) => e?.toJson())?.toList(),
    };

Devices$Query$Device _$Devices$Query$DeviceFromJson(Map<String, dynamic> json) {
  return Devices$Query$Device()
    ..id = json['id'] as int
    ..name = json['name'] as String
    ..type = _$enumDecodeNullable(_$DeviceTypeEnumMap, json['type'],
        unknownValue: DeviceType.artemisUnknown);
}

Map<String, dynamic> _$Devices$Query$DeviceToJson(
        Devices$Query$Device instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'type': _$DeviceTypeEnumMap[instance.type],
    };

const _$DeviceTypeEnumMap = {
  DeviceType.monitoring: 'MONITORING',
  DeviceType.measurement: 'MEASUREMENT',
  DeviceType.ventilation: 'VENTILATION',
  DeviceType.light: 'LIGHT',
  DeviceType.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

Devices$Query _$Devices$QueryFromJson(Map<String, dynamic> json) {
  return Devices$Query()
    ..devices = (json['devices'] as List)
        ?.map((e) => e == null
            ? null
            : Devices$Query$Device.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$Devices$QueryToJson(Devices$Query instance) =>
    <String, dynamic>{
      'devices': instance.devices?.map((e) => e?.toJson())?.toList(),
    };

SeedInventory$Query$Inventory$SeedStock$Strain
    _$SeedInventory$Query$Inventory$SeedStock$StrainFromJson(
        Map<String, dynamic> json) {
  return SeedInventory$Query$Inventory$SeedStock$Strain()
    ..id = json['id'] as int
    ..name = json['name'] as String
    ..photoperiod = _$enumDecodeNullable(
        _$StrainPhotoperiodEnumMap, json['photoperiod'],
        unknownValue: StrainPhotoperiod.artemisUnknown);
}

Map<String, dynamic> _$SeedInventory$Query$Inventory$SeedStock$StrainToJson(
        SeedInventory$Query$Inventory$SeedStock$Strain instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'photoperiod': _$StrainPhotoperiodEnumMap[instance.photoperiod],
    };

SeedInventory$Query$Inventory$SeedStock
    _$SeedInventory$Query$Inventory$SeedStockFromJson(
        Map<String, dynamic> json) {
  return SeedInventory$Query$Inventory$SeedStock()
    ..id = json['id'] as String
    ..count = (json['count'] as num)?.toDouble()
    ..unit = json['unit'] as String
    ..strain = json['strain'] == null
        ? null
        : SeedInventory$Query$Inventory$SeedStock$Strain.fromJson(
            json['strain'] as Map<String, dynamic>);
}

Map<String, dynamic> _$SeedInventory$Query$Inventory$SeedStockToJson(
        SeedInventory$Query$Inventory$SeedStock instance) =>
    <String, dynamic>{
      'id': instance.id,
      'count': instance.count,
      'unit': instance.unit,
      'strain': instance.strain?.toJson(),
    };

SeedInventory$Query$Inventory _$SeedInventory$Query$InventoryFromJson(
    Map<String, dynamic> json) {
  return SeedInventory$Query$Inventory()
    ..seedStock = (json['seedStock'] as List)
        ?.map((e) => e == null
            ? null
            : SeedInventory$Query$Inventory$SeedStock.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$SeedInventory$Query$InventoryToJson(
        SeedInventory$Query$Inventory instance) =>
    <String, dynamic>{
      'seedStock': instance.seedStock?.map((e) => e?.toJson())?.toList(),
    };

SeedInventory$Query _$SeedInventory$QueryFromJson(Map<String, dynamic> json) {
  return SeedInventory$Query()
    ..inventory = json['inventory'] == null
        ? null
        : SeedInventory$Query$Inventory.fromJson(
            json['inventory'] as Map<String, dynamic>);
}

Map<String, dynamic> _$SeedInventory$QueryToJson(
        SeedInventory$Query instance) =>
    <String, dynamic>{
      'inventory': instance.inventory?.toJson(),
    };

AddPicture$Mutation$Picture _$AddPicture$Mutation$PictureFromJson(
    Map<String, dynamic> json) {
  return AddPicture$Mutation$Picture()..url = json['url'] as String;
}

Map<String, dynamic> _$AddPicture$Mutation$PictureToJson(
        AddPicture$Mutation$Picture instance) =>
    <String, dynamic>{
      'url': instance.url,
    };

AddPicture$Mutation _$AddPicture$MutationFromJson(Map<String, dynamic> json) {
  return AddPicture$Mutation()
    ..addpicture = json['addpicture'] == null
        ? null
        : AddPicture$Mutation$Picture.fromJson(
            json['addpicture'] as Map<String, dynamic>);
}

Map<String, dynamic> _$AddPicture$MutationToJson(
        AddPicture$Mutation instance) =>
    <String, dynamic>{
      'addpicture': instance.addpicture?.toJson(),
    };

DeleteStrainCannabinoid$Mutation$Cannabinoid
    _$DeleteStrainCannabinoid$Mutation$CannabinoidFromJson(
        Map<String, dynamic> json) {
  return DeleteStrainCannabinoid$Mutation$Cannabinoid()..id = json['id'] as int;
}

Map<String, dynamic> _$DeleteStrainCannabinoid$Mutation$CannabinoidToJson(
        DeleteStrainCannabinoid$Mutation$Cannabinoid instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

DeleteStrainCannabinoid$Mutation _$DeleteStrainCannabinoid$MutationFromJson(
    Map<String, dynamic> json) {
  return DeleteStrainCannabinoid$Mutation()
    ..deletestraincannabinoid = json['deletestraincannabinoid'] == null
        ? null
        : DeleteStrainCannabinoid$Mutation$Cannabinoid.fromJson(
            json['deletestraincannabinoid'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DeleteStrainCannabinoid$MutationToJson(
        DeleteStrainCannabinoid$Mutation instance) =>
    <String, dynamic>{
      'deletestraincannabinoid': instance.deletestraincannabinoid?.toJson(),
    };

AddDeviceStock$Mutation$DeviceStock
    _$AddDeviceStock$Mutation$DeviceStockFromJson(Map<String, dynamic> json) {
  return AddDeviceStock$Mutation$DeviceStock()..id = json['id'] as String;
}

Map<String, dynamic> _$AddDeviceStock$Mutation$DeviceStockToJson(
        AddDeviceStock$Mutation$DeviceStock instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

AddDeviceStock$Mutation _$AddDeviceStock$MutationFromJson(
    Map<String, dynamic> json) {
  return AddDeviceStock$Mutation()
    ..adddevicestock = json['adddevicestock'] == null
        ? null
        : AddDeviceStock$Mutation$DeviceStock.fromJson(
            json['adddevicestock'] as Map<String, dynamic>);
}

Map<String, dynamic> _$AddDeviceStock$MutationToJson(
        AddDeviceStock$Mutation instance) =>
    <String, dynamic>{
      'adddevicestock': instance.adddevicestock?.toJson(),
    };

DeviceStockInput _$DeviceStockInputFromJson(Map<String, dynamic> json) {
  return DeviceStockInput(
    deviceId: json['deviceId'] as int,
    count: (json['count'] as num)?.toDouble(),
    unit: json['unit'] as String,
  );
}

Map<String, dynamic> _$DeviceStockInputToJson(DeviceStockInput instance) =>
    <String, dynamic>{
      'deviceId': instance.deviceId,
      'count': instance.count,
      'unit': instance.unit,
    };

GrowingSessionDevices$Query$Device _$GrowingSessionDevices$Query$DeviceFromJson(
    Map<String, dynamic> json) {
  return GrowingSessionDevices$Query$Device()
    ..id = json['id'] as int
    ..type = _$enumDecodeNullable(_$DeviceTypeEnumMap, json['type'],
        unknownValue: DeviceType.artemisUnknown)
    ..name = json['name'] as String;
}

Map<String, dynamic> _$GrowingSessionDevices$Query$DeviceToJson(
        GrowingSessionDevices$Query$Device instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': _$DeviceTypeEnumMap[instance.type],
      'name': instance.name,
    };

GrowingSessionDevices$Query _$GrowingSessionDevices$QueryFromJson(
    Map<String, dynamic> json) {
  return GrowingSessionDevices$Query()
    ..devices = (json['devices'] as List)
        ?.map((e) => e == null
            ? null
            : GrowingSessionDevices$Query$Device.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$GrowingSessionDevices$QueryToJson(
        GrowingSessionDevices$Query instance) =>
    <String, dynamic>{
      'devices': instance.devices?.map((e) => e?.toJson())?.toList(),
    };

Plant$Query$Plant$GrowState _$Plant$Query$Plant$GrowStateFromJson(
    Map<String, dynamic> json) {
  return Plant$Query$Plant$GrowState()
    ..id = json['id'] as String
    ..developmentState = _$enumDecodeNullable(
        _$DevelopmentStateTypeEnumMap, json['developmentState'],
        unknownValue: DevelopmentStateType.artemisUnknown)
    ..startedAt = json['startedAt'] == null
        ? null
        : DateTime.parse(json['startedAt'] as String)
    ..endedAt = json['endedAt'] == null
        ? null
        : DateTime.parse(json['endedAt'] as String)
    ..comments = json['comments'] as String;
}

Map<String, dynamic> _$Plant$Query$Plant$GrowStateToJson(
        Plant$Query$Plant$GrowState instance) =>
    <String, dynamic>{
      'id': instance.id,
      'developmentState':
          _$DevelopmentStateTypeEnumMap[instance.developmentState],
      'startedAt': instance.startedAt?.toIso8601String(),
      'endedAt': instance.endedAt?.toIso8601String(),
      'comments': instance.comments,
    };

const _$DevelopmentStateTypeEnumMap = {
  DevelopmentStateType.seeding: 'SEEDING',
  DevelopmentStateType.germinating: 'GERMINATING',
  DevelopmentStateType.growing: 'GROWING',
  DevelopmentStateType.flowering: 'FLOWERING',
  DevelopmentStateType.drying: 'DRYING',
  DevelopmentStateType.curing: 'CURING',
  DevelopmentStateType.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

Plant$Query$Plant$Strain$SeedBank _$Plant$Query$Plant$Strain$SeedBankFromJson(
    Map<String, dynamic> json) {
  return Plant$Query$Plant$Strain$SeedBank()..name = json['name'] as String;
}

Map<String, dynamic> _$Plant$Query$Plant$Strain$SeedBankToJson(
        Plant$Query$Plant$Strain$SeedBank instance) =>
    <String, dynamic>{
      'name': instance.name,
    };

Plant$Query$Plant$Strain$Measure _$Plant$Query$Plant$Strain$MeasureFromJson(
    Map<String, dynamic> json) {
  return Plant$Query$Plant$Strain$Measure()
    ..unit = json['unit'] as String
    ..value = (json['value'] as num)?.toDouble();
}

Map<String, dynamic> _$Plant$Query$Plant$Strain$MeasureToJson(
        Plant$Query$Plant$Strain$Measure instance) =>
    <String, dynamic>{
      'unit': instance.unit,
      'value': instance.value,
    };

Plant$Query$Plant$Strain _$Plant$Query$Plant$StrainFromJson(
    Map<String, dynamic> json) {
  return Plant$Query$Plant$Strain()
    ..name = json['name'] as String
    ..seedBank = json['seedBank'] == null
        ? null
        : Plant$Query$Plant$Strain$SeedBank.fromJson(
            json['seedBank'] as Map<String, dynamic>)
    ..floweringTimes = (json['floweringTimes'] as List)
        ?.map((e) => e == null
            ? null
            : Plant$Query$Plant$Strain$Measure.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$Plant$Query$Plant$StrainToJson(
        Plant$Query$Plant$Strain instance) =>
    <String, dynamic>{
      'name': instance.name,
      'seedBank': instance.seedBank?.toJson(),
      'floweringTimes':
          instance.floweringTimes?.map((e) => e?.toJson())?.toList(),
    };

Plant$Query$Plant _$Plant$Query$PlantFromJson(Map<String, dynamic> json) {
  return Plant$Query$Plant()
    ..name = json['name'] as String
    ..startedAt = json['startedAt'] == null
        ? null
        : DateTime.parse(json['startedAt'] as String)
    ..endedAt = json['endedAt'] == null
        ? null
        : DateTime.parse(json['endedAt'] as String)
    ..states = (json['states'] as List)
        ?.map((e) => e == null
            ? null
            : Plant$Query$Plant$GrowState.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..strain = json['strain'] == null
        ? null
        : Plant$Query$Plant$Strain.fromJson(
            json['strain'] as Map<String, dynamic>);
}

Map<String, dynamic> _$Plant$Query$PlantToJson(Plant$Query$Plant instance) =>
    <String, dynamic>{
      'name': instance.name,
      'startedAt': instance.startedAt?.toIso8601String(),
      'endedAt': instance.endedAt?.toIso8601String(),
      'states': instance.states?.map((e) => e?.toJson())?.toList(),
      'strain': instance.strain?.toJson(),
    };

Plant$Query _$Plant$QueryFromJson(Map<String, dynamic> json) {
  return Plant$Query()
    ..plant = json['plant'] == null
        ? null
        : Plant$Query$Plant.fromJson(json['plant'] as Map<String, dynamic>);
}

Map<String, dynamic> _$Plant$QueryToJson(Plant$Query instance) =>
    <String, dynamic>{
      'plant': instance.plant?.toJson(),
    };

UpdateUser$Mutation$User _$UpdateUser$Mutation$UserFromJson(
    Map<String, dynamic> json) {
  return UpdateUser$Mutation$User()..id = json['id'] as String;
}

Map<String, dynamic> _$UpdateUser$Mutation$UserToJson(
        UpdateUser$Mutation$User instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

UpdateUser$Mutation _$UpdateUser$MutationFromJson(Map<String, dynamic> json) {
  return UpdateUser$Mutation()
    ..updateuser = json['updateuser'] == null
        ? null
        : UpdateUser$Mutation$User.fromJson(
            json['updateuser'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UpdateUser$MutationToJson(
        UpdateUser$Mutation instance) =>
    <String, dynamic>{
      'updateuser': instance.updateuser?.toJson(),
    };

UpdateUserInput _$UpdateUserInputFromJson(Map<String, dynamic> json) {
  return UpdateUserInput(
    id: json['id'] as String,
    name: json['name'] as String,
    email: json['email'] as String,
    password: json['password'] as String,
    inventory: json['inventory'] as bool,
  );
}

Map<String, dynamic> _$UpdateUserInputToJson(UpdateUserInput instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'email': instance.email,
      'password': instance.password,
      'inventory': instance.inventory,
    };

AddSeedStock$Mutation$SeedStock _$AddSeedStock$Mutation$SeedStockFromJson(
    Map<String, dynamic> json) {
  return AddSeedStock$Mutation$SeedStock()..id = json['id'] as String;
}

Map<String, dynamic> _$AddSeedStock$Mutation$SeedStockToJson(
        AddSeedStock$Mutation$SeedStock instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

AddSeedStock$Mutation _$AddSeedStock$MutationFromJson(
    Map<String, dynamic> json) {
  return AddSeedStock$Mutation()
    ..addseedstock = json['addseedstock'] == null
        ? null
        : AddSeedStock$Mutation$SeedStock.fromJson(
            json['addseedstock'] as Map<String, dynamic>);
}

Map<String, dynamic> _$AddSeedStock$MutationToJson(
        AddSeedStock$Mutation instance) =>
    <String, dynamic>{
      'addseedstock': instance.addseedstock?.toJson(),
    };

SeedStockInput _$SeedStockInputFromJson(Map<String, dynamic> json) {
  return SeedStockInput(
    strainId: json['strainId'] as int,
    count: (json['count'] as num)?.toDouble(),
    unit: json['unit'] as String,
  );
}

Map<String, dynamic> _$SeedStockInputToJson(SeedStockInput instance) =>
    <String, dynamic>{
      'strainId': instance.strainId,
      'count': instance.count,
      'unit': instance.unit,
    };

Login$Query$Session$User$Inventory _$Login$Query$Session$User$InventoryFromJson(
    Map<String, dynamic> json) {
  return Login$Query$Session$User$Inventory()..id = json['id'] as String;
}

Map<String, dynamic> _$Login$Query$Session$User$InventoryToJson(
        Login$Query$Session$User$Inventory instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

Login$Query$Session$User _$Login$Query$Session$UserFromJson(
    Map<String, dynamic> json) {
  return Login$Query$Session$User()
    ..email = json['email'] as String
    ..inventory = json['inventory'] == null
        ? null
        : Login$Query$Session$User$Inventory.fromJson(
            json['inventory'] as Map<String, dynamic>);
}

Map<String, dynamic> _$Login$Query$Session$UserToJson(
        Login$Query$Session$User instance) =>
    <String, dynamic>{
      'email': instance.email,
      'inventory': instance.inventory?.toJson(),
    };

Login$Query$Session _$Login$Query$SessionFromJson(Map<String, dynamic> json) {
  return Login$Query$Session()
    ..otp = json['otp'] as bool
    ..token = json['token'] as String
    ..user = json['user'] == null
        ? null
        : Login$Query$Session$User.fromJson(
            json['user'] as Map<String, dynamic>);
}

Map<String, dynamic> _$Login$Query$SessionToJson(
        Login$Query$Session instance) =>
    <String, dynamic>{
      'otp': instance.otp,
      'token': instance.token,
      'user': instance.user?.toJson(),
    };

Login$Query _$Login$QueryFromJson(Map<String, dynamic> json) {
  return Login$Query()
    ..login = json['login'] == null
        ? null
        : Login$Query$Session.fromJson(json['login'] as Map<String, dynamic>);
}

Map<String, dynamic> _$Login$QueryToJson(Login$Query instance) =>
    <String, dynamic>{
      'login': instance.login?.toJson(),
    };

AddStrainFloweringTimeInfos$Mutation$Measure
    _$AddStrainFloweringTimeInfos$Mutation$MeasureFromJson(
        Map<String, dynamic> json) {
  return AddStrainFloweringTimeInfos$Mutation$Measure()
    ..id = json['id'] as String;
}

Map<String, dynamic> _$AddStrainFloweringTimeInfos$Mutation$MeasureToJson(
        AddStrainFloweringTimeInfos$Mutation$Measure instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

AddStrainFloweringTimeInfos$Mutation
    _$AddStrainFloweringTimeInfos$MutationFromJson(Map<String, dynamic> json) {
  return AddStrainFloweringTimeInfos$Mutation()
    ..addstrainfloweringTimes = (json['addstrainfloweringTimes'] as List)
        ?.map((e) => e == null
            ? null
            : AddStrainFloweringTimeInfos$Mutation$Measure.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$AddStrainFloweringTimeInfos$MutationToJson(
        AddStrainFloweringTimeInfos$Mutation instance) =>
    <String, dynamic>{
      'addstrainfloweringTimes':
          instance.addstrainfloweringTimes?.map((e) => e?.toJson())?.toList(),
    };

MeasureInput _$MeasureInputFromJson(Map<String, dynamic> json) {
  return MeasureInput(
    date: json['date'] == null ? null : DateTime.parse(json['date'] as String),
    type: _$enumDecodeNullable(_$DataTypeEnumMap, json['type'],
        unknownValue: DataType.artemisUnknown),
    value: (json['value'] as num)?.toDouble(),
    unit: json['unit'] as String,
  );
}

Map<String, dynamic> _$MeasureInputToJson(MeasureInput instance) =>
    <String, dynamic>{
      'date': instance.date?.toIso8601String(),
      'type': _$DataTypeEnumMap[instance.type],
      'value': instance.value,
      'unit': instance.unit,
    };

AddGrowingSession$Mutation$GrowingSession
    _$AddGrowingSession$Mutation$GrowingSessionFromJson(
        Map<String, dynamic> json) {
  return AddGrowingSession$Mutation$GrowingSession()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic> _$AddGrowingSession$Mutation$GrowingSessionToJson(
        AddGrowingSession$Mutation$GrowingSession instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };

AddGrowingSession$Mutation _$AddGrowingSession$MutationFromJson(
    Map<String, dynamic> json) {
  return AddGrowingSession$Mutation()
    ..addgrowingsession = json['addgrowingsession'] == null
        ? null
        : AddGrowingSession$Mutation$GrowingSession.fromJson(
            json['addgrowingsession'] as Map<String, dynamic>);
}

Map<String, dynamic> _$AddGrowingSession$MutationToJson(
        AddGrowingSession$Mutation instance) =>
    <String, dynamic>{
      'addgrowingsession': instance.addgrowingsession?.toJson(),
    };

GrowingSessionInput _$GrowingSessionInputFromJson(Map<String, dynamic> json) {
  return GrowingSessionInput(
    name: json['name'] as String,
    type: _$enumDecodeNullable(_$GrowingSessionTypeEnumMap, json['type'],
        unknownValue: GrowingSessionType.artemisUnknown),
    state: _$enumDecodeNullable(_$GrowingSessionStateEnumMap, json['state'],
        unknownValue: GrowingSessionState.artemisUnknown),
    startedAt: json['startedAt'] == null
        ? null
        : DateTime.parse(json['startedAt'] as String),
    endedAt: json['endedAt'] == null
        ? null
        : DateTime.parse(json['endedAt'] as String),
    boxVolume: (json['boxVolume'] as List)
        ?.map((e) =>
            e == null ? null : MeasureInput.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    plants: (json['plants'] as List)
        ?.map((e) =>
            e == null ? null : PlantInput.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    userId: json['userId'] as String,
  );
}

Map<String, dynamic> _$GrowingSessionInputToJson(
        GrowingSessionInput instance) =>
    <String, dynamic>{
      'name': instance.name,
      'type': _$GrowingSessionTypeEnumMap[instance.type],
      'state': _$GrowingSessionStateEnumMap[instance.state],
      'startedAt': instance.startedAt?.toIso8601String(),
      'endedAt': instance.endedAt?.toIso8601String(),
      'boxVolume': instance.boxVolume?.map((e) => e?.toJson())?.toList(),
      'plants': instance.plants?.map((e) => e?.toJson())?.toList(),
      'userId': instance.userId,
    };

const _$GrowingSessionTypeEnumMap = {
  GrowingSessionType.indoor: 'INDOOR',
  GrowingSessionType.outdoor: 'OUTDOOR',
  GrowingSessionType.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

const _$GrowingSessionStateEnumMap = {
  GrowingSessionState.project: 'PROJECT',
  GrowingSessionState.running: 'RUNNING',
  GrowingSessionState.terminated: 'TERMINATED',
  GrowingSessionState.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

PlantInput _$PlantInputFromJson(Map<String, dynamic> json) {
  return PlantInput(
    name: json['name'] as String,
    startedAt: json['startedAt'] == null
        ? null
        : DateTime.parse(json['startedAt'] as String),
    endedAt: json['endedAt'] == null
        ? null
        : DateTime.parse(json['endedAt'] as String),
    strainId: json['strainId'] as int,
  );
}

Map<String, dynamic> _$PlantInputToJson(PlantInput instance) =>
    <String, dynamic>{
      'name': instance.name,
      'startedAt': instance.startedAt?.toIso8601String(),
      'endedAt': instance.endedAt?.toIso8601String(),
      'strainId': instance.strainId,
    };

DeleteStrainTypeInfos$Mutation$StrainTypeInfos
    _$DeleteStrainTypeInfos$Mutation$StrainTypeInfosFromJson(
        Map<String, dynamic> json) {
  return DeleteStrainTypeInfos$Mutation$StrainTypeInfos()
    ..id = json['id'] as int;
}

Map<String, dynamic> _$DeleteStrainTypeInfos$Mutation$StrainTypeInfosToJson(
        DeleteStrainTypeInfos$Mutation$StrainTypeInfos instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

DeleteStrainTypeInfos$Mutation _$DeleteStrainTypeInfos$MutationFromJson(
    Map<String, dynamic> json) {
  return DeleteStrainTypeInfos$Mutation()
    ..deletestraintypeinfos = json['deletestraintypeinfos'] == null
        ? null
        : DeleteStrainTypeInfos$Mutation$StrainTypeInfos.fromJson(
            json['deletestraintypeinfos'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DeleteStrainTypeInfos$MutationToJson(
        DeleteStrainTypeInfos$Mutation instance) =>
    <String, dynamic>{
      'deletestraintypeinfos': instance.deletestraintypeinfos?.toJson(),
    };

GrowingSession$Query$GrowingSession$Plant$Strain
    _$GrowingSession$Query$GrowingSession$Plant$StrainFromJson(
        Map<String, dynamic> json) {
  return GrowingSession$Query$GrowingSession$Plant$Strain()
    ..id = json['id'] as int
    ..name = json['name'] as String;
}

Map<String, dynamic> _$GrowingSession$Query$GrowingSession$Plant$StrainToJson(
        GrowingSession$Query$GrowingSession$Plant$Strain instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };

GrowingSession$Query$GrowingSession$Plant
    _$GrowingSession$Query$GrowingSession$PlantFromJson(
        Map<String, dynamic> json) {
  return GrowingSession$Query$GrowingSession$Plant()
    ..id = json['id'] as String
    ..name = json['name'] as String
    ..strain = json['strain'] == null
        ? null
        : GrowingSession$Query$GrowingSession$Plant$Strain.fromJson(
            json['strain'] as Map<String, dynamic>)
    ..startedAt = json['startedAt'] == null
        ? null
        : DateTime.parse(json['startedAt'] as String)
    ..endedAt = json['endedAt'] == null
        ? null
        : DateTime.parse(json['endedAt'] as String);
}

Map<String, dynamic> _$GrowingSession$Query$GrowingSession$PlantToJson(
        GrowingSession$Query$GrowingSession$Plant instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'strain': instance.strain?.toJson(),
      'startedAt': instance.startedAt?.toIso8601String(),
      'endedAt': instance.endedAt?.toIso8601String(),
    };

GrowingSession$Query$GrowingSession
    _$GrowingSession$Query$GrowingSessionFromJson(Map<String, dynamic> json) {
  return GrowingSession$Query$GrowingSession()
    ..id = json['id'] as String
    ..name = json['name'] as String
    ..type = _$enumDecodeNullable(_$GrowingSessionTypeEnumMap, json['type'],
        unknownValue: GrowingSessionType.artemisUnknown)
    ..state = _$enumDecodeNullable(_$GrowingSessionStateEnumMap, json['state'],
        unknownValue: GrowingSessionState.artemisUnknown)
    ..startedAt = json['startedAt'] == null
        ? null
        : DateTime.parse(json['startedAt'] as String)
    ..endedAt = json['endedAt'] == null
        ? null
        : DateTime.parse(json['endedAt'] as String)
    ..plants = (json['plants'] as List)
        ?.map((e) => e == null
            ? null
            : GrowingSession$Query$GrowingSession$Plant.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$GrowingSession$Query$GrowingSessionToJson(
        GrowingSession$Query$GrowingSession instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'type': _$GrowingSessionTypeEnumMap[instance.type],
      'state': _$GrowingSessionStateEnumMap[instance.state],
      'startedAt': instance.startedAt?.toIso8601String(),
      'endedAt': instance.endedAt?.toIso8601String(),
      'plants': instance.plants?.map((e) => e?.toJson())?.toList(),
    };

GrowingSession$Query _$GrowingSession$QueryFromJson(Map<String, dynamic> json) {
  return GrowingSession$Query()
    ..growingsession = json['growingsession'] == null
        ? null
        : GrowingSession$Query$GrowingSession.fromJson(
            json['growingsession'] as Map<String, dynamic>);
}

Map<String, dynamic> _$GrowingSession$QueryToJson(
        GrowingSession$Query instance) =>
    <String, dynamic>{
      'growingsession': instance.growingsession?.toJson(),
    };

UpdateFcmToken$Mutation$User _$UpdateFcmToken$Mutation$UserFromJson(
    Map<String, dynamic> json) {
  return UpdateFcmToken$Mutation$User()..id = json['id'] as String;
}

Map<String, dynamic> _$UpdateFcmToken$Mutation$UserToJson(
        UpdateFcmToken$Mutation$User instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

UpdateFcmToken$Mutation _$UpdateFcmToken$MutationFromJson(
    Map<String, dynamic> json) {
  return UpdateFcmToken$Mutation()
    ..updateuserfcmtoken = json['updateuserfcmtoken'] == null
        ? null
        : UpdateFcmToken$Mutation$User.fromJson(
            json['updateuserfcmtoken'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UpdateFcmToken$MutationToJson(
        UpdateFcmToken$Mutation instance) =>
    <String, dynamic>{
      'updateuserfcmtoken': instance.updateuserfcmtoken?.toJson(),
    };

Strain$Query$Strain$SeedFinderStrainInfo
    _$Strain$Query$Strain$SeedFinderStrainInfoFromJson(
        Map<String, dynamic> json) {
  return Strain$Query$Strain$SeedFinderStrainInfo()
    ..id = json['id'] as int
    ..sfStrainHtmlDescription = json['sfStrainHtmlDescription'] as String;
}

Map<String, dynamic> _$Strain$Query$Strain$SeedFinderStrainInfoToJson(
        Strain$Query$Strain$SeedFinderStrainInfo instance) =>
    <String, dynamic>{
      'id': instance.id,
      'sfStrainHtmlDescription': instance.sfStrainHtmlDescription,
    };

Strain$Query$Strain$SeedBank _$Strain$Query$Strain$SeedBankFromJson(
    Map<String, dynamic> json) {
  return Strain$Query$Strain$SeedBank()
    ..id = json['id'] as int
    ..name = json['name'] as String
    ..logoUrl = json['logoUrl'] as String;
}

Map<String, dynamic> _$Strain$Query$Strain$SeedBankToJson(
        Strain$Query$Strain$SeedBank instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'logoUrl': instance.logoUrl,
    };

Strain$Query$Strain$StrainTypeInfos
    _$Strain$Query$Strain$StrainTypeInfosFromJson(Map<String, dynamic> json) {
  return Strain$Query$Strain$StrainTypeInfos()
    ..id = json['id'] as int
    ..type = _$enumDecodeNullable(_$StrainTypeEnumMap, json['type'],
        unknownValue: StrainType.artemisUnknown)
    ..percentage = (json['percentage'] as num)?.toDouble();
}

Map<String, dynamic> _$Strain$Query$Strain$StrainTypeInfosToJson(
        Strain$Query$Strain$StrainTypeInfos instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': _$StrainTypeEnumMap[instance.type],
      'percentage': instance.percentage,
    };

const _$StrainTypeEnumMap = {
  StrainType.indica: 'INDICA',
  StrainType.sativa: 'SATIVA',
  StrainType.ruderalis: 'RUDERALIS',
  StrainType.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

Strain$Query$Strain$Cannabinoid _$Strain$Query$Strain$CannabinoidFromJson(
    Map<String, dynamic> json) {
  return Strain$Query$Strain$Cannabinoid()
    ..id = json['id'] as int
    ..name = json['name'] as String
    ..percentage = (json['percentage'] as num)?.toDouble();
}

Map<String, dynamic> _$Strain$Query$Strain$CannabinoidToJson(
        Strain$Query$Strain$Cannabinoid instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'percentage': instance.percentage,
    };

Strain$Query$Strain$Measure _$Strain$Query$Strain$MeasureFromJson(
    Map<String, dynamic> json) {
  return Strain$Query$Strain$Measure()
    ..id = json['id'] as String
    ..type = _$enumDecodeNullable(_$DataTypeEnumMap, json['type'],
        unknownValue: DataType.artemisUnknown)
    ..unit = json['unit'] as String
    ..value = (json['value'] as num)?.toDouble();
}

Map<String, dynamic> _$Strain$Query$Strain$MeasureToJson(
        Strain$Query$Strain$Measure instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': _$DataTypeEnumMap[instance.type],
      'unit': instance.unit,
      'value': instance.value,
    };

Strain$Query$Strain _$Strain$Query$StrainFromJson(Map<String, dynamic> json) {
  return Strain$Query$Strain()
    ..supplier = json['supplier'] as String
    ..link = json['link'] as String
    ..seedFinderStrainInfo = json['seedFinderStrainInfo'] == null
        ? null
        : Strain$Query$Strain$SeedFinderStrainInfo.fromJson(
            json['seedFinderStrainInfo'] as Map<String, dynamic>)
    ..seedBank = json['seedBank'] == null
        ? null
        : Strain$Query$Strain$SeedBank.fromJson(
            json['seedBank'] as Map<String, dynamic>)
    ..name = json['name'] as String
    ..genetic = json['genetic'] as String
    ..photoperiod = _$enumDecodeNullable(
        _$StrainPhotoperiodEnumMap, json['photoperiod'],
        unknownValue: StrainPhotoperiod.artemisUnknown)
    ..type = (json['type'] as List)
        ?.map((e) => e == null
            ? null
            : Strain$Query$Strain$StrainTypeInfos.fromJson(
                e as Map<String, dynamic>))
        ?.toList()
    ..cannabinoids = (json['cannabinoids'] as List)
        ?.map((e) => e == null
            ? null
            : Strain$Query$Strain$Cannabinoid.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..floweringTimes = (json['floweringTimes'] as List)?.map((e) => e == null ? null : Strain$Query$Strain$Measure.fromJson(e as Map<String, dynamic>))?.toList();
}

Map<String, dynamic> _$Strain$Query$StrainToJson(
        Strain$Query$Strain instance) =>
    <String, dynamic>{
      'supplier': instance.supplier,
      'link': instance.link,
      'seedFinderStrainInfo': instance.seedFinderStrainInfo?.toJson(),
      'seedBank': instance.seedBank?.toJson(),
      'name': instance.name,
      'genetic': instance.genetic,
      'photoperiod': _$StrainPhotoperiodEnumMap[instance.photoperiod],
      'type': instance.type?.map((e) => e?.toJson())?.toList(),
      'cannabinoids': instance.cannabinoids?.map((e) => e?.toJson())?.toList(),
      'floweringTimes':
          instance.floweringTimes?.map((e) => e?.toJson())?.toList(),
    };

Strain$Query _$Strain$QueryFromJson(Map<String, dynamic> json) {
  return Strain$Query()
    ..strain = json['strain'] == null
        ? null
        : Strain$Query$Strain.fromJson(json['strain'] as Map<String, dynamic>);
}

Map<String, dynamic> _$Strain$QueryToJson(Strain$Query instance) =>
    <String, dynamic>{
      'strain': instance.strain?.toJson(),
    };

AddGrowState$Mutation$GrowState _$AddGrowState$Mutation$GrowStateFromJson(
    Map<String, dynamic> json) {
  return AddGrowState$Mutation$GrowState()
    ..developmentState = _$enumDecodeNullable(
        _$DevelopmentStateTypeEnumMap, json['developmentState'],
        unknownValue: DevelopmentStateType.artemisUnknown);
}

Map<String, dynamic> _$AddGrowState$Mutation$GrowStateToJson(
        AddGrowState$Mutation$GrowState instance) =>
    <String, dynamic>{
      'developmentState':
          _$DevelopmentStateTypeEnumMap[instance.developmentState],
    };

AddGrowState$Mutation _$AddGrowState$MutationFromJson(
    Map<String, dynamic> json) {
  return AddGrowState$Mutation()
    ..addgrowstate = json['addgrowstate'] == null
        ? null
        : AddGrowState$Mutation$GrowState.fromJson(
            json['addgrowstate'] as Map<String, dynamic>);
}

Map<String, dynamic> _$AddGrowState$MutationToJson(
        AddGrowState$Mutation instance) =>
    <String, dynamic>{
      'addgrowstate': instance.addgrowstate?.toJson(),
    };

GrowStateInput _$GrowStateInputFromJson(Map<String, dynamic> json) {
  return GrowStateInput(
    developmentState: _$enumDecodeNullable(
        _$DevelopmentStateTypeEnumMap, json['developmentState'],
        unknownValue: DevelopmentStateType.artemisUnknown),
    startedAt: json['startedAt'] == null
        ? null
        : DateTime.parse(json['startedAt'] as String),
    endedAt: json['endedAt'] == null
        ? null
        : DateTime.parse(json['endedAt'] as String),
    comments: json['comments'] as String,
  );
}

Map<String, dynamic> _$GrowStateInputToJson(GrowStateInput instance) =>
    <String, dynamic>{
      'developmentState':
          _$DevelopmentStateTypeEnumMap[instance.developmentState],
      'startedAt': instance.startedAt?.toIso8601String(),
      'endedAt': instance.endedAt?.toIso8601String(),
      'comments': instance.comments,
    };

GrowingSessions$Query$GrowingSession
    _$GrowingSessions$Query$GrowingSessionFromJson(Map<String, dynamic> json) {
  return GrowingSessions$Query$GrowingSession()
    ..id = json['id'] as String
    ..name = json['name'] as String
    ..type = _$enumDecodeNullable(_$GrowingSessionTypeEnumMap, json['type'],
        unknownValue: GrowingSessionType.artemisUnknown)
    ..state = _$enumDecodeNullable(_$GrowingSessionStateEnumMap, json['state'],
        unknownValue: GrowingSessionState.artemisUnknown)
    ..startedAt = json['startedAt'] == null
        ? null
        : DateTime.parse(json['startedAt'] as String)
    ..endedAt = json['endedAt'] == null
        ? null
        : DateTime.parse(json['endedAt'] as String);
}

Map<String, dynamic> _$GrowingSessions$Query$GrowingSessionToJson(
        GrowingSessions$Query$GrowingSession instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'type': _$GrowingSessionTypeEnumMap[instance.type],
      'state': _$GrowingSessionStateEnumMap[instance.state],
      'startedAt': instance.startedAt?.toIso8601String(),
      'endedAt': instance.endedAt?.toIso8601String(),
    };

GrowingSessions$Query _$GrowingSessions$QueryFromJson(
    Map<String, dynamic> json) {
  return GrowingSessions$Query()
    ..growingsessions = (json['growingsessions'] as List)
        ?.map((e) => e == null
            ? null
            : GrowingSessions$Query$GrowingSession.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$GrowingSessions$QueryToJson(
        GrowingSessions$Query instance) =>
    <String, dynamic>{
      'growingsessions':
          instance.growingsessions?.map((e) => e?.toJson())?.toList(),
    };

GrowingSessionPictures$Query$Picture
    _$GrowingSessionPictures$Query$PictureFromJson(Map<String, dynamic> json) {
  return GrowingSessionPictures$Query$Picture()
    ..id = json['id'] as String
    ..takenAt = json['takenAt'] == null
        ? null
        : DateTime.parse(json['takenAt'] as String)
    ..url = json['url'] as String;
}

Map<String, dynamic> _$GrowingSessionPictures$Query$PictureToJson(
        GrowingSessionPictures$Query$Picture instance) =>
    <String, dynamic>{
      'id': instance.id,
      'takenAt': instance.takenAt?.toIso8601String(),
      'url': instance.url,
    };

GrowingSessionPictures$Query _$GrowingSessionPictures$QueryFromJson(
    Map<String, dynamic> json) {
  return GrowingSessionPictures$Query()
    ..pictures = (json['pictures'] as List)
        ?.map((e) => e == null
            ? null
            : GrowingSessionPictures$Query$Picture.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$GrowingSessionPictures$QueryToJson(
        GrowingSessionPictures$Query instance) =>
    <String, dynamic>{
      'pictures': instance.pictures?.map((e) => e?.toJson())?.toList(),
    };

DeletePicture$Mutation$Picture _$DeletePicture$Mutation$PictureFromJson(
    Map<String, dynamic> json) {
  return DeletePicture$Mutation$Picture()
    ..id = json['id'] as String
    ..takenAt = json['takenAt'] == null
        ? null
        : DateTime.parse(json['takenAt'] as String);
}

Map<String, dynamic> _$DeletePicture$Mutation$PictureToJson(
        DeletePicture$Mutation$Picture instance) =>
    <String, dynamic>{
      'id': instance.id,
      'takenAt': instance.takenAt?.toIso8601String(),
    };

DeletePicture$Mutation _$DeletePicture$MutationFromJson(
    Map<String, dynamic> json) {
  return DeletePicture$Mutation()
    ..deletepicture = json['deletepicture'] == null
        ? null
        : DeletePicture$Mutation$Picture.fromJson(
            json['deletepicture'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DeletePicture$MutationToJson(
        DeletePicture$Mutation instance) =>
    <String, dynamic>{
      'deletepicture': instance.deletepicture?.toJson(),
    };

LastMeasureArguments _$LastMeasureArgumentsFromJson(Map<String, dynamic> json) {
  return LastMeasureArguments(
    growingSessionId: json['growingSessionId'] as String,
    type: _$enumDecodeNullable(_$DataTypeEnumMap, json['type'],
        unknownValue: DataType.artemisUnknown),
  );
}

Map<String, dynamic> _$LastMeasureArgumentsToJson(
        LastMeasureArguments instance) =>
    <String, dynamic>{
      'growingSessionId': instance.growingSessionId,
      'type': _$DataTypeEnumMap[instance.type],
    };

DeviceInventoryArguments _$DeviceInventoryArgumentsFromJson(
    Map<String, dynamic> json) {
  return DeviceInventoryArguments(
    inventoryId: json['inventoryId'] as String,
  );
}

Map<String, dynamic> _$DeviceInventoryArgumentsToJson(
        DeviceInventoryArguments instance) =>
    <String, dynamic>{
      'inventoryId': instance.inventoryId,
    };

AllMeasuresArguments _$AllMeasuresArgumentsFromJson(Map<String, dynamic> json) {
  return AllMeasuresArguments(
    growingSessionId: json['growingSessionId'] as String,
    type: _$enumDecodeNullable(_$DataTypeEnumMap, json['type'],
        unknownValue: DataType.artemisUnknown),
    count: json['count'] as int,
    offset: json['offset'] as int,
  );
}

Map<String, dynamic> _$AllMeasuresArgumentsToJson(
        AllMeasuresArguments instance) =>
    <String, dynamic>{
      'growingSessionId': instance.growingSessionId,
      'type': _$DataTypeEnumMap[instance.type],
      'count': instance.count,
      'offset': instance.offset,
    };

SeedInventoryArguments _$SeedInventoryArgumentsFromJson(
    Map<String, dynamic> json) {
  return SeedInventoryArguments(
    inventoryId: json['inventoryId'] as String,
  );
}

Map<String, dynamic> _$SeedInventoryArgumentsToJson(
        SeedInventoryArguments instance) =>
    <String, dynamic>{
      'inventoryId': instance.inventoryId,
    };

AddPictureArguments _$AddPictureArgumentsFromJson(Map<String, dynamic> json) {
  return AddPictureArguments(
    growingSessionId: json['growingSessionId'] as String,
  );
}

Map<String, dynamic> _$AddPictureArgumentsToJson(
        AddPictureArguments instance) =>
    <String, dynamic>{
      'growingSessionId': instance.growingSessionId,
    };

DeleteStrainCannabinoidArguments _$DeleteStrainCannabinoidArgumentsFromJson(
    Map<String, dynamic> json) {
  return DeleteStrainCannabinoidArguments(
    strainId: json['strainId'] as int,
    cannabinoidId: json['cannabinoidId'] as int,
  );
}

Map<String, dynamic> _$DeleteStrainCannabinoidArgumentsToJson(
        DeleteStrainCannabinoidArguments instance) =>
    <String, dynamic>{
      'strainId': instance.strainId,
      'cannabinoidId': instance.cannabinoidId,
    };

AddDeviceStockArguments _$AddDeviceStockArgumentsFromJson(
    Map<String, dynamic> json) {
  return AddDeviceStockArguments(
    inventoryId: json['inventoryId'] as String,
    input: json['input'] == null
        ? null
        : DeviceStockInput.fromJson(json['input'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AddDeviceStockArgumentsToJson(
        AddDeviceStockArguments instance) =>
    <String, dynamic>{
      'inventoryId': instance.inventoryId,
      'input': instance.input?.toJson(),
    };

GrowingSessionDevicesArguments _$GrowingSessionDevicesArgumentsFromJson(
    Map<String, dynamic> json) {
  return GrowingSessionDevicesArguments(
    growingSessionId: json['growingSessionId'] as String,
  );
}

Map<String, dynamic> _$GrowingSessionDevicesArgumentsToJson(
        GrowingSessionDevicesArguments instance) =>
    <String, dynamic>{
      'growingSessionId': instance.growingSessionId,
    };

PlantArguments _$PlantArgumentsFromJson(Map<String, dynamic> json) {
  return PlantArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$PlantArgumentsToJson(PlantArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

UpdateUserArguments _$UpdateUserArgumentsFromJson(Map<String, dynamic> json) {
  return UpdateUserArguments(
    input: json['input'] == null
        ? null
        : UpdateUserInput.fromJson(json['input'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$UpdateUserArgumentsToJson(
        UpdateUserArguments instance) =>
    <String, dynamic>{
      'input': instance.input?.toJson(),
    };

AddSeedStockArguments _$AddSeedStockArgumentsFromJson(
    Map<String, dynamic> json) {
  return AddSeedStockArguments(
    inventoryId: json['inventoryId'] as String,
    input: json['input'] == null
        ? null
        : SeedStockInput.fromJson(json['input'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AddSeedStockArgumentsToJson(
        AddSeedStockArguments instance) =>
    <String, dynamic>{
      'inventoryId': instance.inventoryId,
      'input': instance.input?.toJson(),
    };

LoginArguments _$LoginArgumentsFromJson(Map<String, dynamic> json) {
  return LoginArguments(
    name: json['name'] as String,
    password: json['password'] as String,
    otp: json['otp'] as int,
  );
}

Map<String, dynamic> _$LoginArgumentsToJson(LoginArguments instance) =>
    <String, dynamic>{
      'name': instance.name,
      'password': instance.password,
      'otp': instance.otp,
    };

AddStrainFloweringTimeInfosArguments
    _$AddStrainFloweringTimeInfosArgumentsFromJson(Map<String, dynamic> json) {
  return AddStrainFloweringTimeInfosArguments(
    strainId: json['strainId'] as int,
    input: (json['input'] as List)
        ?.map((e) =>
            e == null ? null : MeasureInput.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$AddStrainFloweringTimeInfosArgumentsToJson(
        AddStrainFloweringTimeInfosArguments instance) =>
    <String, dynamic>{
      'strainId': instance.strainId,
      'input': instance.input?.map((e) => e?.toJson())?.toList(),
    };

AddGrowingSessionArguments _$AddGrowingSessionArgumentsFromJson(
    Map<String, dynamic> json) {
  return AddGrowingSessionArguments(
    input: json['input'] == null
        ? null
        : GrowingSessionInput.fromJson(json['input'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AddGrowingSessionArgumentsToJson(
        AddGrowingSessionArguments instance) =>
    <String, dynamic>{
      'input': instance.input?.toJson(),
    };

DeleteStrainTypeInfosArguments _$DeleteStrainTypeInfosArgumentsFromJson(
    Map<String, dynamic> json) {
  return DeleteStrainTypeInfosArguments(
    strainId: json['strainId'] as int,
    straintypeinfosId: json['straintypeinfosId'] as int,
  );
}

Map<String, dynamic> _$DeleteStrainTypeInfosArgumentsToJson(
        DeleteStrainTypeInfosArguments instance) =>
    <String, dynamic>{
      'strainId': instance.strainId,
      'straintypeinfosId': instance.straintypeinfosId,
    };

GrowingSessionArguments _$GrowingSessionArgumentsFromJson(
    Map<String, dynamic> json) {
  return GrowingSessionArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$GrowingSessionArgumentsToJson(
        GrowingSessionArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

UpdateFcmTokenArguments _$UpdateFcmTokenArgumentsFromJson(
    Map<String, dynamic> json) {
  return UpdateFcmTokenArguments(
    userId: json['userId'] as String,
    fcmToken: json['fcmToken'] as String,
  );
}

Map<String, dynamic> _$UpdateFcmTokenArgumentsToJson(
        UpdateFcmTokenArguments instance) =>
    <String, dynamic>{
      'userId': instance.userId,
      'fcmToken': instance.fcmToken,
    };

StrainArguments _$StrainArgumentsFromJson(Map<String, dynamic> json) {
  return StrainArguments(
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$StrainArgumentsToJson(StrainArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

AddGrowStateArguments _$AddGrowStateArgumentsFromJson(
    Map<String, dynamic> json) {
  return AddGrowStateArguments(
    plantId: json['plantId'] as String,
    input: json['input'] == null
        ? null
        : GrowStateInput.fromJson(json['input'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AddGrowStateArgumentsToJson(
        AddGrowStateArguments instance) =>
    <String, dynamic>{
      'plantId': instance.plantId,
      'input': instance.input?.toJson(),
    };

GrowingSessionPicturesArguments _$GrowingSessionPicturesArgumentsFromJson(
    Map<String, dynamic> json) {
  return GrowingSessionPicturesArguments(
    growingSessionId: json['growingSessionId'] as String,
  );
}

Map<String, dynamic> _$GrowingSessionPicturesArgumentsToJson(
        GrowingSessionPicturesArguments instance) =>
    <String, dynamic>{
      'growingSessionId': instance.growingSessionId,
    };

DeletePictureArguments _$DeletePictureArgumentsFromJson(
    Map<String, dynamic> json) {
  return DeletePictureArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$DeletePictureArgumentsToJson(
        DeletePictureArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
