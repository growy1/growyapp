import 'dart:convert';
import 'dart:io';

import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import 'package:sembast_web/sembast_web.dart';

class Jwt {
  Jwt({this.encoded, this.issuer, this.audience, this.expireAt});
  factory Jwt.fromToken(String base) {
    final String text = base.split('.')[1];
    final Uint8List decoded = base64.decode(base64.normalize(text));
    final String val = utf8.decode(decoded);
    final Map<String, dynamic> jason = json.decode(val) as Map<String, dynamic>;
    final String iss = jason['iss'] as String;
    final String aud = jason['aud'] as String;
    final DateTime exp =
        DateTime.fromMillisecondsSinceEpoch((jason['exp'] as int) * 1000);
    return Jwt(encoded: base, issuer: iss, audience: aud, expireAt: exp);
  }
  static const String dbPath = 'growyapp.db';

  static Future<Jwt> fromStorage() async {
    Jwt token;
    Database db;
    final StoreRef<String, String> store = StoreRef<String, String>.main();

    if (!kIsWeb) {
      final Directory appDocDir = await getApplicationDocumentsDirectory();
      final DatabaseFactory dbFactory = databaseFactoryIo;
      db = await dbFactory.openDatabase(appDocDir.path + '/' + dbPath);
    } else {
      final DatabaseFactory dbFactory = databaseFactoryWeb;
      db = await dbFactory.openDatabase(dbPath);
    }
    final String base = await store.record('jwt').get(db);
    if (base != null && base.isNotEmpty) {
      token = Jwt.fromToken(base);
    }
    return token;
  }

  String encoded;
  String issuer;
  String audience;
  DateTime expireAt;
  bool get isexpired => DateTime.now().isAfter(expireAt);
  Future<void> logout() async {
    Database db;
    final StoreRef<String, String> store = StoreRef<String, String>.main();

    if (!kIsWeb) {
      final Directory appDocDir = await getApplicationDocumentsDirectory();
      final DatabaseFactory dbFactory = databaseFactoryIo;
      db = await dbFactory.openDatabase(appDocDir.path + '/' + dbPath);
    } else {
      final DatabaseFactory dbFactory = databaseFactoryWeb;
      db = await dbFactory.openDatabase(dbPath);
    }
    encoded = null;
    await store.record('jwt').delete(db);
  }
}
