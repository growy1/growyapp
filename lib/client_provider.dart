import 'dart:io';

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:growyapp/jwt.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast_web/sembast_web.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';

String uuidFromObject(Object object) {
  if (object is Map<String, Object>) {
    final String typeName = object['__typename'] as String;
    final String id = object['id'].toString();
    if (typeName != null && id != null) {
      return <String>[typeName, id].join('/');
    }
  }
  return null;
}

final OptimisticCache cache = OptimisticCache(
  dataIdFromObject: uuidFromObject,
);

ValueNotifier<GraphQLClient> clientFor({
  @required String uri,
  String subscriptionUri,
}) {
  Link link;
  Database db;
  final StoreRef<String, String> store = StoreRef<String, String>.main();
  final HttpLink httpLink = HttpLink(uri: uri);
  final AuthLink authLink = AuthLink(
    getToken: () async {
      String jwt;
      String token;
      bool valid = false;
      const String dbPath = 'growyapp.db';
      if (!kIsWeb) {
        final Directory appDocDir = await getApplicationDocumentsDirectory();
        final DatabaseFactory dbFactory = databaseFactoryIo;
        db = await dbFactory.openDatabase(appDocDir.path + '/' + dbPath);
      } else {
        final DatabaseFactory dbFactory = databaseFactoryWeb;
        db = await dbFactory.openDatabase(dbPath);
      }
      jwt = await store.record('jwt').get(db);
      if (jwt != null && jwt.isNotEmpty) {
        if (!Jwt.fromToken(jwt).isexpired) {
          valid = true;
        }
      }
      token = valid ? jwt : await store.record('basicauth').get(db);
      return token;
    },
  );
  if (subscriptionUri != null) {
    final WebSocketLink websocketLink = WebSocketLink(
      url: subscriptionUri,
      config: const SocketClientConfig(
        autoReconnect: true,
        inactivityTimeout: Duration(seconds: 30),
      ),
    );

    link = authLink.concat(websocketLink);
  }

  link = authLink.concat(httpLink);
  return ValueNotifier<GraphQLClient>(
    GraphQLClient(
      cache: cache,
      link: link,
    ),
  );
}

/// Wraps the root application with the `graphql_flutter` client.
/// We use the cache for all state management.
class ClientProvider extends StatelessWidget {
  ClientProvider({
    @required this.child,
    @required String uri,
    String subscriptionUri,
  }) : client = clientFor(
          uri: uri,
          subscriptionUri: subscriptionUri,
        );

  final Widget child;
  final ValueNotifier<GraphQLClient> client;

  @override
  Widget build(BuildContext context) {
    return GraphQLProvider(
      client: client,
      child: child,
    );
  }
}
