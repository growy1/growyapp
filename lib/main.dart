import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'package:growyapp/jwt.dart';
import 'package:growyapp/push_messaging.dart';
import 'package:growyapp/client_provider.dart';
import 'package:growyapp/components/Login/login.dart';
import 'package:growyapp/components/Admin/setting_screen.dart';
import 'package:growyapp/components/Inventory/inventory_screen.dart';
import 'package:growyapp/components/GrowingSession/growing_sessions.dart';

void main() {
  runApp(MyApp());
}

const String graphqlEndpoint = 'https://api.aerogrow.org/query';
// const String graphqlEndpoint = 'http://localhost:8080/query';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ClientProvider(
        child: CacheProvider(
          child: MaterialApp(
            title: '🌱 Growy',
            theme: ThemeData(
              primarySwatch: Colors.green,
              highlightColor: const Color(0x007dde92),
              // backgroundColor: const Color(0x000c090a),
              backgroundColor: Colors.black,
              visualDensity: VisualDensity.adaptivePlatformDensity,
              // fontFamily: 'Roboto',
            ),
            home: const MyHomePage(),
          ),
        ),
        uri: graphqlEndpoint);
  }
}

@immutable
class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  _MyHomePageState() {
    _homeloggedwidgets = <Widget>[
      const GrowingSessionsComponent(),
      const InventoryScreenComponent(),
    ];
    _homeunloggedwidgets = <Widget>[
      LoginPage(
        logedInNotify: () {
          _isLogged = true;
          setState(() {});
        },
      ),
      const SettingScreenComponent()
    ];
  }

  Jwt tokenDetails;
  int _selectedIndex = 0;
  bool _isLogged = false;
  List<Widget> _homeloggedwidgets;
  List<Widget> _homeunloggedwidgets;

  final List<BottomNavigationBarItem> _loggedItems =
      const <BottomNavigationBarItem>[
    BottomNavigationBarItem(
        label: 'Sessions', icon: Icon(Icons.timelapse_outlined)),
    BottomNavigationBarItem(label: 'Inventory', icon: Icon(Icons.book)),
  ];

  final List<BottomNavigationBarItem> _unloggedItems =
      const <BottomNavigationBarItem>[
    BottomNavigationBarItem(label: 'Login', icon: Icon(Icons.lock_open)),
    BottomNavigationBarItem(label: 'Settings', icon: Icon(Icons.settings))
  ];
  Future<void> getUserSavedCreds() async {
    tokenDetails = await Jwt.fromStorage();
    if (tokenDetails != null) {
      _isLogged = !tokenDetails.isexpired;
    }
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    if (!kIsWeb) {
      PushNotificationsManager().init();
    }
    getUserSavedCreds();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: _isLogged
            ? _homeloggedwidgets[_selectedIndex]
            : _homeunloggedwidgets[_selectedIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.black,
        unselectedItemColor: Colors.grey,
        currentIndex: _selectedIndex,
        type: BottomNavigationBarType.fixed,
        onTap: (int index) => setState(() {
          _selectedIndex = index;
        }),
        items: _isLogged ? _loggedItems : _unloggedItems,
      ),
    );
  }
}
